import os
import json

class sl_config:
    def __init__(self, sl):
        self.sl = sl
        self.fname = sl.const.path_var + '/config/smartlife.'
        if sl.dev:
            self.fname += 'dev.'
        self.fname += 'json'
        self.config = {}
        if os.path.isfile(self.fname):
            f = open(self.fname, 'r')
            self.config = json.loads(f.read())
            f.close()
        change = False
        if not ('plugins' in self.config):
            self.config['plugins'] = {}
            change = True
        if not 'web_core' in self.config['plugins']:
            self.config['plugins']['web_core'] = {}
            change = True
        if not 'smarthome' in self.config['plugins']:
            self.config['plugins']['smarthome'] = {}
            change = True
        if not ('logger' in self.config):
            self.config['logger'] = {'level': 'info', 'dest': 'file'}
            change = True
        if sl.dev and not ('timeout' in self.config):
            self.config['timeout'] = 300
            change = True
        if change:
            self.save()

    def __getattr__(self, k):
        try:
            return self.config[k]
        except KeyError:
            return None

    def __setattr__(self, k, v):
        if k == 'sl' or k == 'fname' or k == 'config':
            self.__dict__[k] = v
        else:
            self.config[k] = v

    def save(self):
         f = open(self.fname, 'w')
         f.write(json.dumps(self.config, indent=2, sort_keys=True))
         f.close()

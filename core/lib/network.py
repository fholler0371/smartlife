import os
import sys

try:
    import netifaces
except:
    os.popen(sys.executable + ' -m pip install netifaces').read()
    import netifaces


def guess_network():
    net = []
    for interface in netifaces.interfaces():
        if netifaces.AF_INET in netifaces.ifaddresses(interface) and 'broadcast' in netifaces.ifaddresses(interface)[netifaces.AF_INET][0]:
            net.append(interface)
    gw = netifaces.gateways()['default'][netifaces.AF_INET][1]
    interface = None
    if len(net) > 1:
        for i in net:
            if str(i) != str(gw):
                interface = i
    elif len(net) > 0:
        interface = net[0]
    if interface == None:
        print('kein Netzwerk gefunden')
        return ''
    addr = netifaces.ifaddresses(interface)[netifaces.AF_INET][0]
    return addr['addr'] + '/' + addr['netmask']

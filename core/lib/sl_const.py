import os
import __main__
import time

import lib.network as network

class sl_const():
    def __init__(self, sl):
        self.sl = sl
        self.path = '/'.join(os.path.realpath(__file__).split('/')[:-2])
        if hasattr(sl, 'aws'):
            self.path_var = '/mnt/smartlife/'
        else:
            self.path_var = self.path
        '''Ermitlung des Namen des Programms '''
        file = os.path.abspath(__main__.__file__)
        self.name = os.path.splitext(os.path.basename(file))[0]
        self.host = os.uname()[1]
        self.start_time = int(time.time())
        self.serial = 'None'
        for entry in os.popen('cat /proc/cpuinfo').read().split('\n'):
            if entry.startswith('Serial'):
                self.serial = entry.split(': ')[1]
        self.ip = network.guess_network()

import os
import json
import importlib

def pluginLoader(sl, name):
     sl.log.info('Start Loading Plugin: %s' % (name))
     if name in sl.plugins.list:
         sl.log.info('Plugin bereits geladen: %s' % (name))
         return sl.plugins.list[name]
     folder = sl.const.path + '/plugins/' + name
     if not(os.path.isdir(folder)):
         sl.log.error('Plugin (Ornder) fehlt: %s' % (name))
     fname = folder + '/manifest.json'
     if not(os.path.isfile(fname)):
         sl.log.error('Manifest fehlt: %s' % (name))
     try:
         f = open(fname, "r")
         manifest = json.loads(f.read())
         f.close()
     except:
         sl.log.error('Manifest kann nicht geladen werden: %s' % (name))
     modulName = "plugins." + manifest['name']
     modul = importlib.import_module(modulName)
     if not(hasattr(modul, manifest['class'])):
         sl.log.error('Klasse nicht gefunden: %s' % (name))
         return None
     class_ = getattr(modul, manifest['class'])
     pl = class_(sl, manifest)
     return pl

class pluginBase:
     def __init__(self, sl, manifest):
         sl.log.info('Init pluginBase')
         self.sl = sl
         self.loaded = False
         self.manifest = manifest
         self.name = manifest['name']
         self.friendly = manifest['friendly']
         config = manifest['config'].copy()
         config.update(self.config)
         if config != self.config:
             self.sl.config.plugins[self.name] = config
             self.sl.config.save()
         ok = True
         if 'requirements' in manifest:
             for pl_name in manifest['requirements']:
                 pluginLoader(sl, pl_name)
                 if pl_name in sl.plugins.list:
                     if not sl.plugins.list[pl_name].loaded:
                        ok = False
                 else:
                     ok = False
         # require part #
         self.loaded = ok
         if self.loaded:
             sl.plugins.add(self)

     @property
     def config(self):
         self.sl.log.info('get config')
         if not(self.name in self.sl.config.plugins):
             self.sl.config.plugins[self.name] = {}
             self.sl.config.save()
         return self.sl.config.plugins[self.name]

     def start(self):
         pass

     def stop(self):
         pass

     def main_menu(self, keyword):
         return None

     def api(self, data):
         return data

class plugins:
    def __init__(self, sl):
        self.sl = sl
        self.sl.log.info('Init Class Plugins')
        self.plugins = {}

    @property
    def list(self):
        return self.plugins

    def __getattr__(self, k):
        try:
            return self.plugins[k]
        except KeyError:
            return None

    def add(self, plugin):
        self.plugins[plugin.name] = plugin

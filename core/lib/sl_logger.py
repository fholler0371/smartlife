import os
import logging
from logging.handlers import RotatingFileHandler

DEFAULT = logging.CRITICAL
stream_handler = None
file_handler = None

class MyFormatter(logging.Formatter):
    width = 50
    datefmt='%Y-%m-%d %H:%M:%S'

    def format(cls, record):
        cpath = '%s:%s:%s:%s' % (record.pathname.split('/')[-2], record.module, record.funcName, record.lineno)
        cpath = cpath[-cls.width:].ljust(cls.width)
        record.message = record.getMessage()
        s = "%-8s %s %s : %s" % (record.levelname, cls.formatTime(record, cls.datefmt), cpath, record.getMessage())
        return s

def getLogger(name):
    global stream_handler, DEFAULT
    logger = logging.getLogger(name)
    stream_handler = logging.StreamHandler()
    logger.setLevel(DEFAULT)
    formatter = MyFormatter()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)
    return logger

def update(sl, logger, config):
    global file_name
    if config['level'] == 'debug':
        logger.setLevel(logging.DEBUG)
    elif config['level'] == 'info':
        logger.setLevel(logging.INFO)
    elif config['level'] == 'warning':
        logger.setLevel(logging.WARNING)
    elif config['level'] == 'error':
        logger.setLevel(logging.ERROR)
    elif config['level'] == 'critical':
        logger.setLevel(logging.CRITICAL)
    global stream_handler, file_handler
    while len(logger.handlers) > 0:
        h = logger.handlers[0]
        logger.removeHandler(h)
    if config['dest'] == 'console':
        if stream_handler == None:
            stream_handler = logging.StreamHandler()
            stream_handler.setFormatter(MyFormatter())
        logger.addHandler(stream_handler)
    elif config['dest'] == 'file':
        path = sl.const.path + '/log'
        if not(os.path.isdir(path)):
            os.mkdir(path)
        if file_handler == None:
            file_name = path + '/' + sl.const.name + '.log'
            file_handler = RotatingFileHandler(file_name, maxBytes=1000000, backupCount=16)
            file_handler.setFormatter(MyFormatter())
        logger.addHandler(file_handler)

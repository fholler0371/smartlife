import os
import sys
import sqlite3

try:
    import bcrypt
except:
    os.popen(sys.executable + ' -m pip install bcrypt').read()
    import bcrypt

real_name = input('echter Name: ')
user = input('Nutzer: ')
passwd = input('Passwort: ')
print('----------------------------------')

salt = bcrypt.gensalt()
hashed = bcrypt.hashpw(passwd.encode(), salt).decode()
print('Nutzer: ', user)
print('hashed Password: ', hashed)
print('----------------------------------')

passwd = input('Passwort wiederholen: ')
hashed2 = bcrypt.hashpw(passwd.encode(), hashed[:29].encode()).decode()
print('hashed Password 2: ', hashed2)
print('identisch: ', (hashed == hashed2))
print('----------------------------------')

if hashed != hashed2:
    sys.exit(0)

path = '/'.join(os.path.realpath(__file__).split('/')[:-2])+'/db'
if not os.path.isdir(path):
   os.mkdir(path)

conn = sqlite3.connect(path + "/user.sqlite")
cur = conn.cursor()
sql = "CREATE TABLE IF NOT EXISTS user (id integer PRIMARY KEY, name text NOT NULL, user text Not Null, passwd text Not Null)"
cur.execute(sql)
conn.commit()

sql = "SELECT id FROM user WHERE user='" + user + "'"
cur.execute(sql)
res = cur.fetchone()

if res == None:
    sql = "INSERT INTO user (name, user, passwd) VALUES ('%s' , '%s', '%s')" % (real_name, user, hashed)
else:
    id = res[0]
    sql = "UPDATE user set name='%s', passwd='%s' WHERE id='%s'" % (real_name, hashed, str(id))
cur.execute(sql)

sql = "SELECT id FROM user WHERE user='%s'" % (user)
cur.execute(sql)
res = cur.fetchone()
id = res[0]
sql = "CREATE TABLE IF NOT EXISTS rights (id integer PRIMARY KEY, user integer NOT NULL, right text Not Null)"
cur.execute(sql)
conn.commit()
sql = "delete from rights where user='%s'" % (str(id))
cur.execute(sql)
conn.commit()
rights = ['authencitator.user', 'authencitator.master', 'smarthome.backend']
for right in rights:
    sql = "insert into rights (user, right) values ('%s', '%s')" % (str(id), right)
    cur.execute(sql)

conn.commit()
conn.close()

import os
import sys
import time
import json
import subprocess
try:
    import requests
except:
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'requests'])
    import requests

INSTALL_PATH='/'.join(os.path.realpath(__file__).split('/')[:-2])
CONFIG={}

def dialog_choice(title, choises):
     while True:
        cols, rows = os.popen('stty size', 'r').read().split()
        cmd = 'whiptail --title "Versionsauswahl" --menu "Bitte einen Branch ausuchen" '+str(int(int(cols)/2))+' '+str(int(int(rows)/2))+' 9 '
        i=1
        for choice in choises:
            cmd += '   "'+str(i)+')" "'+choice+'" '
            i+=1
        res = os.popen(cmd+' 3>&2 2>&1 1>&3').read()[:1]
        try:
            ch = int(res)
        except ValueError as e:
            print('\nEs sind nur Zahlen erlaubt !')
        if ch > 0 and ch < i:
            return choises[ch-1]
        else:
            print('\nErlaupt sind Zahlen von 1 bis ' + str(i-1) + ' !')
        time.sleep(3)

def get_git_branches():
    out = []
    res = json.loads(requests.get('https://gitlab.com/api/v4/projects/20112599/repository/branches').text)
    for entry in res:
        out.append(entry['name'])
    return out

def git_get_brach():
    global CONFIG
    olddir = os.getcwd()
    os.chdir(INSTALL_PATH + '/.repro')
    subprocess.check_call(['git', 'config', 'remote.origin.fetch', '+refs/heads/*:refs/remotes/origin/*'])
    subprocess.check_call(['git', 'fetch', '--all', '--quiet'])
    subprocess.check_call(['git', 'reset', '--hard', '--quiet'])
    subprocess.check_call(['git', 'checkout', CONFIG['branch'], '--quiet'])
    subprocess.check_call(['git', 'pull', '--quiet'])

def save_config():
    global INSTALL_PATH, CONFIG
    f = open(INSTALL_PATH + '/config/install.json', 'w')
    f.write(json.dumps(CONFIG))
    f.close()

def load_config():
    global INSTALL_PATH, CONFIG
    fname = INSTALL_PATH + '/config/install.json'
    if os.path.isfile(fname):
        f = open(fname, 'r')
        CONFIG = json.loads(f.read())
        f.close()

def update():
    global INSTALL_PATH
    x = os.popen('sudo apt-get install python3-dev libffi-dev -y').read()
    os.popen('install -C  ' + INSTALL_PATH + '/.repro/core/bin/* ' + INSTALL_PATH + '/bin')
    os.popen('mkdir -p ' + INSTALL_PATH + '/lib')
    os.popen('install -C  ' + INSTALL_PATH + '/.repro/core/lib/* ' + INSTALL_PATH + '/lib')
    os.popen('mkdir -p ' + INSTALL_PATH + '/plugins')
    os.popen('cp -R ' + INSTALL_PATH + '/.repro/plugins/* ' + INSTALL_PATH + '/plugins')
    os.popen('mkdir -p ' + INSTALL_PATH + '/tools')
    os.popen('cp -R ' + INSTALL_PATH + '/.repro/tools/* ' + INSTALL_PATH + '/tools')
    os.popen('mkdir -p ' + INSTALL_PATH + '/webroot')
    os.popen('cp -R ' + INSTALL_PATH + '/.repro/core/webroot/* ' + INSTALL_PATH + '/webroot')
    for d in os.listdir(INSTALL_PATH + '/plugins'):
        path = INSTALL_PATH + '/plugins/' + d + '/js_web'
        if os.path.isdir(path):
            os.popen('cp '+path+'/* '+INSTALL_PATH+'/webroot/lib/module/')
    os.popen('mkdir -p ' + INSTALL_PATH + '/webroot/lib/sm_backend')
    for d in os.listdir(INSTALL_PATH + '/plugins'):
        path = INSTALL_PATH + '/plugins/' + d + '/js_sm_backend'
        if os.path.isdir(path):
            os.popen('cp '+path+'/* '+INSTALL_PATH+'/webroot/lib/sm_backend/')
    print('install webserver')
    res = os.popen('command -v nginx').read()
    if res == '':
        print('install nginx')
        x = os.popen('sudo apt-get install nginx -y 2>/dev/null').read()
    fName = INSTALL_PATH+'/nginx.conf'
    if os.path.isfile(fName):
        os.unlink(fName)
    f = open(fName, 'w+')
    f.write('server {\n  listen 5000;\n  index index.html;\n  root  /opt/smartlife/webroot;\n  location /api-core/ {\n    proxy_pass http://127.0.0.1:5001;\n  }\n}')
    f.close()
    x = os.popen('sudo mv ' + fName + ' /etc/nginx/sites-enabled/smartlife').read()
    x += os.popen('sudo systemctl restart nginx').read()
    if os.path.isfile(fName):
        os.unlink(fName)
    print('install requirejs')
    x += os.popen('wget https://requirejs.org/docs/release/2.3.6/minified/require.js -O ' + INSTALL_PATH + '/webroot/lib/require/require.js >/dev/null 2>/dev/null').read()
    x += os.popen('mkdir -p ' + INSTALL_PATH + '/.jqwidgets').read()
    print('install jqwidegts')
    fName = INSTALL_PATH+'/.jqwidgets/master.zip'
    if os.path.isfile(fName):
        os.unlink(fName)
#    x += os.popen('wget https://github.com/jqwidgets/jQWidgets/archive/master.zip -O ' + fName + ' >/dev/null 2>/dev/null').read()
    x += os.popen('unzip -u ' + fName + ' -d ' + INSTALL_PATH + '/.jqwidgets/ >/dev/null 2>/dev/null').read()
    x += os.popen('cp -R ' + INSTALL_PATH + '/.jqwidgets/jQWidgets-master/jqwidgets/* ' + INSTALL_PATH + '/webroot/lib/jqwidgets 2>/dev/null').read()
    print('install jqueryjs')
    x += os.popen('wget https://code.jquery.com/jquery-3.5.1.min.js -O ' + INSTALL_PATH + '/webroot/lib/jquery/jquery-3.5.1.min.js >/dev/null 2>/dev/null').read()
    x += os.popen('mkdir -p ' + INSTALL_PATH + '/.jqwidgets').read()
    print('install finished')


def main():
    global INSTALL_PATH, CONFIG
    l = len(sys.argv)
    if not os.path.isdir(INSTALL_PATH + '/config'):
        os.makedirs(INSTALL_PATH + '/config')
    if l > 1:
        print('option is:',  sys.argv[1])
        if "-i" != sys.argv[1]:
            load_config()
    if not('branch' in CONFIG):
        branches = get_git_branches()
        CONFIG['branch']=dialog_choice('Welcher Brach soll installiert werdern?', branches)
#    git_get_brach()
    update()
    save_config()

if __name__ == '__main__':
    main()

print(CONFIG)

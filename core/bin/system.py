import sys
import os
import pwd
import subprocess

SCRIPT = '''
[Unit]
Description=file
After=network.target

[Service]
ExecStart=python path/file.py
WorkingDirectory=path
StandardOutput=inherit

StandardError=inherit
Restart=always
User=user
[Install]
WantedBy=multi-user.target
'''

def install():
    global SCRIPT
    path = '/'.join(os.path.realpath(__file__).split('/')[:-2])
    file = 'smartlife'
    script = SCRIPT.replace('file', 'bin/'+file).replace('path', path).replace('user', pwd.getpwuid(os.getuid())[0]).replace('python', path+'/env/bin/python')
    f = open(path + '/tmp_service', 'w')
    f.write(script)
    f.close()
    subprocess.run(('sudo cp ' + path + '/tmp_service /etc/systemd/system/' + file + '.service').split(' '))
    os.remove(path + '/tmp_service')
    subprocess.run(('sudo systemctl enable ' + file + '.service').split(' '))
    subprocess.run(('sudo systemctl start ' + file + '.service').split(' '))

def main():
    l = len(sys.argv)
    if len(sys.argv) > 1:
        if 'install' == sys.argv[1]:
            install()

if __name__ == '__main__':
    main()

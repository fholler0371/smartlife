import sys
import os
import time
import signal

sys.path.append('/'.join(os.path.realpath(__file__).split('/')[:-2]))

import lib.sl_const as sl_const
import lib.sl_config as sl_config
import lib.sl_logger as sl_logger
import lib.plugin as plugin

_sl = None

def signalHandler(signum, frame):
    global _sl
    _sl.stop()

class smartlife():
    def __init__(self):
        self.dev = False
        global _sl
        _sl = self
        if len(sys.argv) > 1:
            self.dev = True
        self.running = True
        self.const = sl_const.sl_const(self)
        signal.signal(signal.SIGINT, signalHandler)
        signal.signal(signal.SIGTERM, signalHandler)
        signal.signal(signal.SIGHUP, signalHandler)
        signal.signal(signal.SIGQUIT, signalHandler)
        self.config = sl_config.sl_config(self)
        self.log = sl_logger.getLogger(self.const.name)
        if self.config.logger == None:
            self.log.critical('Keine Konfigurationsdaten fuer Logger')
            os._exit(0)
        else:
            sl_logger.update(self, self.log, self.config.logger)
        self.plugins = plugin.plugins(self)
        pl_config = self.config.plugins
        try:
            for name in pl_config:
                if not(name.startswith('__')) and not (name in  self.plugins.list):
                    pl = plugin.pluginLoader(self, name)
                    if pl != None:
                        if pl.loaded:
                            pl.start()
        except Exception as e:
            self.running = False
            self.log.error("Fehler beim start")
            self.log.error(str(e))
            if str(e) == 'dictionary changed size during iteration':
                self.stop()
            
    def run(self):
        self.log.critical('SmartLife Run')
        timeOut = time.time() + self.config.timeout
        while self.running and (not self.dev or time.time() < timeOut):
            time.sleep(1)
        self.log.debug('SmartLife Run beendet')
 
    def stop(self):
        self.log.critical('SmartLife Stop')
        for key, pl in self.plugins.list.items():
            pl.stop()
        self.log.debug('SmartLife Stop beendet')
        os._exit(1)

if __name__ == '__main__':
    sh = smartlife()
    sh.run()
    sh.stop()

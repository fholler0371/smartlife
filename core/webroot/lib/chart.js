define(['jqxwindow', 'jqxdropdownlist', 'jqxdata', 'jqxchart'], function() {
  window.check = {
    execute : function(data) {
      if (data.win_title == undefined) {
        data.win_title = 'Keine Bezeichnung'
      }
      $('#window_chart').remove()
      var html = '<div id="window_chart"><div></div><div id="window_chart_content"><div id="window_chart_button"></div>'
      html += '<div id="window_chart_button_chart" style="width: 100%; height: calc(100% - 70px); position: absolute; '
      html += 'bottom: 0; left: 0;"></div></div></div>'
      $('body').append(html)
      $('#window_chart').jqxWindow({height: '80%', width: '80%', 'title': data.win_title, isModal: true, resizable: false, modalOpacity: 0.5})
      if (data.typ < 1) {
        $('#window_chart').jqxWindow('close')
        return
      }
      var source = []
      if (data.history < 2) {source.push('Minute')}
      if (data.history < 3) {source.push('5 Minuten')}
      if (data.history < 4) {source.push('15 Minuten')}
      if (data.history < 5) {source.push('Stunde')}
      if (data.history < 6) {source.push('Tag')}
      $("#window_chart_button").jqxDropDownList({ source: source, width: 250, height: 24})
      $("#window_chart_button").jqxDropDownList('selectIndex', source.length-1)
      var source = {
        datatype : 'array',
        localdata : []
      }
      var len = data.values.length,
      dmin = 0,
      dmax = 0
      for (var i=0; i<len; i++) {
         var d = new Date(data.values[i].time*1000)
         if (i == 0) {dmin = d}
         dmax = d
         source.localdata.push({'date': d, 'value': data.values[i].value,
                                           'value_max': data.values[i].value_max,
                                           'value_min': data.values[i].value_min})
      }
      var dataAdapter = new $.jqx.dataAdapter(source)
      var months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez']
      var settings = {
        title: data.label,
        description: "",
        enableAnimations: false,
        showLegend: false,
        padding: { left: 30, top: 5, right: 10, bottom: 5 },
        titlePadding: { left: 50, top: 0, right: 0, bottom: 10 },
        source: dataAdapter,
        xAxis: {
          dataField: 'date',
          displayText: 'Datum',
          formatFunction: function (value) {
            return value.getDate() + '-' + months[value.getMonth()] + '-' + value.getFullYear();
          },
          type: 'date',
          baseUnit: 'month',
          valuesOnTicks: true,
          minValue: dmin,
          maxValue: dmax,
          tickMarks: {
            visible: true,
            interval: 1,
            color: '#BCBCBC'
          },
          unitInterval: 1,
          gridLines: {
            visible: true,
            interval: 3,
            color: '#BCBCBC'
          },
          labels: {
            angle: -45,
            rotationPoint: 'topright',
            offset: { x: 0, y: -25 }
          }
        },
        valueAxis: {
          visible: true,
          title: { text: '' },
          tickMarks: { color: '#BCBCBC' }
        },
        seriesGroups: [
          {
            type: 'spline',
            series: [
              { dataField: 'value', displayText: 'Wert', lineColor: '#000000', lineWidth: 4 },
              { dataField: 'value_min', displayText: 'Minimum', lineColor: '#00FF00', lineWidth: 1 },
              { dataField: 'value_max', displayText: 'Maximum', lineColor: '#FF0000', lineWidth: 1 }
            ]
          }
        ]
      }
      $('#window_chart_button_chart').jqxChart(settings)
    }
  }
  return window.check
})

import os
import time
from threading import Thread

import lib.plugin as plugin

from plugins.rfid import device, db

class sendout(Thread):
    def __init__(self, sl, data):
       Thread.__init__(self)
       self.sl = sl
       self.data = data

    def run(self):
       try:
           self.sl.plugins.sensors.new_data(self.data)
       except Exception as e:
           self.sl.log.error(str(e))

class rfid(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.th = None
        res = os.popen('sudo chmod 777 /dev/spi*').read()

    def new_text(self, text):
        if db.check(text):
            if 'sensors' in self.sl.plugins.list:
                th = sendout(self.sl, {'name': self.sl.const.host+'rfid:id', self.sl.const.host+'friendly': 'rfid:id',
                                       'value': text, 'time': int(time.time())})
                th.start()

    def sm_api(self, data):
        if data['data']['tab'] == 'history':
            if data['cmd'] == 'get_data':
                data['data'] = db.history()
        return data

    def sm_backend_menu(self):
        return {'name': 'rfid', 'friendly':'RFID-Reader'}

    def start(self):
        self.sl.log.info('Start')
        db.init(self.sl)
        self.th = device.scanner(self.sl, self.new_text)
        self.th.start()
        if 'sensors' in self.sl.plugins.list:
            self.sl.plugins.sensors.start()

    def stop(self):
        self.sl.log.info('Stop')
        try:
            if self.th:
                self.th.running = False
        except:
            print()

import os
import sys
import time

from threading import Thread

try:
    from mfrc522 import SimpleMFRC522
except:
    os.popen(sys.executable + ' -m pip install mfrc522').read()
    from mfrc522 import SimpleMFRC522

import RPi.GPIO as GPIO

class scanner(Thread):
    def __init__(self, sl, cb):
        Thread.__init__(self)
        GPIO.setwarnings(False)
        self.sl = sl
        self.cb = cb
        self.running = True

    def run(self):
        reader = SimpleMFRC522()
        while self.running:
            id, text = reader.read_no_block()
            if id:
                self.cb(id)
            time.sleep(1)

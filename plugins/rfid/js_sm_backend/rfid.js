define(['jqxtabs', 'jqxdata', 'jqxdatatable'], function() {
  var reader = {
    init : function() {
      html = '<div id="smarthome_rfid_tab"><ul><li>History</li></ul>'
      html += '<div id="smarthome_rfid_history"><div id="smarthome_rfid_table" style="margin:10px;"></div></div>'
      html += '</div>'
      $('#smarthome_data').html(html)
      $('#smarthome_rfid_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      window.sm_b.rfid.call_tab(0)
      $('#smarthome_rfid_tab').off('selected')
      $('#smarthome_rfid_tab').on('selected', function(ev) {
        var selectedTab = ev.args.item
        window.sm_b.rfid.call_tab(selectedTab)
      })
    },
    call_tab : function(id) {
      if (id == 0) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'rfid', 'tab': 'history'}}, function(data) {
          var source = {
            localData: data.data,
            dataType: "array",
            dataFields: [
              { name: 'text', type: 'string' },
              { name: 'time', type: 'integer' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source)
          $('#smarthome_rfid_table').jqxDataTable({
            source: dataAdapter,
            columns: [
              { text: 'Text', dataField: 'text', 'width': 350 },
              { text: 'Zeit', dataField: 'time' , 'cellsAlign': 'right', 'width': 200}
            ]
          })
        })
      } else {
        console.error(id)
      }
    }
  }
  window.sm_b.rfid = reader
  return reader
})

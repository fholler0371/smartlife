from plugins.wifiscan import db
import copy
import time

_sl = None

def init(sl):
    global _sl
    _sl = sl
    _sl.log.info('Init')
    if 'sensors' in _sl.plugins.list:
        _sl.plugins.sensors.bind_event(event)

def event(data):
    global _sl
    topic_s = data['topic'].split(':')
    if topic_s[1] == "wifiscan":
        if topic_s[2] in ['new_ap', 'resend_ap']:
            value = data['value']
            db.ap_global_data(value)
        elif topic_s[2] in ['new', 'resend']:
            value = data['value']
            db.st_global_data(data['topic'], value)
        elif topic_s[2] in ['delete']:
            value = data['value']
            db.st_global_data_del(data['topic'], value)

def api_ap(cmd, data):
    global _sl
    out = []
    if cmd == "get_data":
        for ap in db._apg:
            d = copy.copy(db._apg[ap])
            if d['last'] > time.time()-604800:
                d['last'] = make_time_string(d['last'])
                out.append(d)
    elif cmd == "set_data":
        if data['mac'] in db._apg:
            if data['field'] == 'out':
                db._apg[data['mac']]['out'] = data['value']
                out = db._apg[data['mac']]
    if _apg[mac]['out']:
        if 'sensors' in _sl.plugins.list:
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:ap:ssid:'+mac.replace(':','_'),
                               'value': data['ssid'],
                               'time': data['last']})
            th.start()
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:ap:rssi:'+mac.replace(':','_'),
                               'value': data['rssi'],
                               'time': data['last']})
            th.start()
            _sl.log.error('ap_send')
    return out

def api_st(cmd, data):
    global _sl
    out = []
    if cmd == "get_data":
        for st in db._stg:
            d = copy.copy(db._stg[st])
            if d['last'] > time.time()-604800:
                d['last'] = make_time_string(d['last'])
                if d['ap'] in db._apg:
                    d['ap_ssid'] = db._apg[d['ap']]['ssid']
                else:
                    d['ap_ssid'] = d['ap']
                out.append(d)
    elif cmd == "set_data":
        if data['mac'] in db._stg:
            if data['field'] == 'name':
                db._stg[data['mac']]['name'] = data['value']
                out = db._stg[data['mac']]
            elif data['field'] == 'out':
                db._stg[data['mac']]['out'] = data['value']
                out = db._stg[data['mac']]
    return out

def st_global_data(topic, data):
    global _sl
    mac = data['mac']
    host = topic.split(':')[0]
    _sl.log.error(data)

def make_time_string(x):
    t = int(time.time() - x)
    if t < 60:
        s = str(t)+' s'
        return s
    t = int(t/60)
    if t < 60:
        s = str(t)+' min'
        return s
    m = t - 60*int(t/60)
    s = ('0'+str(m))[-2:]
    s = ':'+s
    t = int(t/60)
    if t < 24:
        s = ('0'+str(t))[-2:]+s
        return s
    h = t - 24*int(t/24)
    s = 'd '+('0'+str(m))[-2:]+s
    t = int(t/24)
    s = str(t)+s
    return s

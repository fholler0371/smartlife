define(['jqxtabs', 'jqxdropdownlist', 'jqxcheckbox', 'jqxdata', 'jqxgrid', ], function() {
  var wifi = {
    init : function() {
      html = '<div id="smarthome_wifiscan_tab"><ul><li>WiFi-Cards</li><li>Clients</li><li>Accesspoints</li><li>Einstellungen</li></ul>'
      html += '<div><div id="smarthome_wifiscan_card"></div></div>'
      html += '<div><div id="smarthome_wifiscan_st"></div></div>'
      html += '<div><div id="smarthome_wifiscan_ap"></div></div>'
      html += '<div id="smarthome_wifiscan_settings"><table>'
      html += '<tr><td><b>USB-Port:</b></td><td><div id="smarthome_wifiscan_usb"></div></td><td></td></tr>'
      html += '<tr><td><b>Intervall (in Sekunden):</b></td><td><div id="smarthome_wifiscan_intervall"></div></td><td></td></tr>'
      html += '<tr><td><b>Frontend</b></td><td><div id="smarthome_wifiscan_front" style="margin-left: 10px"></div></td><td></td></tr>'
      html += '<tr><td><b>Backend (Master)</b></td><td><div id="smarthome_wifiscan_back_m" style="margin-left: 10px"></div></td><td></td></tr>'
      html += '<tr><td><b>Backend</b></td><td><div id="smarthome_wifiscan_back" style="margin-left: 10px"></div></td><td></td></tr>'
      html += '<tr><td></td><td></td><td><input type="button" value="Senden" id="system_send" /></td></tr>'
      html += '</table></div></div>'
      $('#smarthome_data').html(html)
      $('#smarthome_wifiscan_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      $('#smarthome_wifiscan_usb').jqxDropDownList({width: 250, height: 30})
      $('#smarthome_wifiscan_intervall').jqxDropDownList({source: ['15', '60'], width: 250, height: 30})
      $('#smarthome_wifiscan_front, #smarthome_wifiscan_back, #smarthome_wifiscan_back_m').jqxCheckBox({height: 25})
      $('#smarthome_wifiscan_settings input:button').jqxButton({width: 250, height: 40}).css('margin', '10px')
      $('#smarthome_wifiscan_settings input:button').off('click')
      $('#smarthome_wifiscan_settings input:button').on('click', function() {
        var frontend = $('#smarthome_wifiscan_front').jqxCheckBox('val'),
        backend = $('#smarthome_wifiscan_back').jqxCheckBox('val'),
        master_backend = $('#smarthome_wifiscan_back_m').jqxCheckBox('val'),
        port = "",
        intervall = 15
        var item = $("#smarthome_wifiscan_usb").jqxDropDownList('getSelectedItem')
        if (item != undefined) {
          port = item.label
        }
        var item = $("#smarthome_wifiscan_intervall").jqxDropDownList('getSelectedItem')
        if (item != undefined) {
          intervall = parseInt(item.label)
        }
        window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'wifiscan', 'tab': 'setup',
                       'frontend': frontend, 'backend': backend, 'master_backend': master_backend, 'port': port,
                       'intervall': intervall}}, function(data) {
        })
      })
      window.sm_b.wifiscan.call_tab(0)
      $('#smarthome_wifiscan_tab').off('selected')
      $('#smarthome_wifiscan_tab').on('selected', function(ev) {
        var selectedTab = ev.args.item
        window.sm_b.wifiscan.call_tab(selectedTab)
      })
      window.smcall({'client': 'smarthome.backend', 'cmd':'get_options', 'data': {'server': window.module.smarthome.server,
                     'modul':'wifiscan', 'tab': ''}}, function(data) {
        if (!data.data.master_backend) {
          $('#smarthome_wifiscan_tab').jqxTabs('disableAt', 1)
          $('#smarthome_wifiscan_tab').jqxTabs('disableAt', 2)
        }
        window.sm_b.wifiscan.call_tab($("#smarthome_wifiscan_tab").val())
      })
    },
    call_tab : function(id) {
      if (id==1) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'wifiscan', 'tab': 'st'}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'mac', type: 'string' },
              { name: 'vendor', type: 'string' },
              { name: 'ap_ssid', type: 'string' },
              { name: 'ap', type: 'string' },
              { name: 'last', type: 'string' },
              { name: 'hosts', type: 'string' },
              { name: 'aktiv', type: 'bool' },
              { name: 'name', type: 'string' },
              { name: 'out', type: 'bool' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $('#smarthome_wifiscan_st').parent().css('overflow', 'hidden')
          $("#smarthome_wifiscan_st").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            editable: true,
            columns: [
              { text: 'MAC', datafield: 'mac', editable: false, width: 150},
              { text: 'Name', datafield: 'name'},
              { text: 'vendor', editable: false, datafield: 'vendor', width: 150},
              { text: 'Accesspoint', editable: false, datafield: 'ap_ssid', width: 150},
              { text: 'Host', editable: false, datafield: 'hosts', width: 200},
              { text: 'Zeit', editable: false, datafield: 'last', width: 100, cellsalign: 'right'},
              { text: 'Aktiv', editable: false, datafield: 'aktiv', width: 100, cellsalign: 'center', columntype: 'checkbox'},
              { text: 'Send', datafield: 'out', width: 100, cellsalign: 'center', columntype: 'checkbox'}
            ]
          })
          $("#smarthome_wifiscan_st").off('cellendedit')
          $("#smarthome_wifiscan_st").on('cellendedit', function (event) {
            var args = event.args
            var data = $('#smarthome_wifiscan_st').jqxGrid('getrowdatabyid', args.rowindex)
            window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                           'modul':'wifiscan', 'tab': 'st',
                           'mac':data.mac, 'field': args.datafield, 'value': args.value}}, function(data) {
            })
          })
        })
      } else if (id==2) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'wifiscan', 'tab': 'ap'}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'mac', type: 'string' },
              { name: 'ssid', type: 'string' },
              { name: 'rssi', type: 'float' },
              { name: 'last', type: 'string' },
              { name: 'hosts', type: 'string' },
              { name: 'out', type: 'bool' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $('#smarthome_wifiscan_ap').parent().css('overflow', 'hidden')
          $("#smarthome_wifiscan_ap").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            editable: true,
            columns: [
              { text: 'SSID', editable: false, datafield: 'ssid', width: 200 },
              { text: 'mac', datafield: 'mac', editable: false},
              { text: 'RSSI', editable: false, datafield: 'rssi', cellsalign: 'right', width: 150},
              { text: 'Host', editable: false, datafield: 'hosts', width: 200},
              { text: 'Zeit', editable: false, datafield: 'last', width: 100, cellsalign: 'right'},
              { text: 'Send', datafield: 'out', width: 100, cellsalign: 'center', columntype: 'checkbox'}
            ]
          })
          $("#smarthome_wifiscan_ap").off('cellendedit')
          $("#smarthome_wifiscan_ap").on('cellendedit', function (event) {
            var args = event.args
            var data = $('#smarthome_wifiscan_ap').jqxGrid('getrowdatabyid', args.rowindex)
            window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                           'modul':'wifiscan', 'tab': 'ap',
                           'mac':data.mac, 'field': args.datafield, 'value': args.value}}, function(data) {
            })
          })
        })
      } else if (id == 3) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'wifiscan', 'tab': 'setup'}}, function(data) {
          $("#smarthome_wifiscan_usb").jqxDropDownList('clear')
          var len = data.data.ports.length
          for (var i=0; i<len; i++) {
            $("#smarthome_wifiscan_usb").jqxDropDownList('addItem', data.data.ports[i])
          }
          var item = $("#smarthome_wifiscan_usb").jqxDropDownList('getItemByValue', data.data.port)
          if (item == undefined) {
            $("#smarthome_wifiscan_usb").jqxDropDownList('selectIndex', -1 )
          } else {
            $("#smarthome_wifiscan_usb").jqxDropDownList('selectIndex', item.index )
          }
          var item = $("#smarthome_wifiscan_intervall").jqxDropDownList('getItemByValue', data.data.intervall)
          if (item == undefined) {
            $("#smarthome_wifiscan_intervall").jqxDropDownList('selectIndex', -1 )
          } else {
            $("#smarthome_wifiscan_intervall").jqxDropDownList('selectIndex', item.index )
          }
          $('#smarthome_wifiscan_front').jqxCheckBox('val', data.data.frontend)
          $('#smarthome_wifiscan_back_m').jqxCheckBox('val', data.data.master_backend)
          $('#smarthome_wifiscan_back').jqxCheckBox('val', data.data.backend)
        })
      } else {
        console.error(id)
      }
    }
  }
  window.sm_b.wifiscan = wifi
  return wifi
})

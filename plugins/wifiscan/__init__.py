import copy
import os

import lib.plugin as plugin

from plugins.wifiscan import db

class wifiscan(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.use_backend = False
        self.use_frontend = False
        self.running = False

    def start(self):
        self.sl.log.info('Start')
        if not self.running:
            self.running = True
            db.init(self.sl)
            if self.config['backend']:
                self.use_backend = True
                from plugins.wifiscan import scanner
                scanner.init(self.sl, self.config['port'], self.config['intervall'], self.new_data)
            if self.config['frontend']:
                self.use_frontend = True
                from plugins.wifiscan import master
                master.init(self.sl)
        if 'timer' in self.sl.plugins.list:
            self.sl.plugins.timer.start()
            self.timer = self.sl.plugins.timer.do(3600, db.save)

    def stop(self):
        self.sl.log.info('Stop')
        db.save()
        if self.use_backend:
            from plugins.wifiscan import scanner
            scanner.stop()
   
    def sm_backend_menu(self):
        return {'name': 'wifiscan', 'friendly':'WIFI-Scanner'}
    
    def sm_api(self, data):
        self.sl.log.debug(data)
        cmd = data['cmd']
        tab = data['data']['tab']
        if cmd == 'get_options':
           data['data'] = {'master_backend': self.config['master_backend']}
        elif tab == 'setup':
           data['data'] = self.setup_data(cmd, data['data'])
        elif tab == 'ap':
           from plugins.wifiscan import master
           data['data'] = master.api_ap(cmd, data['data'])
        elif tab == 'st':
           from plugins.wifiscan import master
           data['data'] = master.api_st(cmd, data['data'])
        return data

def setup_data(self, cmd, data):
        out = []
        if cmd == 'get_data':
            out = copy.copy(self.config)
            out['ports'] = []
            for dev in os.listdir('/dev/'):
                if dev.startswith('ttyUSB'):
                    out['ports'].append(dev)
        elif cmd == 'set_data':
            self.sl.config.plugins[self.name]['frontend'] = data['frontend']
            self.sl.config.plugins[self.name]['backend'] = data['backend']
            self.sl.config.plugins[self.name]['port'] = data['port']
            self.sl.config.plugins[self.name]['intervall'] = data['intervall']
            self.sl.config.save()
        return out

    def new_data(self, data):
        for e in data['aps']:
            name = str(e[0])
            mac = str(e[5])
            rssi = int(str(e[3]))
            db.ap_lokal_data(name, mac, rssi)
        for e in data['stations']:
            mac = str(e[0])
            vendor = str(e[3])
            ap_id = int(str(e[5]))
            ap = str(data['aps'][ap_id][0])
            ap_mac = str(data['aps'][ap_id][5])
            last = str(e[6])
            if last == '<1sec':
                db.st_lokal_data(mac, vendor, ap_mac)

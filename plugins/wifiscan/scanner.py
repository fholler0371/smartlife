import os
import sys
import time
import json
from threading import Thread 

try:
    import serial
except:
    os.popen(sys.executable + ' -m pip install pyserial --upgrade').read()
    import serial

_sl = None
_scanner = None

class wscanner(Thread):
    def __init__(self, sl, port, intervall, cb):
        Thread.__init__(self)
        self.running = True
        self.sl = sl
        self.port = port
        self.intervall = intervall
        self.cb = cb

    def run(self):
        while self.running:
            try:
                ser = serial.Serial('/dev/' + self.port, 115200)
                ser.write('stopap\n'.encode())
                ser.write(('scan -t '+str(self.intervall)+'\n').encode())
                time.sleep(self.intervall+3)
                ser.read(ser.in_waiting)
                ser.write('print /scan.json\n'.encode())
                time.sleep(0.5)
                res = ser.read(ser.in_waiting).decode().split('\r\n')
                if res[1] != "" and res[1][:1] == "{":
                    data = json.loads(res[1])
                    self.cb(data)
                ser.close()
            except Exception as e:
                self.sl.log.error(repr(e))
                time.sleep(15)
            time.sleep(1)

def init(sl, port, intervall, cb):
    global _sl, _scanner
    _sl = sl
    _sl.log.info('init')
    os.popen('sudo chmod 777 /dev/' + port).read()
    scanner = wscanner(_sl, port, intervall, cb)
    scanner.start()
    _scanner = scanner

def stop():
    global _sl, _scanner
    _sl.log.info('stop')
    _scanner.running = False

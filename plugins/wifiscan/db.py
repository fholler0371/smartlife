import time
import copy
import sqlite3
from threading import Thread

_sl = None
_ap = {}
_apg = {}
_stg = {}
_st = {}
_file = ''

class sendout(Thread):
    def __init__(self, sl, data):
       Thread.__init__(self)
       self.sl = sl
       self.data = data

    def run(self):
       try:
           self.sl.plugins.sensors.new_data(self.data)
       except Exception as e:
           self.sl.log.error(repr(e))

def init(sl):
    global _sl, _file, _st, _ap, _apg, _stg
    _sl = sl
    _file = sl.const.path_var + '/db'
    try:
        os.mkdir(_file)
    except:
        x=1
    _file += '/wifiscan.sqlite'
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS st_lokal (mac text, vendor text, ap text, last integer, resend integer)"
    cur.execute(sql)
    sql = "CREATE TABLE IF NOT EXISTS ap_lokal (mac text, ssid text, rssi text, last integer, resend integer)"
    cur.execute(sql)
    sql = "CREATE TABLE IF NOT EXISTS ap_global (mac text, ssid text, rssi text, last integer, hosts text, out integer)"
    cur.execute(sql)
    sql = "CREATE TABLE IF NOT EXISTS st_global (mac text, vendor text, ap text, last integer, hosts text, aktiv integer, name text, out integer)"
    cur.execute(sql)
    conn.commit()
    sql = "select mac, vendor, ap, last, resend from st_lokal order by mac"
    cur.execute(sql)
    st_lokal = cur.fetchall()
    sql = "select mac, ssid, rssi, last, resend from ap_lokal order by ssid"
    cur.execute(sql)
    ap_lokal = cur.fetchall()
    sql = "select mac, ssid, rssi, last, hosts, out from ap_global order by ssid"
    cur.execute(sql)
    ap_global = cur.fetchall()
    sql = "select mac, vendor, ap, last, hosts, aktiv, name, out from st_global order by mac"
    cur.execute(sql)
    st_global = cur.fetchall()
    conn.close()
    for rec in st_lokal:
        _st[mac] = {'mac': rec[0], 'vendor': rec[1], 'ap': rec[2], 'last': rec[3], 'resend': rec[4]}
    for rec in ap_lokal:
        _ap[rec[0]] = {'mac': rec[0], 'ssid': rec[1], 'rssi': float(rec[2]), 'last': rec[3], 'resend': rec[4]}
    for rec in ap_global:
        _apg[rec[0]] = {'mac': rec[0], 'ssid': rec[1], 'rssi': float(rec[2]), 'last': rec[3], 'hosts': rec[4], 'out': rec[5] == 1}
    for rec in st_global:
        if not(rec[0] in _stg):
            _stg[rec[0]] = {'mac':rec[0],'vendor':rec[1],'ap':rec[2],'last': rec[3],'hosts':rec[4],'aktiv':rec[5]==1,'name':rec[6],'out':rec[7]==1}

def ap_global_data(topic, data):
    global _sl, _apg
    mac = data['mac']
    host = topic.split(':')[0]
    if mac in _apg:
        _apg[mac]['ssid'] = data['ssid']
        _apg[mac]['rssi'] = data['rssi']
        _apg[mac]['last'] = data['last']
        hosts = _apg[mac]['hosts'].split(',')
        if not(host in hosts):
            hosts.append(host)
        _apg[mac]['hosts'] = ','.join(hosts)
    else:
        _apg[mac] = {'mac': mac, 'ssid':data['ssid'], 'rssi':data['rssi'], 'last': data['last'], 'hosts': host, 'name': mac, 'out': False}
    if _apg[mac]['out']:
        if 'sensors' in _sl.plugins.list:
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:ap:ssid:'+mac.replace(':','_'),
                               'value': data['ssid'],
                               'time': data['last']})
            th.start()
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:ap:rssi:'+mac.replace(':','_'),
                               'value': data['rssi'],
                               'time': data['last']})
            th.start()

def st_global_data(topic, data):
    global _sl, _stg
    mac = data['mac']
    host = topic.split(':')[0]
    if mac in _stg:
        _stg[mac]['vendor'] = data['vendor']
        _stg[mac]['ap'] = data['ap']
        _stg[mac]['last'] = data['last']
        _stg[mac]['aktiv'] = True
        hosts = _stg[mac]['hosts'].split(',')
        if not(host in hosts):
            hosts.append(host)
        _stg[mac]['hosts'] = ','.join(hosts)
        if not(_stg[mac]['name']) or  _stg[mac]['name'] == '':
            _stg[mac]['name'] = mac
    else:
        _stg[mac] = {'mac': mac, 'vendor':data['vendor'], 'ap':data['ap'], 'last': data['last'], 'hosts': host, 'aktiv': True, 'name': mac, 'out': False}
    if _stg[mac]['out']:
        if 'sensors' in _sl.plugins.list:
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:st:name:'+mac.replace(':','_'),
                               'value': _stg[mac]['name'],
                               'time': data['last']})
            th.start()
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:st:state:'+mac.replace(':','_'),
                               'value': True,
                               'time': data['last']})
            th.start()
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:st:ap:'+mac.replace(':','_'),
                               'value': _apg[data['ap']]['ssid'],
                               'time': data['last']})
            th.start()

def st_global_data_del(topic, data):
    global _sl, _stg
    mac = data['mac']
    host = topic.split(':')[0]
    if mac in _stg:
        _stg[mac]['vendor'] = data['vendor']
        _stg[mac]['ap'] = data['ap']
        _stg[mac]['last'] = data['last']
        _stg[mac]['aktiv'] = False
        hosts = _stg[mac]['hosts'].split(',')
        if not(host in hosts):
            hosts.append(host)
        _stg[mac]['hosts'] = ','.join(hosts)
        if not(_stg[mac]['name']) or  _stg[mac]['name'] == '':
            _stg[mac]['name'] = mac
    else:
        _stg[mac] = {'mac': mac, 'vendor':data['vendor'], 'ap':data['ap'], 'last': data['last'], 'hosts': host, 'aktiv': False, 'name': mac, 'out': False}
    if _stg[mac]['out']:
        if 'sensors' in _sl.plugins.list:
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:st:name:'+mac.replace(':','_'),
                               'value': _stg[mac]['name'],
                               'time': data['last']})
            th.start()
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:st:state:'+mac.replace(':','_'),
                               'value': False,
                               'time': data['last']})
            th.start()
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:st:ap:'+mac.replace(':','_'),
                               'value': _apg[data['ap']]['ssid'],
                               'time': data['last']})
            th.start()

def ap_lokal_data(ssid, mac, rssi):
    global _sl, _ap
    if not(mac in _ap):
        _ap[mac] = {'mac': mac, 'ssid': ssid, 'rssi': rssi, 'last': int(time.time()), 'resend': int(time.time())}
        _sl.log.debug('New AP: '+ssid)
        if 'sensors' in _sl.plugins.list:
            th = sendout(_sl, {'name': _sl.const.host+':wifiscan:new_ap',
                               'value': _ap[mac],
                               'time': int(time.time())})
            th.start()
    else:
        _ap[mac]['ssid'] = ssid
        _ap[mac]['rssi'] = rssi
        _ap[mac]['last'] = int(time.time())
    for entry in _ap:
        if _ap[entry]['resend'] < time.time()-3600:
            _sl.log.debug('Resend AP: '+entry)
            if 'sensors' in _sl.plugins.list:
                th = sendout(_sl, {'name': _sl.const.host+':wifiscan:resend_ap',
                                   'value': _ap[entry],
                                   'time': int(time.time())})
                th.start()
            _ap[entry]['resend'] = int(time.time())

def st_lokal_data(mac, vendor, ap):
    global _sl, _st
    if not(mac in _st):
        _st[mac] = {'mac': mac, 'vendor': vendor, 'ap': ap, 'last': int(time.time()), 'resend': int(time.time())}
        _sl.log.debug('New St: '+mac)
        if 'sensors' in _sl.plugins.list:
        th = sendout(_sl, {'name': _sl.const.host+':wifiscan:new',
                           'value': _st[mac],
                           'time': int(time.time())})
        th.start()
    else:
        _st[mac]['vendor'] = vendor
        _st[mac]['ap'] = ap
        _st[mac]['last'] = int(time.time())
    st = copy.copy(_st)
    for entry in st:
        if st[entry]['resend'] < time.time()-3600:
            _sl.log.debug('Resend St: '+entry)
            if 'sensors' in _sl.plugins.list:
                th = sendout(_sl, {'name': _sl.const.host+':wifiscan:resend',
                                   'value': _st[entry],
                                   'time': int(time.time())})
                th.start()
            _st[entry]['resend'] = int(time.time())
        if st[entry]['last'] < time.time()-300:
            _sl.log.debug('Delete St: '+entry)
            if 'sensors' in _sl.plugins.list:
                th = sendout(_sl, {'name': _sl.const.host+':wifiscan:delete',
                                   'value': _st[entry],
                                   'time': int(time.time())})
                th.start()
            del _st[entry]

def save(job=None):
    global _sl, _file, _st, _ap, _apg
    _sl.log.info('save')
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "delete from st_lokal"
    cur.execute(sql)
    for rec in _st:
        data = _st[rec]
        sql = "insert into st_lokal (mac, vendor, ap, last, resend) values ('"+data['mac']+"', '"+data['vendor']+"', '"+data['ap']+"', "
        sql += "'"+str(data['last'])+"', '"+str(data['resend'])+"')"
        cur.execute(sql)
    conn.commit()
    sql = "delete from ap_lokal"
    cur.execute(sql)
    for rec in _ap:
        data = _ap[rec]
        sql = "insert into ap_lokal (mac, ssid, rssi, last, resend) values ('"+data['mac']+"', '"+data['ssid']+"', '"+str(data['rssi'])+"', "
        sql += "'"+str(data['last'])+"', '"+str(data['resend'])+"')"
        cur.execute(sql)
    conn.commit()
    sql = "delete from ap_global"
    cur.execute(sql)
    for rec in _apg:
        ddata = _apg[rec]
        out = '0'
        if data['out']:
            out = '1'
        sql = "insert into ap_global (mac, ssid, rssi, last, hosts, out) values ('"+data['mac']+"', '"+data['ssid']+"', '"+str(data['rssi'])+"', "
        sql += "'"+str(data['last'])+"', '"+data['hosts']+"', '"+out+"')"
        cur.execute(sql)
    conn.commit()
    sql = "delete from st_global"
    cur.execute(sql)
    for rec in _stg:
        data = _stg[rec]
        aktiv = '0'
        if data['aktiv']:
            aktiv = '1'
        out = '0'
        if data['out']:
            out = '1'
        if not('name' in data):
            data['name'] = data['mac']
        try:
            sql = "insert into st_global (mac, vendor, ap, last, hosts, aktiv, name, out) values ('"+data['mac']+"', '"+data['vendor']+"', '"+data['ap']
            sql += "', '"+str(data['last'])+"', '"+data['hosts']+"', '"+aktiv+"', '"+data['name']+"', '"+out+"')"
            cur.execute(sql)
        except Exception as e:
            _sl.log.error(_stg[rec])
            _sl.log.error(repr(e))
    conn.commit()
    conn.close()


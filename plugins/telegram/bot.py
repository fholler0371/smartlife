import os
import sys

try:
    from telegram.ext.updater import Updater
except:
    os.popen(sys.executable + ' -m pip install python-telegram-bot --upgrade').read()
    from telegram.ext.updater import Updater

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.message import Message
from telegram.update import Update
from telegram.ext.callbackcontext import CallbackContext
from telegram.ext.commandhandler import CommandHandler
from telegram.ext.updater import Updater
from telegram.replykeyboardmarkup import ReplyKeyboardMarkup
from telegram.replykeyboardremove import ReplyKeyboardRemove
from telegram.callbackquery import CallbackQuery
from telegram.ext.callbackqueryhandler import CallbackQueryHandler
from telegram import Bot

from threading import Thread

_sl = None
_key = ""
_updater = None
_th = None
_chat = ""
_last_menu = []

class telegramThread(Thread):
    def __init__(self, updater):
        Thread.__init__(self)
        self.updater = updater
    def run(self):
        self.updater.start_polling()

def init(sl, key, chat):
    global _sl, _key, _updater, _th, _chat, _bot
    _sl = sl
    _key = key
    _chat = chat
    updater: Updater = Updater(key, use_context=True)
    updater.dispatcher.add_handler(CommandHandler('start', dostart))  # handling /start command
    updater.dispatcher.add_handler(CommandHandler('remove', doremove))  # handling /remove command
    updater.dispatcher.add_handler(CommandHandler('user', douser))  # handling /user command
    updater.dispatcher.add_handler(CommandHandler('module', domodule))  # handling /user command
    updater.dispatcher.add_handler(CallbackQueryHandler(button))
    _updater = updater
    th = telegramThread(updater)
    th.start()
    _bot = Bot(key)

def stop():
    global _updater
    if _updater:
        _updater.stop()

def send(text):
    global _bot, _chat
    _bot.sendMessage(_chat, text)

def dostart(update: Update, context: CallbackContext):
    kbd_layout = [['/module', '/user'], ['/start', '/remove']]
    kbd = ReplyKeyboardMarkup(kbd_layout, one_time_keyboard=True)
    update.message.reply_text(text="Bitte wählen", reply_markup=kbd)

def doremove(update: Update, context: CallbackContext):
    reply_markup = ReplyKeyboardRemove()
    update.message.reply_text(text="Menü entfernt", reply_markup=reply_markup)

def douser(update: Update, context: CallbackContext):
    update.message.reply_text(text="User-Id: "+str(update.message.from_user.id))

def domodule(update: Update, context: CallbackContext):
    global _chat
    if str(_chat) != str(update.message.from_user.id):
        update.message.reply_text(text="Heute möchte ich nicht chatten")
    else:
        global _sl, _last_menu
        items = []
        for name in _sl.plugins.list:
            pl = _sl.plugins.list[name]
            if hasattr(pl, 'tele_menu'):
                items.append(pl.tele_menu())
        _last_menu = items
        if len(items) > 0:
             keyboard = []
             for item in items:
                keyboard.append([InlineKeyboardButton(item[0], callback_data=item[1])])
             reply_markup = InlineKeyboardMarkup(keyboard)
             update.message.reply_text('Bitte wählen:', reply_markup=reply_markup)
        else:
            update.message.reply_text(text="Keine Einträge")

def button(update, context):
    query = update.callback_query
    query.answer()
    global _sl, _chat, _last_menu
    if str(_chat) != str(update.callback_query.from_user.id):
        query.edit_message_text(text="Heute möchte ich nicht chatten")
    else:
        teyt = ""
        for entry in _last_menu:
            if entry[1] == query.data:
                text = entry[0]
        if text == "":
            query.edit_message_text(text="Auswahl: ???")
        else:
            query.edit_message_text(text="Auswahl: "+text)
            name, parameter = query.data.split(':',1)
            if name in _sl.plugins.list:
                pl = _sl.plugins.list[name]
                if hasattr(pl, 'tele_api'):
                     pl.tele_api(parameter)

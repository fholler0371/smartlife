define(['jqxtabs', 'jqxinput', 'jqxbutton'], function() {
  var telegram = {
    init : function() {
      html = '<div id="smarthome_telegram_tab"><ul><li>Einstellungen</li></ul>'
      html += '<div><table>'
      html += '<tr><td>API</td><td><input id="smarthome_telegram_api"></td><td></td></tr>'
      html += '<tr><td>Message-ID</td><td><input id="smarthome_telegram_msg"></td><td></td></tr>'
      html += '<tr><td><td><td></td><td><input type="button" value="Senden" id="system_send" /></td></tr>'
      html += '</table></div></div>'
      $('#smarthome_data').html(html)
      $('#smarthome_telegram_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      $('#smarthome_telegram_api, #smarthome_telegram_msg').jqxInput({width: 250, height: 30 })
      $('#smarthome_telegram_tab input:button').jqxButton({width: 250, height: 40}).css('margin', '10px')
      $('#smarthome_telegram_tab input:button').off('click')
      $('#smarthome_telegram_tab input:button').on('click', window.sm_b.telegram.send)
      window.sm_b.telegram.call_tab(0)
    },
    call_tab : function(id) {
      if (id == 0) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'telegram', 'tab': 'setup'}}, function(data) {
          $('#smarthome_telegram_api').jqxInput('val', data.data.api)
          $('#smarthome_telegram_msg').jqxInput('val', data.data.user)
        })
      }
    },
    send: function() {
      var api = $('#smarthome_telegram_api').jqxInput('val')
      var user = $('#smarthome_telegram_msg').jqxInput('val')
      window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                     'modul':'telegram', 'tab': 'setup', 'api': api, 'user': user}}, function(data) {
      })
    }
  }
  window.sm_b.telegram = telegram
  return telegram
})

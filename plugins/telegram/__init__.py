import lib.plugin as plugin

from plugins.telegram import bot

class telegram(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.running = False

    def start(self):
        self.sl.log.info('Start')
        if not self.running:
            self.running = True
            if self.config['key'] != "":
                bot.init(self.sl, self.config['key'], self.config['bot'])

    def stop(self):
        self.sl.log.info('Stop')

    def sm_backend_menu(self):
        return {'name': 'telegram', 'friendly':'Telegram'}

    def sm_api(self, data):
        if data['data']['tab'] == 'setup':
            if data['cmd'] == 'get_data':
                data['data'] = {'api': self.config['key'],
                                'user': self.config['bot']}
                if data['data']['api'] != "":
                    data['data']['api'] = "*****"+data['data']['api'][5:]
                if data['data']['user'] != "":
                    data['data']['user'] = "*****"+data['data']['user'][5:]
            elif data['cmd'] == 'set_data':
                if not (data['data']['api'] == "" or data['data']['api'].startswith("*****")):
                    self.sl.config.plugins[self.name]['key'] = data['data']['api']
                if not (data['data']['user'] == "" or data['data']['user'].startswith("*****")):
                    self.sl.config.plugins[self.name]['bot'] = data['data']['user']
                self.sl.config.save()
                data['data'] = {}
        return data

    def send(self, text):
        bot.send(text)

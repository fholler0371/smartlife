import json
import time

from plugins.smarthome import system

import lib.plugin as plugin

class smarthome(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        if self.config['server_friendly'] == "":
            sl.config.plugins[self.name]['server_friendly'] = sl.const.host
            sl.config.save()
        self.running = False

    def main_menu(self, keyword):
        self.sl.log.info('Main Menu')
        if 'smarthome.backend' == keyword:
            if self.config['master']:
                out = []
                if len(self.config['clients']) > 0:
                    out.append({'label': 'SmartHome - Backend', 'sub': []})
                    for entry in self.config['clients']:
                        out[0]['sub'].append({'label': entry['name'], 'mod': 'smarthome', 'p1':'backend', 'p2': entry['ip']})
                out.append({'label': 'SmartHome - Scan', 'mod': 'smarthome', 'p1':'scan'})
                return out
            else:
                return {'label': 'SmartHome - Backend', 'mod': 'smarthome', 'p1':'backend'}

    def start(self):
        if not self.running:
            self.sl.log.info('start')
            if 'udp_broadcast' in self.sl.plugins.list:
                self.sl.plugins.udp_broadcast.start()

    def api(self, data):
        server = data['data']['server']
        if server == 'master' or server == self.sl.const.ip.split('/')[0]:
            if data['cmd'] == 'get_module':
                data['data'] = {'module':[{'name': 'system', 'friendly':'System', 'default': True}], 'friendly':self.config["server_friendly"]}
                for plugin in self.sl.plugins.list:
                    pl = self.sl.plugins.list[plugin]
                    if hasattr(pl, 'sm_backend_menu'):
                        data['data']['module'].append(pl.sm_backend_menu())
            elif data['cmd'] == 'scan':
                if 'udp_broadcast' in self.sl.plugins.list:
                    self.scan_data = []
                    self.sl.plugins.udp_broadcast.sent('smarthome:scan:'+json.dumps({}))
                    time.sleep(3)
                    self.sl.config.plugins[self.name]['clients'] = self.scan_data
                    self.sl.config.save()
                data['data'] = 'scanning'
            else:
                if data['data']['modul'] == 'system':
                    data = system.api(self, self.sl, data)
                else:
                    modul = data['data']['modul']
                    pl = self.sl.plugins.list[modul]
                    if pl and hasattr(pl, 'sm_api'):
                        data = pl.sm_api(data)
        return data

    def udp(self, mesg):
        cmd, data = mesg.split(':', 1)
        if cmd == 'scan':
            self.sl.log.info('receive scan request')
            self.sl.plugins.udp_broadcast.sent('smarthome:scan_answer:'+
                                               json.dumps({'ip':self.sl.const.ip.split('/')[0], 'name':self.config['server_friendly']}))
        elif cmd == 'scan_answer':
            self.scan_data.append(json.loads(data))
        else:
            self.sl.log.error('unbekannt udp', mesg)

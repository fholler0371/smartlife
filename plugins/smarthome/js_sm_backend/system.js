define(['jqxtabs', 'jqxdata', 'jqxdatatable', 'jqxpanel', 'jqxinput', 'jqxcheckbox', 'jqxdropdownlist', 'jqxbutton',
        'jqxgrid_selection', 'jqxgrid', 'jqxgrid_edit'], function() {
  var system = {
    init : function() {
      html = '<div id="smarthome_system_tab"><ul><li>Status</li><li>Einstellungen</li><li>Aktionen</li><li>Plugins</li><li>Log-Datei</li></ul>'
      html += '<div id="smarthome_system_status">'
      html += '<div id="smarthome_system_status_table" style="margin:10px;"></div></div><div id="smarthome_system_settings">'
      html += '<table><tr><td><b>Servername:</b></td><td><input id="smarthome_system_servername" /></td><td></td></tr>'
      html += '<tr><td><b>Hauptserver:</b></td><td><div id="smarthome_system_main"></div></td><td></td></tr>'
      html += '<tr><td><b>Loglevel:</b></td><td><div id="smarthome_system_loglevel"></div></td><td></td></tr>'
      html += '<tr><td></td><td></td><td><input id="smarthome_system_send" type="button" value="Einstellungen senden" /></td></tr></table>'
      html += '</div><div id="smarthome_system_actions"><input type="button" value="Reload" data-id="reload" /></div>'
      html += '<div id="smarthome_system_plugins"><div id="smarthome_system_plugins_tab"></div></div><div id="smarthome_system_log">'
      html += '</div></div>'
      $('#smarthome_data').html(html)
      $('#smarthome_system_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'});
      $('#smarthome_system_tab').off('selected')
      $('#smarthome_system_tab').on('selected', function(ev) {
        var selectedTab = ev.args.item
        window.sm_b.system.call_tab(selectedTab)
      })
      window.sm_b.system.call_tab(0)
      $('#smarthome_system_servername').jqxInput({width:250})
      $('#smarthome_system_main').jqxCheckBox()
      $('#smarthome_system_loglevel').jqxDropDownList({width:250, source : ['debug', 'info', 'warning', 'error', 'critical'], itemHeight: 25,
                                                  dropDownHeight: 130})
      $('#smarthome_system_send').jqxButton()
      $('#smarthome_system_send').off('click')
      $('#smarthome_system_send').on('click', function() {
        var name = $('#smarthome_system_servername').jqxInput('val'),
            main = $('#smarthome_system_main').jqxCheckBox('val'),
            log = $('#smarthome_system_loglevel').jqxDropDownList('val')
        window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'system', 'tab': 'setup', 'name': name, 'main': main, 'log': log}}, function(data) {
        })
      })
      $('#smarthome_system_actions > input').jqxButton()
      $('#smarthome_system_actions > input').off('click')
      $('#smarthome_system_actions > input').on('click', window.sm_b.system.action_click)
    },
    action_click : function(ev) {
      var id = $(ev.currentTarget).data('id')
      window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                     'modul':'system', 'tab': 'action', 'do': id}}, function(data) {
      })
    },
    call_tab : function(id) {
      if (id == 0) {
         window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server, 
                        'modul':'system', 'tab': 'status'}}, function(data) {
           var source = {
             localData: data.data,
             dataType: "array",
             dataFields: [
               { name: 'label', type: 'string' },
               { name: 'data', type: 'string' }
             ]
           }
           var dataAdapter = new $.jqx.dataAdapter(source)
           $('#smarthome_system_status_table').jqxDataTable({
             source: dataAdapter,
             columns: [
               { text: 'Bezeichnung', dataField: 'label', 'width': 200 },
               { text: 'Wert', dataField: 'data' , 'cellsAlign': 'right', 'width': 350}
             ]
           })
         })
      } else if (id == 1) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'system', 'tab': 'setup'}}, function(data) {
          $('#smarthome_system_servername').jqxInput('val', data.data.name)
          if (data.data.main) {
            $('#smarthome_system_main').jqxCheckBox('check')
          } else {
            $('#smarthome_system_main').jqxCheckBox('uncheck')
          }
          var item =  $('#smarthome_system_loglevel').jqxDropDownList('getItemByValue', data.data.log)
          if (item != undefined && item.index >= 0) {
            $("#smarthome_system_loglevel").jqxDropDownList('selectIndex', item.index)
          }
        })
      } else if (id == 3) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                        'modul':'system', 'tab': 'plugins'}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'name', type: 'string' },
              { name: 'friendly', type: 'string' },
              { name: 'active', type: 'bool' },
              { name: 'background', type: 'bool' },
              { name: 'description', type: 'string' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          var cellbeginedit = function (row, datafield, columntype, value) {
            var data = $('#smarthome_system_plugins_tab').jqxGrid('getrowdatabyid', row);
            return !data.background
          }
          $("#smarthome_system_plugins_tab").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            editable: true,
            columns: [
              { text: 'Plugin', editable: false, datafield: 'name', width: 200 },
              { text: 'Name',  editable: false, datafield: 'friendly', width: 200 },
              { text: 'Aktiv', columntype: 'checkbox', cellbeginedit: cellbeginedit, datafield: 'active', width: 100 },
              { text: 'Background',  editable: false, columntype: 'checkbox', datafield: 'background', width: 100 },
              { text: 'Beschreibung',  editable: false, datafield: 'description'}
            ]
          })
          $("#smarthome_system_plugins_tab").on('cellendedit', function (event) {
            var args = event.args
            var data = $('#smarthome_system_plugins_tab').jqxGrid('getrowdatabyid', args.rowindex);
            window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                          'modul':'system', 'tab': 'plugins',
                          'name':data.name, 'value': args.value}}, function(data) {
            })
          })
        })
      } else if (id == 4) {
        $('#smarthome_system_log').css('overflow', 'hidden')
        $('#smarthome_system_log').html('<div id="log_liste"></div>')
        $('#log_liste').jqxPanel({ width: '100%', height: '100%'})
        $('#panelContentlog_liste').css('font-family', "'Fira Code', monospace")
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                        'modul':'system', 'tab': 'log'}}, function(data) {
          $('#log_liste').jqxPanel('append', data.data.join('<br>').split(' ').join('&nbsp;'))
        })
      }
    }
  }
  if (undefined == window.sm_b) {
    window.sm_b = {}
  }
  window.sm_b.system = system
  return system
})

import os
import sys
import time
import json
import lib.sl_logger as sl_logger
import traceback 

try:
    import psutil
except:
    os.popen(sys.executable + ' -m pip install psutil').read()
    import psutil

def get_plugins(self, sl, cmd, data):
    if cmd == 'set_data':
        name = data['name']
        if data['value']:
            if not name in sl.config.plugins:
                if '__'+name in sl.config.plugins:
                    sl.config.plugins[name] = sl.config.plugins['__'+name]
                    del sl.config.plugins['__'+name]
                else:
                    sl.config.plugins[name] = {}
        else:
            if name in sl.config.plugins:
                if '__'+name in sl.config.plugins:
                    del sl.config.plugins['__'+name]
                del sl.config.plugins[name]
        sl.config.save()
        return data
    elif cmd == 'get_data':
        out = []
        for name in os.listdir(sl.const.path + '/plugins'):
            m_path = sl.const.path + '/plugins/' + name + '/manifest.json'
            if os.path.isfile(m_path):
                f = open(m_path, 'r')
                manifest = json.loads(f.read())
                f.close()
                row = {'name': name, 'description': manifest['description'], 'background': manifest['background'], 'friendly': manifest['friendly'],
                       'active': False}
                if name in sl.plugins.list:
                    row['active'] = True
                out.append(row)
    return out

def get_action(self, sl, cmd, data):
    sl.log.info('system action')
    if cmd == 'set_data':
        if data['do'] == 'reload':
            sl.log.critical('Call Stop')
            try:
                sl.stop()
            except Exception as e:
                sl.log.error(repr(e))
                exc_type, exc_value, exc_tb = sys.exc_info()
                filename, line_num, func_name, text = traceback.extract_tb(exc_tb)[-1]
                sl.log.error(filename)
                os._exit(1)
    return data

def get_setup(self, sl, cmd, data):
    if cmd == 'set_data':
        sl.config.plugins[self.name]['server_friendly'] = data['name']
        sl.config.plugins[self.name]['master'] = data['main']
        sl.config.logger['level'] = data['log']
        sl_logger.update(sl, sl.log, sl.config.logger)
        sl.config.save()
    elif cmd == 'get_data':
        out = {}
        out['name'] = self.config['server_friendly']
        out['main'] = self.config['master']
        out['log'] = sl.config.logger['level']
        return out
    return data

def get_log(sl, cmd, data):
    if cmd == 'get_data':
        out = []
        path = sl.const.path_var
        file = path + '/log/' + sl.const.name + '.log'
        lines = os.popen('tail -250 '+file).read().split('\n')
        l = len(lines) - 2
        while l > -1:
            out.append(lines[l])
            l -= 1
        return out
    else:
        return data

def get_status(sl, cmd, data):
    if cmd == 'get_data':
        out = []
        try:
           response = os.popen('cat /sys/firmware/devicetree/base/model').read()
           out.append({'label': 'Board', 'data': response})
        except:
            sl.log.error('Konte Pi-Typ nicht ermitteln')
        try:
           response = os.popen('cat /proc/meminfo').read().split('\n')
           value = response[0]
           while value.find('  ') > -1:
               value = value.replace('  ', ' ')
           value = int(value.split(' ')[1])/1024/1024
           out.append({'label': 'Speicher', 'data': ("{:.3f}".format(value)).replace('.', ',') + ' GiB'})
           value = response[1]
           while value.find('  ') > -1:
               value = value.replace('  ', ' ')
           value = int(value.split(' ')[1])/1024/1024
           out.append({'label': 'Feier Speicher', 'data': ("{:.3f}".format(value)).replace('.', ',') + ' GiB'})
        except:
            sl.log.error('Fehler beim Auslesen des Speichers')
        try:
           response = os.popen('cat /sys/class/thermal/thermal_zone0/temp').read()
           out.append({'label': 'Temperatur', 'data': ("{:.1f}".format(int(response)/1000)).replace('.', ',')+' C'})
        except:
            sl.log.error('Konte Temperatur nicht ermitteln')
        out.append({'label': 'Rechnername', 'data': sl.const.host})
        value = psutil.disk_usage(sl.const.path)
        out.append({'label': 'Festplatte', 'data': ("{:.2f}".format(value.free/1024/1024/1024)).replace('.', ',') + ' GiB ' +
                    ("{:.1f}".format(100-value.percent)).replace('.', ',') + '%'})
        value = int(time.time() - psutil.boot_time())
        s = ('0'+str(value % 60))[-2:]
        value = int(value/60)
        s = ('0'+str(value % 60))[-2:] + ':' + s
        value = int(value/60)
        s = ('0'+str(value % 24))[-2:] + ':' + s
        value = int(value/24)
        if value > 0:
            s = str(value) + 'd ' + s
        out.append({'label': 'Laufzeit Rechner', 'data': s})
        value = int(time.time() - sl.const.start_time)
        s = ('0'+str(value % 60))[-2:]
        value = int(value/60)
        s = ('0'+str(value % 60))[-2:] + ':' + s
        value = int(value/60)
        s = ('0'+str(value % 24))[-2:] + ':' + s
        value = int(value/24)
        if value > 0:
            s = str(value) + 'd ' + s
        out.append({'label': 'Laufzeit Programm', 'data': s})
        try:
            response = os.popen('uname -r').read()
            out.append({'label': 'Kernalversion', 'data': response.split('\n')[0]})
        except:
            sl.log.error('Konte Kernal nicht ermitteln')
        try:
            response = os.popen('cat /etc/os-release').read()
            out.append({'label': 'OS-Name', 'data': response.split('"')[1]})
        except:
            sl.log.error('Konte osName nicht ermitteln')
        out.append({'label': 'Python', 'data': sys.version.split(' ')[0]})
        out.append({'label': 'Serien-Nr.', 'data': sl.const.serial})
        out.append({'label': 'IP-Adresse', 'data': sl.const.ip.split('/')[0]})
        try:
            response = os.popen('cat /proc/loadavg').read()
            out.append({'label': 'Systemlast 15min', 'data': response.split(' ')[2].replace('.', ',')})
        except:
            sl.log.error('Last ermitteln')
        return out
    else:
        return data

def api(self, sl, data):
    cmd = data['cmd']
    tab = data['data']['tab']
    if tab == 'status':
        data['data'] = get_status(sl, cmd, data['data'])
    elif tab == 'log':
        data['data'] = get_log(sl, cmd, data['data'])
    elif tab == 'setup':
        data['data'] = get_setup(self, sl, cmd, data['data'])
    elif tab == 'plugins':
        data['data'] = get_plugins(self, sl, cmd, data['data'])
    elif tab == 'action':
        data['data'] = get_action(self, sl, cmd, data['data'])
    else:
        print(cmd, tab)
    return data

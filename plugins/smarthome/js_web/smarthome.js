define(['module', 'jqxlistbox'], function(module) {
  var smarthome = {
    server : 'master',
    module: undefined,
    init : function() {
      if (window.module.smarthome.init_data.p1 == "scan") {
        window.smcall({'client': 'smarthome.backend', 'cmd':'scan', 'data': {'server': window.module.smarthome.server}}, function(data) {
          console.log(window.module.smarthome.init_data)
        })
      } else if (window.module.smarthome.init_data.p1 == "backend") {
        if (window.module.smarthome.init_data.p2 != "undefined") {
          window.module.smarthome.server = window.module.smarthome.init_data.p2
        }
        html = '<div style="width:100%; height:100%;"><div id="smarthome_module"></div>'
        html += '<div id="smarthome_data" style="height:100%; position: absolute; top:0;" ></div>'
        $('#client_area').html(html+'</div>')
        $(window).off('resize', window.module.smarthome.resize)
        $(window).on('resize', window.module.smarthome.resize)
        window.module.smarthome.resize()
        $('#mainTop').off('sizeChange', window.module.smarthome.left_size)
        $('#mainTop').on('sizeChange', window.module.smarthome.left_size)
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_module', 'data': {'server': window.module.smarthome.server}}, function(data) {
          window.module.smarthome.module = data.data.module
          $('#head_title').html(data.data.friendly)
          var l = data.data.module.length
          var source = []
          var selected=-1
          for (var i=0; i<l; i++) {
            var entry = data.data.module[i]
            source.push(entry.friendly)
            if (entry.default) {
              selected = i
            }
          }
          $("#smarthome_module").jqxListBox({ selectedIndex: selected, source: source, width: 250, height: '100%' });
          window.module.smarthome.left_size()
          $("#smarthome_module").off('select')
          $("#smarthome_module").on('select',  window.module.smarthome.select_modul)
          window.module.smarthome.select_modul()
        })
      }
    },
    select_modul : function(ev) {
      var id = $("#smarthome_module").jqxListBox('getSelectedItem').index
      var name = window.module.smarthome.module[id].name
      var paths = {}
      paths['sm_b.'+name] = 'sm_backend/'+name
      requirejs.config({paths:paths})
      requirejs(['sm_b.'+name], function(mod) {
        mod.init()
      })
    },
    stop : function() {
    },
    resize : function() {
      $('#client_area').children().height($('#client_area').height())
    },
    left_size: function() {
      var w = $('#mainTop > .leftMenu').width()
      if ($('#mainTop').hasClass('leftMenuSmall')) {
        $("#smarthome_module").hide()
        $("#smarthome_data").css('left', 0)
        $("#smarthome_data").css('width', '100%')
      } else {
        $("#smarthome_module").show()
        $("#smarthome_module").jqxListBox('width', w)
        $("#smarthome_data").css('left', w)
        $("#smarthome_data").css('width', 'calc( 100% - '+w+'px )')
      }
    }
  }
  smarthome['init_data'] = window.module_const[module.id]
  window.module.smarthome = smarthome
  return smarthome
})

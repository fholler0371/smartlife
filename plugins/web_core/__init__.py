import lib.plugin as plugin

import plugins.web_core.server as server

class web_core(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.server = None

    def start(self):
        self.sl.log.info('Start Plugin')
        port = self.config['port']
        if self.sl.dev:
            port += 100
        self.server = server.server(self.sl, port)
        if 'smarthome' in self.sl.plugins.list:
            self.sl.plugins.smarthome.start()

    def stop(self):
        self.sl.log.info('Stop Plugin')
        if self.server:
            server.stop(self.server)

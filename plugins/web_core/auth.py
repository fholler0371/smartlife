def getSalt(sl, call):
    sl.log.debug('check user: ' + call['data']['user'])
    out = call
    user = call['data']['user']
    out['login'], _name, _packages = sl.plugins.authencitator.check_user(call['data']['user'], call['data']['passwd'])
    if 'token' in out:
        del out['token']
    if 'data' in out:
        del out['data']
    if out['login']:
        out['name'] = _name
        out['token'] = {'timeout': 0, 'packages': _packages, 'user': user}
    return out

def decode(sl, data):
    sl.log.debug('decode')
    return sl.plugins.authencitator.decode(data)

def encode(sl, data):
    sl.log.debug('encode')
    return sl.plugins.authencitator.encode(data)

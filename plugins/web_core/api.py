import json
import urllib.request

from plugins.web_core import auth

def api(sl, ip, call):
    if 'client' in call:
        if 'master' == call['client']:
            if 'get_salt' == call['cmd']:
                out = auth.getSalt(sl, call)
            else:
                out = auth.decode(sl, call)
                out['data'] = []
                if out['login']:
                    if 'get_menu' == out['cmd']:
                        for pl_name in out['token']['packages']:
                            name = pl_name.split('.')[0]
#                            print(name)
                            if name in sl.plugins.list:
                                pl = sl.plugins.list[name]
                                menu = pl.main_menu(pl_name)
                                if menu != None:
                                    if type(menu) == type([]):
                                        for entry in menu:
                                            out['data'].append(entry)
                                    else:
                                        out['data'].append(menu)
                        if 'sm_backend' in out['token']['packages']:
                            pass
            if out['login']:
                out = auth.encode(sl, out)
            else:
                out['token'] = ""
#            print(out)
#            print(sl)
#            print(call)
        else:
            out = call
            if ip == "127.0.0.1":
                out = auth.decode(sl, out)
            if out['login']:
                if call['client'] in call['token']['packages']:
                    name = call['client'].split('.')[0]
                    if name == 'smarthome':
                        if call['data']['server'] == "master" or call['data']['server'] == sl.const.ip.split('/')[0]:
                            if name in sl.plugins.list:
                                pl = sl.plugins.list[name]
                                out = pl.api(out)
                        else:
                            try:
                                sname = 'http://'+call['data']['server']+':'+str(sl.config.plugins['web_core']['port'])
                                sdata = json.dumps(out).encode('utf8')
                                req = urllib.request.Request(sname, data=sdata, headers={'content-type': 'application/json'})
                                response = urllib.request.urlopen(req)
                                out = json.loads(response.read().decode())
                            except Exception as e:
                                sl.log.error(str(e))
                    else:
                        if name in sl.plugins.list:
                            pl = sl.plugins.list[name]
                            out = pl.api(out)
            if out['login']:
                if ip == "127.0.0.1":
                     out = auth.encode(sl, out)
            else:
                out['token'] = ""
        return out
    else:
        return {}

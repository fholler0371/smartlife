import json
import os
import mimetypes

import urllib.request as request

from threading import Thread
from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn

from urllib.parse import unquote
import cgi
import os
import tempfile

from plugins.web_core import api

_sl = None
_port = -1

class last_call(Thread):
    def __init__(self, url):
        Thread.__init__(self)
        self.url = url

    def run(self):
        try:
            request.urlopen(self.url, timeout=10)
        except:
            print()

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
     """Handle requests in a separate thread."""

class webserverHandler(BaseHTTPRequestHandler):
    def __init__(self, sl, *args, **kwargs):
        self.sl = sl
        super().__init__(*args, **kwargs)

    def log_message(self, format, *args):
        self.sl.log.info("\n    %s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))

    def do_POST(self):
        self.sl.log.debug('call from '+self.client_address[0])
        if self.path == '/api-core/upload.php':
            form = cgi.FieldStorage(fp=self.rfile,
                                    headers=self.headers,
                                    environ={'REQUEST_METHOD':'POST', 'CONTENT_TYPE':self.headers['Content-Type']})
            data = form.getvalue('data')
            if type(data) is list:
                l = len(data)
                pdata = json.loads(unquote(data[l-1]))
            else:
                pdata = json.loads(unquote(data))
            pdata['data']['filename'] = form['file'].filename
            dir = self.sl.const.path_var + '/tmp'
            try:
                os.mkdir(dir)
            except:
                x=1
            prefix = pdata['data']['modul'] + '_'
            temp = tempfile.NamedTemporaryFile(mode='wb', prefix=prefix, dir=dir, delete=False)
            pdata['data']['tmpname'] = temp.name
            temp.write(form['file'].file.read())
            temp.close()
            ret = api.api(self.sl, self.address_string(), pdata)
        else:
            content_len = int(self.headers['Content-Length'])
            pdata = self.rfile.read(content_len)
            ret = api.api(self.sl, self.address_string(), json.loads(pdata.decode()))
        data = json.dumps(ret).encode()
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.send_header('Content-Length', str(len(data)))
        self.end_headers()
        self.wfile.write(data)

    def do_GET(self):
        self.sl.log.info('call from '+self.client_address[0])
        if self.path.startswith('/api-core/get_file?_='):
            data = self.path.split('?_=')[1]
            if 'authencitator' in self.sl.plugins.list:
                file = self.sl.const.path_var+'/files/'+self.sl.plugins.authencitator.file_decode(data)
                if os.path.isfile(file):
                    mimeType = mimetypes.guess_type(file)[0]
                    self.send_response(200)
                    self.send_header('Content-type', mimeType)
                    self.end_headers()
                    f = open(file, "rb")
                    self.wfile.write(f.read())
                    f.close()
        self.send_response(200)
        self.send_header('Content-Type', 'text/html')
        self.end_headers()
        self.wfile.write('Sorry'.encode())

class loopThread(Thread):
    def __init__(self, server):
        Thread.__init__(self)
        self.server = server

    def run(self):
        self.server.serve_forever()

def server(sl, port):
    global _sl, _port
    sl.log.info('create webserver')
    _sl = sl
    _port = port
    handler = partial(webserverHandler, sl)
    server = ThreadedHTTPServer(('0.0.0.0', port), handler)
    th = loopThread(server)
    th.start()
    return server

def stop(server):
    global _sl, _port
    _sl.log.info('stop webserver')
    server.shutdown()
    try:
        last_call("http://127.0.0.1:"+str(_port)).start()
    except:
        print()

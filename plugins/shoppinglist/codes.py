import sqlite3

_sl = None
_data = []
_file = ''

try:
    from sqlescapy import sqlescape
except:
    os.popen(sys.executable + ' -m pip install sqlescapy').read()
    from sqlescapy import sqlescape

def init(sl):
    global _sl, _file
    _sl = sl
    file = sl.const.path_var + '/db'
    try:
        os.mkdir(file)
    except:
        print()
    file += '/shoppinglist.sqlite'
    _file = file
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS codes (id integer PRIMARY KEY, code text, product text)"
    cur.execute(sql)
    conn.commit()
    sql = "select id, code, product from codes order by product"
    cur.execute(sql)
    recs = cur.fetchall()
    conn.close()
    for rec in recs:
        _data.append({'id': rec[0], 'code': rec[1], 'product': rec[2]})

def get_data():
    global _data
    return _data

def add_bar_data(lbar):
    global _data, _file, _sl
    if lbar != "":
        conn = sqlite3.connect(_file)
        cur = conn.cursor()
        sql = "select id from codes where code='"+lbar+"'"
        cur.execute(sql)
        rec = cur.fetchone()
        if rec:
            return {'id': -2}
        sql = "insert into codes (code, product) values ('"+lbar+"', 'Neues Produkt')"
        cur.execute(sql)
        conn.commit()
        sql = "select max(id) from codes"
        cur.execute(sql)
        rec = cur.fetchone()
        sql = "select id, code, product from codes where id='"+str(rec[0])+"'"
        cur.execute(sql)
        rec = cur.fetchone()
        conn.close()
        if rec:
            _data.append({'id': rec[0], 'code': rec[1], 'product': rec[2]})
            return {'id': rec[0], 'code': rec[1], 'product': rec[2]}
        return {'id': -1}
    else:
        return {'id': -1}            

def del_data(data):
    global _data, _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "delete from codes where id='"+sqlescape(str(data['id']))+"'"
    cur.execute(sql)
    conn.commit()
    conn.close()
    id = -1
    l = len(_data)
    i = 0
    while i < l:
        ele = _data[i]
        if data['id'] == ele['id']:
            id = i
        i += 1
    if id > -1:
        del _data[id]
    return data

def edit_data(data):
    global _data, _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "update codes set product='"+sqlescape(data['product'])+"' where id='"+sqlescape(str(data['id']))+"'"
    cur.execute(sql)
    conn.commit()
    conn.close()
    id = -1
    l = len(_data)
    i = 0
    while i < l:
        ele = _data[i]
        if data['id'] == ele['id']:
            _data[i]['product'] = data['product']
        i += 1
    return data

def check(code):
    global _data
    product = ""
    l = len(_data)
    i = 0
    while i < l:
        ele = _data[i]
        if ele['code'] == code:
            product = ele['product']
        i += 1
    return product

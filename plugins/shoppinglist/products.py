import sqlite3

_sl = None
_data = []
_file = ''

try:
    from sqlescapy import sqlescape
except:
    os.popen(sys.executable + ' -m pip install sqlescapy').read()
    from sqlescapy import sqlescape

def init(sl):
    global _sl, _file
    _sl = sl
    file = sl.const.path_var + '/db'
    try:
        os.mkdir(file)
    except:
        print()
    file += '/shoppinglist.sqlite'
    _file = file
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS products (id integer PRIMARY KEY, product text)"
    cur.execute(sql)
    conn.commit()
    sql = "select id, product from products order by product"
    cur.execute(sql)
    recs = cur.fetchall()
    conn.close()
    for rec in recs:
        _data.append({'id': rec[0], 'product': rec[1]})

def get_data():
    global _data
    return _data

def add_data():
    global _data, _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "insert into products (product) values ('Neues Produkt')"
    cur.execute(sql)
    conn.commit()
    sql = "select max(id) from products"
    cur.execute(sql)
    rec = cur.fetchone()
    sql = "select id, product from products where id='"+str(rec[0])+"'"
    cur.execute(sql)
    rec = cur.fetchone()
    conn.close()
    if rec:
        _data.append({'id': rec[0], 'product': rec[1]})
        return {'id': rec[0], 'product': rec[1]}
    conn.close()
    return {}

def del_data(data):
    global _data, _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "delete from products where id='"+sqlescape(str(data['id']))+"'"
    cur.execute(sql)
    conn.commit()
    conn.close()
    id = -1
    l = len(_data)
    i = 0
    while i < l:
        ele = _data[i]
        if data['id'] == ele['id']:
            id = i
        i += 1
    if id > -1:
        del _data[id]
    return data

def edit_data(data):
    global _data, _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "update products set product='"+sqlescape(data['product'])+"' where id='"+sqlescape(str(data['id']))+"'"
    cur.execute(sql)
    conn.commit()
    conn.close()
    id = -1
    l = len(_data)
    i = 0
    while i < l:
        ele = _data[i]
        if data['id'] == ele['id']:
            _data[i]['product'] = data['product']
        i += 1
    return data

def get_array():
    global _data
    out = []
    for rec in _data:
        out.append(rec['product'])
    out = sorted(out, key=lambda ele: ele.upper())   # sort by age
    return out

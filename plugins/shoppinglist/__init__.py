import lib.plugin as plugin

from plugins.shoppinglist import products, codes, shlist

class shoppinglist(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.running = False
        self.last_barcode = ''
        self.last_rfid = ''

    def start(self):
        self.sl.log.info('Start')
        if 'sensors' in self.sl.plugins.list:
            self.sl.plugins.sensors.start()
            self.sl.plugins.sensors.bind_event(self.event)
        products.init(self.sl)
        codes.init(self.sl)
        shlist.init(self.sl)

    def main_menu(self, keyword):
        if 'shoppinglist' == keyword:
            return {'label': 'Einkaufsliste', 'mod': 'shoppinglist', 'p1':''}

    def api(self, data):
        if data['cmd'] == "get_products":
            data['data'] = products.get_data()
        elif data['cmd'] == "add_products":
            data['data'] = products.add_data()
        elif data['cmd'] == "del_products":
            data['data'] = products.del_data(data['data'])
        elif data['cmd'] == "edit_products":
            data['data'] = products.edit_data(data['data'])
        elif data['cmd'] == "get_codes":
            data['data'] = {'table': codes.get_data(), 'products': products.get_array()}
        elif data['cmd'] == "add_bar_codes":
            data['data'] = codes.add_bar_data(self.last_barcode)
        elif data['cmd'] == "add_rfid_codes":
            data['data'] = codes.add_bar_data(self.last_rfid)
        elif data['cmd'] == "del_codes":
            data['data'] = codes.del_data(data['data'])
        elif data['cmd'] == "edit_codes":
            data['data'] = codes.edit_data(data['data'])
        elif data['cmd'] == "get_list":
            data['data'] = {'table': shlist.get_data(), 'products': products.get_array()}
        elif data['cmd'] == "add_list":
            data['data'] = shlist.add_data()
        elif data['cmd'] == "del_list":
            data['data'] = shlist.del_data(data['data'])
        elif data['cmd'] == "edit_list":
            data['data'] = shlist.edit_data(data['data'])
        elif data['cmd'] == "telegram":
            if "telegram" in self.sl.plugins.list:
                self.sl.plugins.telegram.send(shlist.get_text())
            data['data'] = {}
        return data

    def stop(self):
        self.sl.log.info('Stop')

    def event(self, data):
        product = ""
        if self.config['barcode'] == data['topic']:
            self.last_barcode = "barcode:"+str(data['value'])
            product = codes.check(self.last_barcode)
        elif self.config['rfid'] == data['topic']:
            self.last_rfid = "rfid:"+str(data['value'])
            product = codes.check(self.last_rfid)
        if product != "":
            shlist.add_sensor(product)

    def sm_backend_menu(self):
        return {'name': 'shoppinglist', 'friendly':'Einkaufsliste'}

    def sm_api(self, data):
        if data['data']['tab'] == 'setup':
            if data['cmd'] == 'get_data':
                data['data'] = {'sensors' : self.sl.plugins.sensors.get_names(),
                                'rfid': self.config['rfid'],
                                'barcode': self.config['barcode']}
            elif data['cmd'] == 'set_data':
                self.sl.config.plugins[self.name]['barcode'] = data['data']['barcode']
                self.sl.config.plugins[self.name]['rfid'] = data['data']['rfid']
                self.sl.config.save()
                data['data'] = {}
        return data

    def tele_menu(self):
        return ['Einkaufsliste', 'shoppinglist:']

    def tele_api(self, data):
        self.sl.plugins.telegram.send(shlist.get_text())

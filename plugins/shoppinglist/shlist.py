import sqlite3

_sl = None
_data = []
_file = ''

try:
    from sqlescapy import sqlescape
except:
    os.popen(sys.executable + ' -m pip install sqlescapy').read()
    from sqlescapy import sqlescape

def init(sl):
    global _sl, _file
    _sl = sl
    file = sl.const.path_var + '/db'
    try:
        os.mkdir(file)
    except:
        print()
    file += '/shoppinglist.sqlite'
    _file = file
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS shliste (id integer PRIMARY KEY, product text, anz integer)"
    cur.execute(sql)
    conn.commit()
    sql = "select id, product, anz from shliste order by product"
    cur.execute(sql)
    recs = cur.fetchall()
    conn.close()
    for rec in recs:
        _data.append({'id': rec[0], 'product': rec[1], 'items': rec[2]})

def get_data():
    global _data
    return _data

def add_data():
    global _data, _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "insert into shliste (product, anz) values ('Neues Produkt', '1')"
    cur.execute(sql)
    conn.commit()
    sql = "select max(id) from shliste"
    cur.execute(sql)
    rec = cur.fetchone()
    sql = "select id, product, anz from shliste where id='"+str(rec[0])+"'"
    cur.execute(sql)
    rec = cur.fetchone()
    conn.close()
    if rec:
        _data.append({'id': rec[0], 'product': rec[1], 'items': rec[2]})
        return {'id': rec[0], 'product': rec[1], 'items': rec[2]}
    conn.close()
    return {}

def del_data(data):
    global _data, _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "delete from shliste where id='"+sqlescape(str(data['id']))+"'"
    cur.execute(sql)
    conn.commit()
    conn.close()
    id = -1
    l = len(_data)
    i = 0
    while i < l:
        ele = _data[i]
        if data['id'] == ele['id']:
            id = i
        i += 1
    if id > -1:
        del _data[id]
    return data

def edit_data(data):
    global _data, _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    if data['field'] == 'product':
        sql = "update shliste set product='"+sqlescape(data['value'])+"' where id='"+sqlescape(str(data['id']))+"'"
    else:
        sql = "update shliste set anz="+sqlescape(data['value'])+" where id='"+sqlescape(str(data['id']))+"'"
    cur.execute(sql)
    conn.commit()
    conn.close()
    id = -1
    l = len(_data)
    i = 0
    while i < l:
        ele = _data[i]
        if data['id'] == ele['id']:
            if data['field'] == 'product':
                _data[i]['product'] = data['value']
            else:
                _data[i]['items'] = int(data['value'])
        i += 1
    return data

def add_sensor(data):
    global _data, _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "select id from shliste where product='"+data+"'"
    cur.execute(sql)
    rec = cur.fetchone()
    if rec:
        return
    sql = "insert into shliste (product, anz) values ('"+data+"', '1')"
    cur.execute(sql)
    conn.commit()
    sql = "select max(id) from shliste"
    cur.execute(sql)
    rec = cur.fetchone()
    sql = "select id, product, anz from shliste where id='"+str(rec[0])+"'"
    cur.execute(sql)
    rec = cur.fetchone()
    conn.close()
    if rec:
        _data.append({'id': rec[0], 'product': rec[1], 'items': rec[2]})
        return

def get_text():
    global _data
    out = []
    for data in _data:
         out.append(str(data['items'])+'x '+data['product'])
    return '\n'.join(out)

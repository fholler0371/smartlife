define(['module', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxbutton', 'jqxgrid_edit', 'jqxdropdownlist',
        'jqxcombobox'], function(module) {
  var list = {
    init : function() {
      html = '<div style="width:100%; height:100%;"><div id="shoppinglist_tab"><ul><li>List</li><li>Produkte</li><li>Readerdata</li></ul>'
      html += '<div><div id="shoppinglist_list"></div></div>'
      html += '<div><div id="shoppinglist_products"></div></div>'
      html += '<div><div id="shoppinglist_codes"></div></div>'
      $('#client_area').html(html+'</div>')
      $(window).off('resize', window.module.shoppinglist.resize)
      $(window).on('resize', window.module.shoppinglist.resize)
      window.module.shoppinglist.resize()
      $('#mainTop').off('sizeChange', window.module.shoppinglist.left_size)
      $('#mainTop').on('sizeChange', window.module.shoppinglist.left_size)
      window.module.shoppinglist.left_size()
      $('#shoppinglist_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      $('#shoppinglist_tab').off('selected')
      $('#shoppinglist_tab').on('selected', function(ev) {
        var selectedTab = ev.args.item
        window.module.shoppinglist.call_tab(selectedTab)
      })
      window.module.shoppinglist.call_tab(0)
      $("#shoppinglist_tab .jqx-tabs-content").css('padding', 0).css('border-width', 0)
    },
    call_tab : function(id) {
      if (id == 0) {
        window.smcall({'client': 'shoppinglist', 'cmd':'get_list', 'data': {}}, function(data) {
          var list = data.data.products
          var source = {
            localdata: data.data.table,
            datatype: "array",
            datafields: [
              { name: 'id', type: 'integer' },
              { name: 'product', type: 'string'},
              { name: 'items', type: 'integer' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#shoppinglist_list").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            editable: true,
            columns: [
              { text: 'id', editable: false, hidden: true, datafield: 'id'},
              { text: 'Produkt', datafield: 'product', columntype: 'combobox',
                createeditor: function (row, value, editor) {
                  editor.jqxComboBox({ source: data.data.products, valueMember: 'value' });
                }
              },
              { text: 'Anzahl', datafield: 'items', width: 200, columntype: 'dropdownlist',
                createeditor: function (row, value, editor) {
                  editor.jqxDropDownList({ source: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'], valueMember: 'value' });
                }
              }
            ],
            showtoolbar: true,
            rendertoolbar: function (bar) {
              var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>")
              var addButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;'"+
                                " src='/lib/img/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Einfügen</span></div>")
              var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;'"+
                                   " src='/lib/img/close.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Löschen</span></div>")
              var telegramButton = $("<div style='float: left; margin-left: 5px;'>"+
                                     "<span style='margin-left: 4px; position: relative; top: 8px;'>Export Telegram</span></div>")
              container.append(addButton)
              container.append(deleteButton)
              container.append(telegramButton)
              bar.append(container)
              addButton.jqxButton({  width: 80, height: 20 })
              deleteButton.jqxButton({  width: 80, height: 20 })
              addButton.click(function() {
                window.smcall({'client': 'shoppinglist', 'cmd':'add_list', 'data': {}}, function(data) {
                  $("#shoppinglist_list").jqxGrid('addrow', null, data.data)
                })
              })
              deleteButton.click(function() {
                var selectedrowindex = $("#shoppinglist_list").jqxGrid('getselectedrowindex')
                if (selectedrowindex != -1) {
                  var data = $('#shoppinglist_list').jqxGrid('getrowdata', selectedrowindex)
                  window.smcall({'client': 'shoppinglist', 'cmd':'del_list', 'data': {'id': data.id}}, function(data) {})
                  $("#shoppinglist_list").jqxGrid('deleterow', selectedrowindex)
                }
              })
              telegramButton.click(function() {
                  window.smcall({'client': 'shoppinglist', 'cmd':'telegram', 'data': {}}, function(data) {})
              })
            }
          })
          $("#shoppinglist_list").parent().css('padding', 0).css('overflow', 'hidden') 
          $("#shoppinglist_list").off('cellendedit')
          $("#shoppinglist_list").on('cellendedit', function (event) {
            var args = event.args
            var data = $('#shoppinglist_list').jqxGrid('getrowdatabyid', args.rowindex)
            window.smcall({'client': 'shoppinglist', 'cmd':'edit_list', 'data': {
                'id':data.id, 'value': args.value, 'field': args.datafield
              }}, function(data) {})
          })
        })
      }
      if (id == 1) {
        window.smcall({'client': 'shoppinglist', 'cmd':'get_products', 'data': {}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'id', type: 'integer' },
              { name: 'product', type: 'string' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#shoppinglist_products").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            editable: true,
            columns: [
              { text: 'id', editable: false, hidden: true, datafield: 'id'},
              { text: 'Produkt', datafield: 'product'}
            ],
            showtoolbar: true,
            rendertoolbar: function (bar) {
              var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>")
              var addButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;'"+
                                " src='/lib/img/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Einfügen</span></div>")
              var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;'"+
                                   " src='/lib/img/close.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Löschen</span></div>")
              container.append(addButton)
              container.append(deleteButton)
              bar.append(container)
              addButton.jqxButton({  width: 80, height: 20 })
              deleteButton.jqxButton({  width: 80, height: 20 })
              addButton.click(function() {
                window.smcall({'client': 'shoppinglist', 'cmd':'add_products', 'data': {}}, function(data) {
                  $("#shoppinglist_products").jqxGrid('addrow', null, data.data)
                })
              })
              deleteButton.click(function() {
                var selectedrowindex = $("#shoppinglist_products").jqxGrid('getselectedrowindex')
                if (selectedrowindex != -1) {
                  var data = $('#shoppinglist_products').jqxGrid('getrowdata', selectedrowindex)
                  window.smcall({'client': 'shoppinglist', 'cmd':'del_products', 'data': {'id': data.id}}, function(data) {})
                  $("#shoppinglist_products").jqxGrid('deleterow', selectedrowindex)
                }
              })
            }
          })
          $("#shoppinglist_products").parent().css('padding', 0).css('overflow', 'hidden')
          $("#shoppinglist_products").off('cellendedit')
          $("#shoppinglist_products").on('cellendedit', function (event) {
            var args = event.args
            var data = $('#shoppinglist_products').jqxGrid('getrowdatabyid', args.rowindex)
            window.smcall({'client': 'shoppinglist', 'cmd':'edit_products', 'data': {'id':data.id, 'product': args.value}}, function(data) {})
          })
        })
      }
      if (id == 2) {
        window.smcall({'client': 'shoppinglist', 'cmd':'get_codes', 'data': {}}, function(data) {
          var list = data.data.products
          var source = {
            localdata: data.data.table,
            datatype: "array",
            datafields: [
              { name: 'id', type: 'integer' },
              { name: 'code', type: 'string'},
              { name: 'product', type: 'string' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#shoppinglist_codes").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            editable: true,
            columns: [
              { text: 'id', editable: false, hidden: true, datafield: 'id'},
              { text: 'Kode', editable: false, datafield: 'code', width: 300},
              { text: 'Produkt', datafield: 'product', columntype: 'dropdownlist',
                createeditor: function (row, value, editor) {
                  editor.jqxDropDownList({ source: data.data.products, valueMember: 'value' });
                }
              }
            ],
            showtoolbar: true,
            rendertoolbar: function (bar) {
              var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>")
              var addBarButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;'"+
                                " src='/lib/img/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Barcode</span></div>")
              var addRFIDButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;'"+
                                " src='/lib/img/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>RFID</span></div>")
              var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;'"+
                                   " src='/lib/img/close.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Löschen</span></div>")
              container.append(addBarButton)
              container.append(addRFIDButton)
              container.append(deleteButton)
              bar.append(container)
              addBarButton.jqxButton({  width: 80, height: 20 })
              addRFIDButton.jqxButton({  width: 80, height: 20 })
              deleteButton.jqxButton({  width: 80, height: 20 })
              addBarButton.click(function() {
                window.smcall({'client': 'shoppinglist', 'cmd':'add_bar_codes', 'data': {}}, function(data) {
                  if (data.data.id == -1) {
                    alert("Kein Kode bekannt")
                  } else if (data.data.id == -2) {
                    alert("Kode bereits registriert")
                  } else {
                    $("#shoppinglist_codes").jqxGrid('addrow', null, data.data)
                  }
                })
              })
              addRFIDButton.click(function() {
                window.smcall({'client': 'shoppinglist', 'cmd':'add_rfid_codes', 'data': {}}, function(data) {
                  if (data.data.id == -1) {
                    alert("Kein Kode bekannt")
                  } else if (data.data.id == -2) {
                    alert("Kode bereits registriert")
                  } else {
                    $("#shoppinglist_codes").jqxGrid('addrow', null, data.data)
                  }
                })
              })
              deleteButton.click(function() {
                var selectedrowindex = $("#shoppinglist_codes").jqxGrid('getselectedrowindex')
                if (selectedrowindex != -1) {
                  var data = $('#shoppinglist_codes').jqxGrid('getrowdata', selectedrowindex)
                  window.smcall({'client': 'shoppinglist', 'cmd':'del_codes', 'data': {'id': data.id}}, function(data) {})
                  $("#shoppinglist_codes").jqxGrid('deleterow', selectedrowindex)
                }
              })
            }
          })
          $("#shoppinglist_codes").parent().css('padding', 0).css('overflow', 'hidden')
          $("#shoppinglist_codes").off('cellendedit')
          $("#shoppinglist_codes").on('cellendedit', function (event) {
            var args = event.args
            var data = $('#shoppinglist_codes').jqxGrid('getrowdatabyid', args.rowindex)
            window.smcall({'client': 'shoppinglist', 'cmd':'edit_codes', 'data': {'id':data.id, 'product': args.value}}, function(data) {})
          })
        })
      }
    },
    stop : function() {
    },
    resize: function() {
      $('#client_area').children().height($('#client_area').height())
    },
    left_size: function() {
    }
  }
  list['init_data'] = window.module_const[module.id]
  window.module.shoppinglist = list
  return list
})

define(['jqxtabs', 'jqxdropdownlist', 'jqxbutton'], function() {
  var list = {
    init : function() {
      html = '<div id="smarthome_shoppinglist_tab"><ul><li>Einstellungen</li></ul>'
      html += '<div><table>'
      html += '<tr><td colspan=3><b>Sensoren:</b></td></tr>'
      html += '<tr><td>Barcode</td><td><div id="smarthome_shoppinglist_barcode"></div></td><td></td></tr>'
      html += '<tr><td>RFID</td><td><div id="smarthome_shoppinglist_rfid"></div></td><td></td></tr>'
      html += '<tr><td><td><td></td><td><input type="button" value="Senden" id="system_send" /></td></tr>'
      html += '</table></div></div>'
      $('#smarthome_data').html(html)
      $('#smarthome_shoppinglist_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      window.sm_b.shoppinglist.call_tab(0)
      $('#smarthome_shoppinglist_barcode, #smarthome_shoppinglist_rfid').jqxDropDownList({width: 250, height: 30})
      $('#smarthome_shoppinglist_tab input:button').jqxButton({width: 250, height: 40}).css('margin', '10px')
      $('#smarthome_shoppinglist_tab input:button').off('click')
      $('#smarthome_shoppinglist_tab input:button').on('click', window.sm_b.shoppinglist.send)
    },
    send : function() {
      var barcode = $('#smarthome_shoppinglist_barcode').jqxDropDownList('getSelectedIndex')
      var rfid = $('#smarthome_shoppinglist_rfid').jqxDropDownList('getSelectedIndex')
      if (barcode == -1 || rfid == -1) {
        alert('Bitte Sensoren auswählen')
      } else {
        barcode = window.sm_b.shoppinglist.sensors[barcode].name
        rfid = window.sm_b.shoppinglist.sensors[rfid].name
        window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'shoppinglist', 'tab': 'setup', 'barcode': barcode, 'rfid': rfid}}, function(data) {
        })
      }
    },
    call_tab : function(id) {
      if (id == 0) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'shoppinglist', 'tab': 'setup'}}, function(data) {
          window.sm_b.shoppinglist.sensors = data.data.sensors
          $('#smarthome_shoppinglist_barcode, #smarthome_shoppinglist_rfid').jqxDropDownList('clear')
          var barcode = -1
          var rfid = -1
          var len = data.data.sensors.length
          for (var i=0; i < len; i++) {
             $('#smarthome_shoppinglist_barcode, #smarthome_shoppinglist_rfid').jqxDropDownList('addItem', data.data.sensors[i].friendly)
             if (data.data.sensors[i].name == data.data.barcode) {
               barcode = i
             }
             if (data.data.sensors[i].name == data.data.rfid) {
               rfid = i
             }
          }
          if (barcode > -1) {
            $('#smarthome_shoppinglist_barcode').jqxDropDownList('selectIndex', barcode)
          }
          if (rfid > -1) {
            $('#smarthome_shoppinglist_rfid').jqxDropDownList('selectIndex', rfid)
          }
        })
      }
    }
  }
  window.sm_b.shoppinglist = list
  return list
})

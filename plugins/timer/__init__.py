import lib.plugin as plugin

from plugins.timer import jobs

class timer(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.running = False

    def start(self):
        self.sl.log.info('Start')
        if not self.running:
            jobs.init(self.sl)
            self.running = True
            
    def stop(self):
        self.sl.log.info('Stop')
        jobs.stop()

    def do(self, intervall, job, random=True, one=False):
        jobs.add(intervall, job, random, one)

import time
import random
from threading import Thread

_sl = None
_jobs = []
_run_th = None

class run_th(Thread):
    def __init__(self, cb):
        Thread.__init__(self)
        self.cb = cb
        self.running = True

    def run(self):
        while self.running:
            self.cb()
            time.sleep(1)

class do_th(Thread):
    def __init__(self, dothis, job):
        Thread.__init__(self)
        self.dothis = dothis
        self.job = job

    def run(self):
        global _sl
        try:
            self.dothis(job=self.job)
        except Exception as e:
            _sl.log.error(str(e))

class job:
    def __init__(self, intervall, dothis, random, one):
        self.intervall = intervall
        self.do = dothis
        self.random = random
        self.one = one
        self.last = time.time()
        self.active = True
        self.set_next()

    def set_next(self):
        if self.random:
            self.do_time = self.last + self.intervall*0.9 + random.randint(0, int(0.2*self.intervall))
        else:
            self.do_time = self.last + self.intervall
        self.canrun = True

def init(sl):
    global _sl, _run_th
    _sl = sl
    if not _run_th:
        _run_th = run_th(call)
        _run_th.start()

def add(intervall, dothis, random, one):
    global _sl, _jobs
    j = job(intervall, dothis, random, one)
    _jobs.append(j)
    return j

def stop():
    global _run_th
    if _run_th:
        _run_th.running = False

def call():
    global _jobs, _sl
    t = time.time()
    for j in _jobs:
        try:
            if j.active and j.canrun and j.do_time  < t:
                j.canrun = False
                if j.one:
                    j.active = False
                j.last = t
                th = do_th(j.do, j)
                th.start()
                if j.active:
                     j.set_next()
                _sl.log.debug('Run it')
        except Exception as e:
            _sl.log.error(str(e))
    

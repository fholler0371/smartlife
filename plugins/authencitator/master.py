import os
import sqlite3
import json

try:
    from sqlescapy import sqlescape
except:
    os.popen(sys.executable + ' -m pip install sqlescapy').read()
    from sqlescapy import sqlescape

try:
    import bcrypt
except:
    os.popen(sys.executable + ' -m pip install bcrypt').read()
    import bcrypt

def reset(sl, data):
    out = {'ok': True}
    name = sqlescape(data['user'])
    path = sl.const.path_var + '/db'
    file = path + '/user.sqlite'
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "UPDATE user set passwd='' WHERE user='%s'" % ( name )
    conn.close()
    return out

def set_rights(sl, data):
    out = {'ok': True}
    name = sqlescape(data['user'])
    rights = data['rights']
    path = sl.const.path_var + '/db'
    file = path + '/user.sqlite'
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "SELECT id FROM user WHERE user='%s'" % (name)
    cur.execute(sql)
    res = cur.fetchone()
    if res == None:
        out = {'ok': False}
        sl.log.error("Nutzer nicht bekannt")
        return out
    id = res[0]
    sql = "delete from rights where user='%s'" % (str(id))
    cur.execute(sql)
    conn.commit()
    for right in rights:
        right = sqlescape(right)
        sql = "insert into rights (user, right) values ('%s', '%s')" % (str(id), right)
        cur.execute(sql)
    conn.commit()
    conn.close()
    return out

def passwd(sl, data):
    out = {'ok': True}
    name = sqlescape(data['user'])
    passw = sqlescape(data['passwd'])
    if len(passw) < 6:
        sl.log.error('Passwort zu kurz')
        out = {'ok': False}
        return out
    path = sl.const.path_var + '/db'
    file = path + '/user.sqlite'
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(passw.encode(), salt).decode()
    sql = "UPDATE user set passwd='%s' WHERE user='%s'" % (hashed, name)
    cur.execute(sql)
    conn.commit()
    conn.close()
    return out

def real_name(sl, data):
    out = {'ok': True}
    name = sqlescape(data['user'])
    r_name = sqlescape(data['real'])
    path = sl.const.path_var + '/db'
    file = path + '/user.sqlite'
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "UPDATE user set name='%s' where user='%s'" % (r_name, name)
    cur.execute(sql)
    conn.commit()
    conn.close()
    return out

def new_user(sl, data):
    out = {'ok': True}
    name = sqlescape(data['user'])
    if len(name) < 6:
        sl.log.error('Name zu kurz')
        out = {'ok': False}
        return out
    path = sl.const.path_var + '/db'
    file = path + '/user.sqlite'
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "SELECT id FROM user where user='%s'" % (name)
    cur.execute(sql)
    res = cur.fetchone()
    if res:
        sl.log.error('Name existiert')
        out = {'ok': False}
        return out
    sql = "insert into user (user, name, passwd) values ('%s', '', '')" % (name)
    cur.execute(sql)
    conn.commit()
    conn.close()
    return out

def get_rights(sl, user):
    avaible = []
    path = sl.const.path + '/plugins'
    for d in os.listdir(path):
        file = path + '/' + d + '/manifest.json'
        if os.path.isfile(file):
            try:
                f = open(file, 'r')
                manifest = json.loads(f.read())
                f.close()
                if 'rights' in manifest:
                    for right in manifest['rights']:
                        avaible.append(right)
            except:
                print()
    path = sl.const.path_var + '/db'
    file = path + '/user.sqlite'
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "SELECT id, name FROM user where user='%s'" % (user)
    cur.execute(sql)
    res = cur.fetchone()
    id = res[0]
    name = res[1]
    sql = "select right from rights where user='%s'" % (str(id))
    cur.execute(sql)
    rights = cur.fetchall()
    enable = []
    for right in rights:
        enable.append(right[0])
    conn.close()
    return {'avaible': avaible, 'enable': enable, 'name':name}

def get_user(sl):
    out = []
    path = sl.const.path_var + '/db'
    file = path + '/user.sqlite'
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "SELECT user FROM user order by user"
    cur.execute(sql)
    res = cur.fetchall()
    conn.close()
    for entry in res:
        out.append(entry[0])
    return out

def api(sl, data):
    if data['cmd'] == 'get_user':
        data['data'] = get_user(sl)
    elif data['cmd'] == 'get_rights':
        data['data'] = get_rights(sl, sqlescape(data['data']['user']))
    elif data['cmd'] == 'new_user':
        data['data'] = new_user(sl, data['data'])
    elif data['cmd'] == 'real_name':
        data['data'] = real_name(sl, data['data'])
    elif data['cmd'] == 'passwd':
        data['data'] = passwd(sl, data['data'])
    elif data['cmd'] == 'rights':
        data['data'] = set_rights(sl, data['data'])
    elif data['cmd'] == 'reset':
        data['data'] = reset(sl, data['data'])
    return data

import sqlite3
import base64
import json
import time
import os
import sys

try:
    import bcrypt
except:
    os.popen(sys.executable + ' -m pip install bcrypt').read()
    import bcrypt

try:
    from Crypto.Cipher import AES
except:
    os.popen(sys.executable + ' -m pip install pycryptodome').read()
    from Crypto.Cipher import AES

aes_key_1 = ''

def check_aes(sl):
    global aes_key_1
    if aes_key_1 == '':
        sl.log.debug('get aes key')
        file = sl.const.path+'/db/user.sqlite'
        conn = sqlite3.connect(file)
        cur = conn.cursor()
        sql = "CREATE TABLE IF NOT EXISTS aes (id integer PRIMARY KEY, aes_key text NOT NULL)"
        cur.execute(sql)
        conn.commit()
        sql = "SELECT aes_key FROM aes WHERE id='1'"
        cur.execute(sql)
        res = cur.fetchone()
        if res == None:
            aes_key_1 = bcrypt.gensalt().decode()
            sql = "INSERT INTO aes (aes_key) VALUES ('%s')" % (aes_key_1)
            cur.execute(sql)
            conn.commit()
        else:
            aes_key_1 = res[0]
        conn.close()

def decode(sl, data):
    sl.log.debug('decode')
    global aes_key_1
    check_aes(sl)
    out = data
    try:
        ciphertext = base64.decodestring(data['token'].encode())
        nonce = ciphertext[:AES.block_size]
        ciphertext = ciphertext[AES.block_size:]
        cipher = AES.new(aes_key_1[:16].encode(), AES.MODE_EAX, nonce=nonce)
        text = cipher.decrypt(ciphertext)
        out['token'] = json.loads(text.decode())
        out['login'] = out['token']['timeout'] > time.time()
    except:
        out['login'] = False
    return out


def encode(sl, data):
    sl.log.debug('encode')
    global aes_key_1
    check_aes(sl)
    cipher = AES.new(aes_key_1[:16].encode(), AES.MODE_EAX)
    data['token']['timeout'] = int(time.time()+900)
    if 'cmd' in data['token']:
        del data['token']['cmd']
    if 'client' in data['token']:
        del data['token']['client']
    token = json.dumps(data['token']).encode()
    ciphertext, tag = cipher.encrypt_and_digest(token)
    data['token'] = base64.encodestring(cipher.nonce+ciphertext).decode().replace('\n', '')
    return data

def file_decode(sl, data):
    global aes_key_1
    check_aes(sl)
    out = data
    try:
        ciphertext = base64.decodestring(data.encode())
        nonce = ciphertext[:AES.block_size]
        ciphertext = ciphertext[AES.block_size:]
        cipher = AES.new(aes_key_1[:16].encode(), AES.MODE_EAX, nonce=nonce)
        text = cipher.decrypt(ciphertext)
        data = json.loads(text.decode())
        if data['timeout'] > time.time():
            return data['file']
        return data
    except:
        x=1
    return ""

def file_encode(sl, data):
    global aes_key_1
    check_aes(sl)
    cipher = AES.new(aes_key_1[:16].encode(), AES.MODE_EAX)
    token = json.dumps(data).encode()
    ciphertext, tag = cipher.encrypt_and_digest(token)
    return base64.encodestring(cipher.nonce+ciphertext).decode().replace('\n', '')

from plugins.authencitator import user, crypt, master

import lib.plugin as plugin

class authencitator(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')

    def check_user(self, _user, _password):
        self.sl.log.debug('Check User: ' + _user)
        return user.checkUser(self.sl, _user, _password)

    def decode(self, data):
        self.sl.log.debug('decode')
        return crypt.decode(self.sl, data)

    def encode(self, data):
        self.sl.log.debug('encode')
        return crypt.encode(self.sl, data)

    def file_encode(self, data):
        return crypt.file_encode(self.sl, data)

    def file_decode(self, data):
        return crypt.file_decode(self.sl, data)

    def main_menu(self, keyword):
        if keyword == 'authencitator.user':
             return {'label': 'Passwort &auml;ndern', 'mod': 'authencitator', 'p1':'password'}
        elif keyword == 'authencitator.master':
             return {'label': 'Nutzerverwaltung', 'mod': 'authencitator', 'p1':'mangement'}
        return None

    def api(self, data):
        if data['client'] == 'authencitator.user':
            if 'new_passwd' == data['cmd']:
                data = user.new_passwd(self.sl, data)
        elif data['client'] == 'authencitator.master':
            data = master.api(self.sl, data)
        return data

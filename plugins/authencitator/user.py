import os
import sqlite3
import sys

try:
    from sqlescapy import sqlescape
except:
    os.popen(sys.executable + ' -m pip install sqlescapy').read()
    from sqlescapy import sqlescape

try:
    import bcrypt
except:
    os.popen(sys.executable + ' -m pip install bcrypt').read()
    import bcrypt

def checkUser(sl, _user, _password):
    sl.log.info('Check User: ' + _user)
    path = sl.const.path + '/db'
    if not os.path.isdir(path):
        os.mkdir(path)
    file = path + '/user.sqlite'
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "SELECT passwd, name, id FROM user WHERE user='%s'" % (sqlescape(_user))
    try:
        cur.execute(sql)
        res = cur.fetchone()
        if not res:
            res = ['','',-1]
    except:
        res = ['','',-1]
    sql = "select right from rights where user='%s'" % (str(res[2]))
    cur.execute(sql)
    rights = cur.fetchall()
    if not rights:
        rights = []
    right_val = []
    for right in rights:
        right_val.append(right[0])
    conn.close()
    if res:
        _passwd = res[0]
        _name = res[1]
        if len(_passwd) > 0 and bcrypt.checkpw(_password.encode(), _passwd.encode()):
            return [True, _name, right_val]
    return [False, '', None]

def new_passwd(sl, data):
    _user = data['token']['user']
    sl.log.info('Check Password: ' + _user)
    del data['client']
    del data['cmd']
    path = sl.const.path + '/db'
    if not os.path.isdir(path):
        os.mkdir(path)
    file = path + '/user.sqlite'
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "SELECT passwd FROM user WHERE user='%s'" % (sqlescape(_user))
    try:
        cur.execute(sql)
        res = cur.fetchone()
    except:
        res = None
    if res == None:
        data['data'] = False
        sl.log.error('Nutzer nicht bekannt')
        return data
    _passwd = res[0]
    _data = data['data']
    if not(bcrypt.checkpw(_data['old'].encode(), _passwd.encode())):
        data['data'] = False
        sl.log.error('Falsches altes Passwort')
        return data
    if len(_data['new']) < 6:
        data['data'] = False
        sl.log.error('Passwort zu kurz')
        return data
    if _data['new'] != _data['repeat']:
        data['data'] = False
        sl.log.error('Passwort ungleich')
        return data
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(_data['new'].encode(), salt).decode()
    sql = "UPDATE user set passwd='%s' WHERE user='%s'" % (hashed, _user)
    cur.execute(sql)
    conn.commit()
    conn.close()
    data['data'] = True
    return data

import os
import sys

import lib.plugin as plugin
import copy

try:
    from watchdog.observers import Observer
    from watchdog.events import PatternMatchingEventHandler
except:
    os.popen(sys.executable + ' -m pip install watchdog').read()
    from watchdog.observers import Observer
    from watchdog.events import PatternMatchingEventHandler

try:
    import boto3
except:
    os.popen(sys.executable + ' -m pip install boto3').read()
    import boto3


class watch_folder(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.config_path = sl.const.path_var + '/config'
        if not os.path.isdir(self.config_path):
            os.mkdir(self.config_path)
        self.db_path = sl.const.path_var + '/db'
        if not os.path.isdir(self.db_path):
            os.mkdir(self.db_path)

    def sm_backend_menu(self):
        return {'name': 'watch_folder', 'friendly':'Ordnerüberwachung'}

    def sm_api(self, data):
        if data['cmd'] == 'set_data':
           if data['data']['config']['secret'] != '':
               self.sl.log.critical(data['data']['config']['secret'])
               self.sl.config.plugins[self.name]['config'] = data['data']['config']
           if data['data']['db']['secret'] != '':
               self.sl.config.plugins[self.name]['db'] = data['data']['db']
           self.sl.config.save()
        elif data['cmd'] == 'get_data':
           data['data'] = {}
           try:
               data['data']['config'] =  copy.copy(self.sl.config.plugins[self.name]['config'])
               del data['data']['config']['secret']
           except Exception as e:
               self.sl.log.error(str(e))
           try:
               data['data']['db'] = copy.copy(self.sl.config.plugins[self.name]['db'])
               del data['data']['db']['secret']
           except Exception as e:
               self.sl.log.error(str(e))
        return data

    def config_modified(self, event):
        file = event.src_path[len(self.config_path)+1:]
        self.sl.log.info('Config changed: '+file)
        if self.name in self.sl.config.plugins and 'config' in self.config:
            config = self.config['config']
            session = boto3.Session(aws_access_key_id=config["key"], aws_secret_access_key=config["secret"], region_name=config["region"])
            s3 = session.resource("s3")
            s3.Bucket(config["bucket"]).upload_file(event.src_path, self.sl.const.host + '/' + file)

    def db_modified(self, event):
        file = event.src_path[len(self.db_path)+1:]
        self.sl.log.info('db changed: '+file)
        if self.name in self.sl.config.plugins and 'db' in self.config:
            db = self.config['db']
            session = boto3.Session(aws_access_key_id=db["key"], aws_secret_access_key=db["secret"], region_name=db["region"])
            s3 = session.resource("s3")
            if file == 'user.sqlite':
                s3.Bucket(db["bucket"]).upload_file(event.src_path, file)
            else:
                s3.Bucket(db["bucket"]).upload_file(event.src_path, self.sl.const.host + '/' + file)

    def start(self):
        self.sl.log.info('Start')
        self.config_observer = Observer()
        patterns = "*"
        ignore_patterns = ["*.swp"]
        ignore_directories = True
        case_sensitive = True
        my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)
        my_event_handler.on_modified = self.config_modified
        go_recursively = True
        self.config_observer.schedule(my_event_handler, self.config_path, recursive=go_recursively)
        self.config_observer.start()
        self.db_observer = Observer()
        patterns = "*"
        ignore_patterns = ["*.sqlite-journal"]
        ignore_directories = True
        case_sensitive = True
        my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)
        my_event_handler.on_modified = self.db_modified
        go_recursively = True
        self.db_observer.schedule(my_event_handler, self.db_path, recursive=go_recursively)
        self.db_observer.start()

    def stop(self):
        self.sl.log.info('Stop')
        try:
            self.config_observer.stop()
        except:
            print()
        try:
            self.db_observer.stop()
        except:
            print()

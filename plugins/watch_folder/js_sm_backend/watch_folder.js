define(['jqxtabs', 'jqxbutton'], function() {
  var wf = {
    init : function() {
      html = '<div id="smarthome_wf_tab"><ul><li>Einstellungen</li></ul>'
      html += '<div id="smarthome_wf_settings"></div></div>'
      $('#smarthome_data').html(html)
      $('#smarthome_wf_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      html = '<table><tr><td colspan=3><b>Konfigurations-Ordner</b></td></tr>'
      html += '<tr><td>Region: </td><td><input type="text" id="system_wf_c_region" /></td><td></td></tr>'
      html += '<tr><td>Bucket: </td><td><input type="text" id="system_wf_c_bucket" /></td><td></td></tr>'
      html += '<tr><td>Key: </td><td><input type="text" id="system_wf_c_key" /></td><td></td></tr>'
      html += '<tr><td>Secret: </td><td><input type="text" id="system_wf_c_secret" /></td><td></td></tr>'
      html += '<tr><td colspan=3> </td></tr>'
      html += '<tr><td colspan=3><b>Datenbank-Ordner</b></td></tr>'
      html += '<tr><td>Region: </td><td><input type="text" id="system_wf_db_region" /></td><td></td></tr>'
      html += '<tr><td>Bucket: </td><td><input type="text" id="system_wf_db_bucket" /></td><td></td></tr>'
      html += '<tr><td>Key: </td><td><input type="text" id="system_wf_db_key" /></td><td></td></tr>'
      html += '<tr><td>Secret: </td><td><input type="text" id="system_wf_db_secret" /></td><td></td></tr>'
      html += '<tr><td colspan=3> </td></tr>'
      html += '<tr><td></td><td></td><td><input type="button" value="Senden" id="system_send" /></td></tr>'
      html += '</table>'
      $('#smarthome_wf_settings').html(html)
      $("#smarthome_wf_settings input:text").jqxInput({height: 40, width: 250})
      $('#smarthome_wf_settings input:button').jqxButton({width: 250, height: 40}).css('margin', '10px')
      $('#smarthome_wf_settings input:button').off('click')
      $('#smarthome_wf_settings input:button').on('click', window.sm_b.watch_folder.send)
      window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                     'modul':'watch_folder', 'tab': 'setup'}}, function(data) {
        if (data.data.config != undefined) {
          $("#system_wf_c_region").jqxInput('val', data.data.config.region)
          $("#system_wf_c_bucket").jqxInput('val', data.data.config.bucket)
          $("#system_wf_c_key").jqxInput('val', data.data.config.key)
        }
        if (data.data.db != undefined) {
          $("#system_wf_db_region").jqxInput('val', data.data.db.region)
          $("#system_wf_db_bucket").jqxInput('val', data.data.db.bucket)
          $("#system_wf_db_key").jqxInput('val', data.data.db.key)
        }
      })
    },
    send : function() {
      var config = {region: $("#system_wf_c_region").jqxInput('val'),
                    bucket: $("#system_wf_c_bucket").jqxInput('val'),
                    key: $("#system_wf_c_key").jqxInput('val'),
                    secret: $("#system_wf_c_secret").jqxInput('val')}
      var db = {region: $("#system_wf_db_region").jqxInput('val'),
                bucket: $("#system_wf_db_bucket").jqxInput('val'),
                key: $("#system_wf_db_key").jqxInput('val'),
                secret: $("#system_wf_db_secret").jqxInput('val')}
      window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                     'modul':'watch_folder', 'tab': 'setup', 'config': config, 'db': db}}, function(data) {
      })
    }
  }
  window.sm_b.watch_folder = wf
  return wf
})

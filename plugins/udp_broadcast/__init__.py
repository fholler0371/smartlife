import lib.plugin as plugin

from plugins.udp_broadcast import server

class udp_broadcast(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.running = False

    def start(self):
        if not self.running:
            self.sl.log.info('run')
            self.running = True
            server.start(self, self.config['port'], self.resv)

    def stop(self):
        if self.running:
            self.sl.log.info('run')
            server.stop(self)

    def sent(self, mesg):
        self.sl.log.debug(mesg)
        server.sent(self, mesg)

    def resv(self, mesg):
        self.sl.log.info(mesg)
        name, data = mesg.split(':', 1)
        if name in self.sl.plugins.list:
            pl = self.sl.plugins.list[name]
            if hasattr(pl, 'udp'):
                pl.udp(data)

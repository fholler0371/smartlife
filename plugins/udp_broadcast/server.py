import time
import socket
import select

from threading import Thread

th = None

class udp_class(Thread):
    def __init__(self, port, resv):
        Thread.__init__(self)
        self.port = port
        self.running = True
        self.resv = resv

    def run(self):
        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        client.setblocking(0)
        client.bind(("", self.port))
        read_list = [client]
        while self.running:
           readable, writable, errored = select.select(read_list, [], [], 0)
           if len(readable) > 0:
               for sock in readable:
                   if sock == client:
                       data, addr = client.recvfrom(1024)
                       for entry in data.decode().split('\n'):
                           if len(entry) > 0:
                               self.resv(entry)
           else:
               time.sleep(0.1)

    def sent(self, mesg):
        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        client.setblocking(0)
        client.sendto((mesg+'\n').encode(), ("<broadcast>", self.port))

def start(self, port, resv):
    self.sl.log.info('start')
    global th
    if not th:
        th = udp_class(port, resv)
        th.start()

def stop(self):
    self.sl.log.info('stop')
    global th
    if th:
        th.running = False

def sent(self, mesg):
    self.sl.log.info(mesg)
    global th
    if th:
        th.sent(mesg)

import os
import time
from threading import Thread

import lib.plugin as plugin

from plugins.barcodescanner import device, db

class sendout(Thread):
    def __init__(self, sl, data):
       Thread.__init__(self)
       self.sl = sl
       self.data = data

    def run(self):
       try:
           self.sl.plugins.sensors.new_data(self.data)
       except Exception as e:
           self.sl.log.error(str(e))

class barcodescanner(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.th = None
        res = os.popen('sudo chmod 777 /dev/hidraw*').read()

    def new_text(self, text):
        if db.check(text):
            if 'sensors' in self.sl.plugins.list:
                th = sendout(self.sl, {'name': self.sl.const.host+'barcodescanner:id', 'friendly': self.sl.const.host+'barcodescanner:id',
                                   'value': text, 'time': int(time.time())})
                th.start()

    def sm_api(self, data):
        if data['data']['tab'] == 'setup':
            if data['cmd'] == 'set_data':
                self.sl.config.plugins[self.name]['usb'] = data['data']['usb']
                self.sl.config.save()
                data['data'] = {}
            elif data['cmd'] == 'get_data':
                ret = os.listdir('/dev/')
                data['data'] = {'hid': [], 'value': self.config['usb']}
                for entry in ret:
                    if entry.startswith('hidraw'):
                        data['data']['hid'].append(entry)
        elif data['data']['tab'] == 'history':
            if data['cmd'] == 'get_data':
                data['data'] = db.history()
        return data

    def sm_backend_menu(self):
        return {'name': 'barcodescanner', 'friendly':'Barcode-Reader'}

    def start(self):
        self.sl.log.info('Start')
        db.init(self.sl)
        if self.config['usb'] != '':
            self.th = device.scanner(self.sl, self.config['usb'], self.new_text)
            self.th.start()
        if 'sensors' in self.sl.plugins.list:
            self.sl.plugins.sensors.start()

    def stop(self):
        self.sl.log.info('Stop')
        try:
            if self.th:
                self.th.running = False
        except:
            print()

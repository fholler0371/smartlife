define(['jqxtabs', 'jqxdropdownlist', 'jqxdata', 'jqxdatatable'], function() {
  var reader = {
    init : function() {
      html = '<div id="smarthome_barcode_tab"><ul><li>History</li><li>Einstellungen</li></ul>'
      html += '<div id="smarthome_barcode_history"><div id="smarthome_barcode_table" style="margin:10px;"></div></div>'
      html += '<div id="smarthome_barcode_settings"><table>'
      html += '<tr><td><b>USB-Port:</b></td><td><div id="smarthome_barcode_usb"></div></td><td></td></tr>'
      html += '<tr><td></td><td></td><td><input type="button" value="Senden" id="system_send" /></td></tr>'
      html += '</table></div></div>'
      $('#smarthome_data').html(html)
      $('#smarthome_barcode_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      $('#smarthome_barcode_usb').jqxDropDownList({width: 250, height: 30})
      $('#smarthome_barcode_settings input:button').jqxButton({width: 250, height: 40}).css('margin', '10px')
      $('#smarthome_barcode_settings input:button').off('click')
      $('#smarthome_barcode_settings input:button').on('click', window.sm_b.barcodescanner.send)
      window.sm_b.barcodescanner.call_tab(0)
      $('#smarthome_barcode_tab').off('selected')
      $('#smarthome_barcode_tab').on('selected', function(ev) {
        var selectedTab = ev.args.item
        window.sm_b.barcodescanner.call_tab(selectedTab)
      })
    },
    send : function() {
      var value = $("#smarthome_barcode_usb").jqxDropDownList('val')
      window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                     'modul':'barcodescanner', 'tab': 'setup', 'usb': value}}, function(data) {
      })
    },
    call_tab : function(id) {
      if (id == 0) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'barcodescanner', 'tab': 'history'}}, function(data) {
          var source = {
            localData: data.data,
            dataType: "array",
            dataFields: [
              { name: 'text', type: 'string' },
              { name: 'time', type: 'integer' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source)
          $('#smarthome_barcode_table').jqxDataTable({
            source: dataAdapter,
            columns: [
              { text: 'Text', dataField: 'text', 'width': 350 },
              { text: 'Zeit', dataField: 'time' , 'cellsAlign': 'right', 'width': 200}
            ]
          })
        })
      } else if (id == 1) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'barcodescanner', 'tab': 'setup'}}, function(data) {
          $("#smarthome_barcode_usb").jqxDropDownList('clear')
          var len = data.data.hid.length
          for (var i=0; i<len; i++) {
            $("#smarthome_barcode_usb").jqxDropDownList('addItem', data.data.hid[i])
          }
          var item = $("#smarthome_barcode_usb").jqxDropDownList('getItemByValue', data.data.value)
          $("#smarthome_barcode_usb").jqxDropDownList('selectIndex', item.index )
        })
      } else {
        console.error(id)
      }
    }
  }
  window.sm_b.barcodescanner = reader
  return reader
})

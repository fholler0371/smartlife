import os
import sqlite3
import time

_sl = None
_file = ''
_data = []

def init(sl):
    global _sl, _file, _data
    _sl = sl
    file = sl.const.path_var + '/db'
    try:
        os.mkdir(file)
    except:
        print()
    file += '/barcodescanner.sqlite'
    _file = file
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS history (id integer PRIMARY KEY, value text, time integer)"
    cur.execute(sql)
    conn.commit()
    sql = "SELECT value, time From history order by time desc limit 25"
    cur.execute(sql)
    res = cur.fetchall()
    conn.close()
    for entry in res:
        _data.append({'text': entry[0], 'time':entry[1]})

def save(text):
    global _file
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "delete from history where time < '" + str(int(time.time()-2592000)) + "'"
    cur.execute(sql)
    sql = "insert into history (value, time) values ('%s', '%s')" % (text, str(int(time.time())))
    cur.execute(sql)
    conn.commit()
    conn.close()
 
def check(text):
    global _data
    if len(_data) == 0:
        _data.append({'text':text, 'time':int(time.time())})
        save(text)
    else:
        if _data[0]['text'] == text and (time.time()-_data[0]['time']) < 15:
            return False
        else:
            _data.insert(0, {'text':text, 'time':int(time.time())})
            save(text)
            while len(_data) > 25:
                _data.pop()
    return True

def history():
    global _data
    return _data

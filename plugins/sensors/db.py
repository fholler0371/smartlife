import time
import sqlite3
from threading import Thread
import datetime

_sl = None
_file = ''
_file_minute = ''
_file_day = ''
_data = {}
_change = None
_event = None
_zeitgeber = None

class zeitgeber(Thread):
     def __init__(self, run_minute):
         Thread.__init__(self)
         self.running = True
         self.minute = int(time.time()/60)
         self.run_minute = run_minute
         self.day = int(time.time()/60/60/24)
         self.run_day = run_day

     def run(self):
         global _sl
         while self.running:
             if self.minute != int(time.time()/60):
                 self.minute = int(time.time()/60)
                 try:
                     self.run_minute()
                 except:
                     x = 1
             if self.day != int(time.time()/60/60/24):
                 self.day = int(time.time()/60/60/24)
                 try:
                     self.run_day()
                 except:
                     x = 1
             time.sleep(1)

def init(sl, change, event):
    global _sl, _file, _file_minute, _file_day, _data, _event, _change, _zeitgeber
    _sl = sl
    _change = change
    _event = event
    file = sl.const.path_var + '/db'
    try:
        os.mkdir(file)
    except:
        print()
    _file_minute = file + '/sensors_history.minute'
    _file_day = file + '/sensors_history_day_{year}.sqlite'
    file += '/sensors_state.sqlite'
    _file = file
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS state (id integer PRIMARY KEY, topic text, value text, time integer, seen integer, lvalue text, "
    sql += "ltime integer, friendly text, type text, udp integer, history integer)"
    cur.execute(sql)
    conn.commit()
    sql = "select topic, value, time, seen, lvalue, ltime, friendly, type, udp, history from state"
    cur.execute(sql)
    recs = cur.fetchall()
    conn.close()
    for rec in recs:
        value = rec[1]
        lvalue = rec[4]
        if rec[7] == 'int':
            value = int(value)
            lvalue = int(lvalue)
        if rec[7] == 'float':
            value = float(value)
            lvalue = float(lvalue)
        if rec[7] == 'dict':
            try:
                while value.startswith('%22'):
                    value = value[3:]
                while value.endswith('%22'):
                    value = value[:-3]
                value = json.loads(value.replace('%22', '"'))
                while lvalue.startswith('%22'):
                    lvalue = lvalue[3:]
                while lvalue.endswith('%22'):
                    lvalue = lvalue[:-3]
                lvalue = json.loads(lvalue.replace('%22', '"'))
            except Exception as e:
                _sl.log.error(repr(e))
        _data[rec[0]] = {'topic': rec[0], 'value': value, 'time': rec[2], 'seen': rec[3],
                         'lvalue': lvalue, 'ltime': rec[5], 'friendly': rec[6], 'type': rec[7], 'udp': (1==rec[8]), 'history': rec[9]}
    conn = sqlite3.connect(_file_minute)
    cur = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS history (time integer, sensor text, value text)"
    cur.execute(sql)
    conn.commit()
    conn.close()
    _zeitgeber = zeitgeber(run_minute)
    _zeitgeber.start()

def save(job=None):
    global _sl, _data
    try:
        _sl.log.info('save')
        conn = sqlite3.connect(_file)
        cur = conn.cursor()
        for entry in _data:
            data = _data[entry]
            sql = "select id from state where topic='" + entry + "'"
            cur.execute(sql)
            rec = cur.fetchone()
            udp = '0'
            if data['udp']:
                udp = '1'
            if "dict" == data['type']:
                data['value'] = json.dumps(data['value']).replace('"',"%22")
                data['lvalue'] = json.dumps(data['lvalue']).replace('"',"%22")
            if rec == None:
                sql = "insert into state (topic, value, time, seen, lvalue, ltime, friendly, type, udp, history) values "
                sql += "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                sql = sql % (data['topic'], str(data['value']), str(data['time']), str(data['seen']), str(data['lvalue']),
                             str(data['ltime']), str(data['friendly']), str(data['type']), udp, str(data['history']))
            else:
                sql = "update state set value='%s', time='%s', seen='%s', lvalue='%s', ltime='%s', friendly='%s', type='%s', udp='%s'"
                sql += ", history='%s' where id='%s'"
                sql = sql % (str(data['value']), str(data['time']), str(data['seen']), str(data['lvalue']), str(data['ltime']), str(data['friendly']),
                             str(data['type']), udp, str(data['history']), str(rec[0]))
            cur.execute(sql)
            conn.commit()
        conn.close()
    except Exception as e:
        _sl.log.error(str(e))

def check(data):
    global _sl, _data, _change, _event
    _sl.log.debug(str(data))
    fulltopic = data['name']
    if not('friendly' in data):
        data['friendly'] = fulltopic
    if not('lfriendly' in data):
        data['lfriendly'] = fulltopic
    if fulltopic in _data:
        _data[fulltopic]['seen'] = int(time.time())
        if data['value'] != _data[fulltopic]['value']:
            _data[fulltopic]['ltime'] = _data[fulltopic]['time']
            _data[fulltopic]['lvalue'] = _data[fulltopic]['value']
            _data[fulltopic]['time'] = data['time']
            _data[fulltopic]['value'] = data['value']
            if _data[fulltopic]['friendly'] == data['lfriendly']:
                _data[fulltopic]['friendly'] = data['friendly']
            _data[fulltopic]['type'] = type(data['value']).__name__
            if _change:
                _change(_data[fulltopic])
    else:
        _data[fulltopic] = {'topic': fulltopic, 'value': data['value'], 'time': data['time'], 'seen': int(time.time()),
                            'lvalue': data['value'], 'ltime': data['time'],
                            'friendly': data['friendly'],
                            'type': type(data['value']).__name__, 'udp': False, 'history':0}
        if _change:
            _change(_data[fulltopic])
    if _event:
        _event(_data[fulltopic])

def check_short(data):
    global _sl, _data, _change, _event
    _sl.log.debug(str(data))
    fulltopic = data['topic']
    if fulltopic in _data:
        _data[fulltopic]['seen'] = int(time.time())
        if data['value'] != _data[fulltopic]['value']:
            _data[fulltopic]['ltime'] = _data[fulltopic]['time']
            _data[fulltopic]['lvalue'] = _data[fulltopic]['value']
            _data[fulltopic]['time'] = data['time']
            _data[fulltopic]['value'] = data['value']
            _data[fulltopic]['type'] = type(data['value']).__name__
            if _change:
                _change(_data[fulltopic])
        if data['time'] == _data[fulltopic]['time']:
            return
    else:
        _data[fulltopic] = {'topic': fulltopic, 'value': data['value'], 'time': data['time'], 'seen': int(time.time()),
                            'lvalue': data['value'], 'ltime': data['time'],
                            'friendly': data['friendly'],
                            'type': type(data['value']).__name__, 'udp': False, 'history':0}
        if _change:
            _change(_data[fulltopic])
    if _event:
        _event(_data[fulltopic])

def make_time_string(x):
    t = int(time.time() - x)
    if t < 60:
        s = str(t)+' s'
        return s
    t = int(t/60)
    if t < 60:
        s = str(t)+' min'
        return s
    m = t - 60*int(t/60)
    s = ('0'+str(m))[-2:]
    s = ':'+s
    t = int(t/60)
    if t < 24:
        s = ('0'+str(t))[-2:]+s
        return s
    h = t - 24*int(t/24)
    s = 'd '+('0'+str(m))[-2:]+s
    t = int(t/24)
    s = str(t)+s
    return s

def history():
    global _data, _sl
    out = []
    for name in _data:
       rec = _data[name]
       data = {'topic': rec['topic'], 'friendly': rec['friendly'],
               'value': rec['value'], 'time': make_time_string(rec['time']),
               'lvalue': rec['lvalue'], 'ltime': make_time_string(rec['ltime']),
               'udp': rec['udp'], 'history': rec['history'],
               'seen':  make_time_string(rec['seen'])}
       if rec['type'] == 'dict':
           data['value'] = json.dumps(rec['value'])
           data['lvalue'] = json.dumps(rec['lvalue'])
       if data['history'] == 0:
           data['history'] = 'aus'
       elif data['history'] == 5:
           data['history'] = 'tägl.'
       out.append(data)
    return out

def get_data(name):
    global _data
    if name in _data:
        return _data[name]['value']
    return ""

def get_history(name):
    global _data
    if name in _data:
        return _data[name]['history']
    return 0

def set_data(data):
    global _data, _sl
    if data['topic'] in _data:
        if data['field'] == 'history':
           val = 0
           if data['value'] == 'tägl.':
               val = 5
           _data[data['topic']][data['field']] = val
        else:
            _data[data['topic']][data['field']] = data['value']
    save()
    return {}

def get_names():
    global _data
    out = []
    for entry in _data:
        out.append({'name': entry, 'friendly': _data[entry]['friendly']})
    out = sorted(out, key=lambda sensor: sensor['friendly'].upper())
    return(out)

def stop():
    global _zeitgeber
    _zeitgeber.running = False

def run_minute():
    global _sl, _data, _file_minute
    conn = sqlite3.connect(_file_minute)
    cur = conn.cursor()
    _sl.log.info('run:minute')
    sql = "delete from history where time<'" + str(int(time.time()-172800)) + "'"
    cur.execute(sql)
    conn.commit()
    for name in _data:
        data = _data[name]
        if data['history'] > 0:
            sql = "insert into history (time, sensor, value) values ('"+str(int(time.time()))+"', '"+data['topic']+"', '"+str(data['value'])+"')"
            cur.execute(sql)
    conn.commit()
    conn.close()

def run_day():
    global _sl, _data, _file_minute, _file_day
    _sl.log.info('run:day')
    file = _file_day.replace('{year}', str(datetime.datetime.now().year))
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    conn_min = sqlite3.connect(_file_minute)
    cur_min = conn_min.cursor()
    sql = "CREATE TABLE IF NOT EXISTS history (time integer, sensor text, value text, value_min text, value_max text)"
    cur.execute(sql)
    conn.commit()
    for name in _data:
        data = _data[name]
        if data['history'] == 5:
            sql = "select value from history where time>'" + str(int(time.time()-86400)) + "' and sensor='"+data['topic']+"'"
            cur_min.execute(sql)
            recs = cur_min.fetchall()
            l = len(recs)
            nr = 0
            for rec in recs:
                val = float(rec[0])
                if nr == 0:
                    min = val
                    max = val
                    sum = val
                else:
                    if min > val:
                        min = val
                    if max < val:
                        max = val
                    sum += val
                nr += 1
            sum = sum / l
            sql = "insert into history (time, sensor, value, value_max, value_min) values ('"+str(int(time.time()))+"', '"+data['topic']
            sql += "', '"+str(sum)+"', '"+str(max)+"', '"+str(min)+"')"
            cur.execute(sql)
    conn.commit()
    conn_min.close()
    conn.close()

def get_chart_day(sensor):
    global _sl, _file_day
    file = _file_day.replace('{year}', str(datetime.datetime.now().year))
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    out = []
    sql = "select time, value, value_min, value_max from history where sensor='"+sensor+"' order by time"
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
       out.append({'time': row[0], 'value': float(row[1]), 'value_min': float(row[2]), 'value_max': float(row[3])})
    conn.close()
    return out

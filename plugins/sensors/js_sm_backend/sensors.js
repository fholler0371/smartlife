define(['jqxtabs', 'jqxgrid_selection', 'jqxgrid', 'jqxgrid_edit', 'jqxcheckbox', 'jqxinput'], function() {
  var sensors = {
    init : function() {
      html = '<div id="smarthome_sensors_tab"><ul><li>Status</li><li>Einstellungen</li></ul>'
      html += '<div><div id="smarthome_sensors_state"></div></div>'
      html += '<div><table>'
      html += '<tr><td><b>MQTT-Eingangsserver:</b></td><td><input id="sensor_mqtt_in" /></td><td></td></tr>'
      html += '<tr><td><b>MQTT-Ausgangsserver:</b></td><td><input id="sensor_mqtt_out" /></td><td></td></tr>'
      html += '<tr><td></td><td></td><td><input id="sensor_send" type="button" value="Senden" /></td></tr>'
      html += '</table></div></div>'
      $('#smarthome_data').html(html)
      $('#smarthome_sensors_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      window.sm_b.sensors.call_tab(0)
      $('#smarthome_sensors_tab').off('selected')
      $('#smarthome_sensors_tab').on('selected', function(ev) {
        var selectedTab = ev.args.item
        window.sm_b.sensors.call_tab(selectedTab)
      })
      $('#sensor_mqtt_in, #sensor_mqtt_out').jqxInput({height: 25, width: 250})
      $('#sensor_send').jqxButton()
      $('#sensor_send').off('click')
      $('#sensor_send').on('click', function() {
        data = {mqtt_out:$('#sensor_mqtt_out').val(), mqtt_in:$('#sensor_mqtt_in').val()}
        window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'sensors', 'tab': 'setup',
                       'mqtt_out': data.mqtt_out, 'mqtt_in': data.mqtt_in}}, function(data) {
        })
      })
    },
    call_tab : function(id) {
      if (id == 0) {
        $('#smarthome_sensors_state').parent().css('overflow', 'hidden')
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'sensors', 'tab': 'state'}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'topic', type: 'string' },
              { name: 'friendly', type: 'string' },
              { name: 'value', type: 'string' },
              { name: 'time', type: 'string' },
              { name: 'lvalue', type: 'string' },
              { name: 'ltime', type: 'string' },
              { name: 'udp', type: 'bool' },
              { name: 'seen', type: 'string' },
              { name: 'history', type: 'string' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#smarthome_sensors_state").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            editable: true,
            columns: [
              { text: 'Topic', editable: false, datafield: 'topic', width: 200 },
              { text: 'Name', datafield: 'friendly'},
              { text: 'Wert', editable: false, datafield: 'value', cellsalign: 'right', width: 150},
              { text: 'Zeit', editable: false, datafield: 'time', width: 100, cellsalign: 'right'},
              { text: 'letzter Wert', editable: false, datafield: 'lvalue', width: 150, cellsalign: 'right'},
              { text: 'letzte Zeit', editable: false, datafield: 'ltime', width: 100, cellsalign: 'right'},
              { text: 'gesehen', editable: false, datafield: 'seen', width: 100, cellsalign: 'right'},
              { text: 'MQTT', datafield: 'udp', width: 50, columntype: 'checkbox'},
              { text: 'Verlauf', datafield: 'history', width: 100, columntype: 'dropdownlist',
                createeditor: function (row, value, editor) {
                  editor.jqxDropDownList({ source: ['aus', 'tägl.'], valueMember: 'value' });
                }
              }
            ]
          })
          $("#smarthome_sensors_state").off('cellendedit')
          $("#smarthome_sensors_state").on('cellendedit', function (event) {
            var args = event.args
            var data = $('#smarthome_sensors_state').jqxGrid('getrowdatabyid', args.rowindex)
            window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                           'modul':'sensors', 'tab': 'state',
                           'topic':data.topic, 'field': args.datafield, 'value': args.value}}, function(data) {
            })
          })
        })
      }
      if (id == 1) {
        $('#smarthome_sensors_state').parent().css('overflow', 'hidden')
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'sensors', 'tab': 'setup'}}, function(data) {
          $('#sensor_mqtt_in').jqxInput('val', data.data.mqtt_in)
          $('#sensor_mqtt_out').jqxInput('val', data.data.mqtt_out)
        })
      }
    }
  }
  window.sm_b.sensors = sensors
  return sensors
})

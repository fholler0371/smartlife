import os
import sys
import time
import json

try:
    import paho.mqtt.client as paho
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt').read()
    import paho.mqtt.client as paho

from plugins.sensors import db

_sl = None
_hostIn = ''
_hostOut = ''
_clientHostOut = None
_clientHostIn = None
_hostOutConnected = 0
_hostInConnected = 0

def init(sl, hostIn, hostOut):
    global _sl, _hostIn, _hostOut, _clientHostIn
    _sl = sl
    _hostIn = hostIn
    _hostOut = hostOut
    if _hostIn != "":
        _clientHostIn = paho.Client()
        _clientHostIn.on_connect = onConnectIn
        _clientHostIn.on_disconnect = onDisconnectIn
        _clientHostIn.on_message = onMessageIn
        _clientHostIn.connect(_hostIn, 1883)
        _clientHostIn.loop_start()
        t = time.time()
        while time.time() - t < 5 and _hostInConnected == 0:
            time.sleep(0.1)
        if _hostInConnected == 0:
            _sl.log.error("keine MQTT-Verbindung")
        else:
            _clientHostIn.subscribe("system/#", 2)

def stop():
    global _clientHostOut
    try:
        _clientHostOut.loop_stop(force=True)
    except:
        x=1

def onConnectIn(client, userdata, flags, rc):
    global _hostInConnected
    _hostInConnected = 1

def onDisconnectIn(client, userdata, rc):
    global _hostInConnected
    _hostInConnected = 0

def onMessageIn(client, userdata, message):
    global _sl
    if message.topic.split('/')[1] != _sl.const.host:
        db.check_short(json.loads(message.payload.decode()))

def onConnectOut(client, userdata, flags, rc):
    global _hostOutConnected
    _hostOutConnected = 1

def onDisconnectOut(client, userdata, rc):
    global _hostOutConnected
    _hostOutConnected = 0

def send(data):
    global _sl, _hostOut, _clientHostOut, _hostOutConnected
    if _hostOut != "":
        topic = "system/"+"/".join(data['topic'].split(':', 2))
        if not _clientHostOut:
            _clientHostOut = paho.Client()
            _clientHostOut.on_connect = onConnectOut
            _clientHostOut.on_disconnect = onDisconnectOut
            _clientHostOut.connect(_hostOut, 1883)
            _clientHostOut.loop_start()
        if _hostOutConnected == 0:
            _clientHostOut.reconnect()
        t = time.time()
        while time.time() - t < 5 and _hostOutConnected == 0:
            time.sleep(0.1)
        if _hostOutConnected == 0:
            _sl.log.error("keine MQTT-Verbindung")
        else:
            _clientHostOut.publish(topic, json.dumps({"topic": data['topic'], "friendly": data['friendly'],
                                                      "value": data['value'], "time": data['time']}), 2)            

import os
import json
import time

import lib.plugin as plugin

from plugins.sensors import db, mqtt

class sensors(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.th = None
        self.running = False
        self.timer = None
        self.sub_event = []

    def get_value(self, sensor):
        value = db.get_data(sensor)
        return value

    def get_history(self, sensor):
        value = db.get_history(sensor)
        return value

    def get_chart_day(self, sensor):
        return db.get_chart_day(sensor)

    def new_data(self, data):
        self.sl.log.info('new Data')
        try:
            db.check(data)
        except Exception as e:
            self.sl.log.error(repr(e))

    def bind_event(self, cb):
        self.sub_event.append(cb)

    def event(self, data):
        for cb in self.sub_event:
            cb(data)
        if data['udp']:
            mqtt.send(data)

    def change(self, data):
        return
        self.sl.log.critical(data)

    def sm_api(self, data):
        if data['data']['tab'] == 'state':
            if data['cmd'] == 'get_data':
                data['data'] = db.history()
            elif data['cmd'] == 'set_data':
                data['data'] = db.set_data(data['data'])
        elif data['data']['tab'] == 'setup':
            if data['cmd'] == 'get_data':
                data['data'] = {"mqtt_in": self.config['mqtt_in'], "mqtt_out": self.config['mqtt_out']}
            elif data['cmd'] == 'set_data':
                self.sl.config.plugins[self.name]['mqtt_in'] = data['data']['mqtt_in']
                self.sl.config.plugins[self.name]['mqtt_out'] = data['data']['mqtt_out']
                self.sl.config.save()
        return data

    def sm_backend_menu(self):
        return {'name': 'sensors', 'friendly':'Sensoren'}

    def start(self):
        self.sl.log.info('Start')
        if not self.running:
            db.init(self.sl, self.change, self.event)
            mqtt.init(self.sl, self.config['mqtt_in'], self.config['mqtt_out'])
            self.running = True
            if 'udp_broadcast' in self.sl.plugins.list:
                self.sl.plugins.udp_broadcast.start()
            if 'timer' in self.sl.plugins.list:
                self.sl.plugins.timer.start()
                self.timer = self.sl.plugins.timer.do(3600, db.save)

    def stop(self):
        self.sl.log.info('Stop')
        mqtt.stop()
        db.stop()
        db.save()

    def get_names(self):
        return db.get_names()

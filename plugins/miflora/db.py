import sqlite3

_sl = None
_file = ""

def init(sl, miflora):
    global _sl, _file
    if miflora.config['backend']:
        from plugins.miflora import scan
    _sl = sl
    file = sl.const.path_var + '/db'
    try:
        os.mkdir(file)
    except:
        print()
    file += '/miflora.sqlite'
    _file = file
    conn = sqlite3.connect(file)
    cur = conn.cursor()
    if miflora.config['backend']:
        sql = "CREATE TABLE IF NOT EXISTS state (id integer PRIMARY KEY, name text, lname text, addr text, fw text, temp real, hum integer, "
        sql += "light integer, con integer, bat integer, last integer)"
        cur.execute(sql)
        sql = "CREATE TABLE IF NOT EXISTS channels (id integer PRIMARY KEY, name text, friendly text, value text, last integer, send integer, "
        sql += "typ text, lfriendly text)"
        cur.execute(sql)
    if miflora.config['frontend']:
        sql = "CREATE TABLE IF NOT EXISTS cards (id integer PRIMARY KEY, name text, con_sensor text, hum_sensor text, con_limit integer, "
        sql += "hum_limit integer)"
        cur.execute(sql)
    conn.commit()
    if miflora.config['backend']:
        sql = "select name, addr, fw, temp, hum, light, con, bat, last, lname from state order by name"
        cur.execute(sql)
        recs = cur.fetchall()
        sql = "select name, friendly, value, last, send, typ, lfriendly from channels order by friendly"
        cur.execute(sql)
        recs_ch = cur.fetchall()
    conn.close()
    if miflora.config['backend']:
        for rec in recs:
            scan._sensors[rec[1]] = {'name': rec[0], 'addr': rec[1], 'fw': rec[2], 'temp': rec[3],
                                     'hum': rec[4], 'light': rec[5], 'con': rec[6], 'bat': rec[7], 'last': rec[8], 'lname': rec[9]}
        for rec in recs_ch:
            send = rec[4] == 1
            value = rec[2]
            if rec[5] == 'int':
                value = int(value)
            elif rec[5] == 'float':
                value = float(value)
            scan._channels[rec[0]] = {'name': rec[0], 'friendly': rec[1], 'lfriendly': rec[6], 'value': value, 'last': rec[3], 'send': send}

def save():
    global _sl, _file, scan
    _sl.log.info('save')
    from plugins.miflora import scan
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    for entry in scan._sensors:
        _sl.log.debug(entry)
        data = scan._sensors[entry]
        sql = "select id from state where addr='"+data['addr']+"'"
        cur.execute(sql)
        rec = cur.fetchone()
        if rec == None:
            sql = "insert into state (name, lname, addr, fw, temp, hum, light, con, bat, last) values ('"+data['name']+"', '"+data['lname']+"',"
            sql += "'"+data['addr']+"', '"
            sql += data['fw']+"', '"+str(data['temp'])+"', '"+str(data['hum'])+"', '"+str(data['light'])+"', '"+str(data['con'])
            sql += "', '"+str(data['bat'])+"','"+str(data['last'])+"')"
        else:
            sql = "update state set name='"+data['name']+"', fw='"+data['fw']+"', temp='"+str(data['temp'])+"', hum='"+str(data['hum'])
            sql += "', lname='"+str(data['lname'])
            sql += "', light='"+str(data['light'])+"', con='"+str(data['con'])+"', bat='"+str(data['bat'])+"', last='"+str(data['last'])
            sql += "' where id='"+str(rec[0])+"'"
        cur.execute(sql)
        conn.commit()
    for entry in scan._channels:
        _sl.log.debug(entry)
        data = scan._channels[entry]
        sql = "select id from channels where name='"+data['name']+"'"
        cur.execute(sql)
        rec = cur.fetchone()
        typ = type(data['value']).__name__
        send = '0'
        if data['send']:
            send = '1'
        if rec == None:
            sql = "insert into channels (name, friendly, value, typ, last, send, lfriendly) values ('"+data['name']+"', '"+data['friendly']+"', '"
            sql += str(data['value'])+"', '"+typ+"', '"+str(data['last'])+"', '"+send+"', '"+data['lfriendly']+"')"
        else:
            sql = "update channels set friendly='"+data['friendly']+"', value='"+str(data['value'])+"', typ='"+typ+"', last='"+str(data['last'])
            sql += "', send='"+send+"', lfriendly='"+data['lfriendly']+"'"
            sql += " where id='"+str(rec[0])+"'"
        cur.execute(sql)
        conn.commit()
    conn.close()

def new_card():
    global _sl, _file
    _sl.log.info('new card')
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "insert into cards (name) values ('Neue Pflanze')"
    cur.execute(sql)
    conn.commit()
    sql = "select max(id) from cards"
    cur.execute(sql)
    row = cur.fetchone()
    conn.close()
    return {'id': row[0], 'name': 'Neue Pflanze'}

def get_cards():
    global _sl, _file
    _sl.log.info('get cards')
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "select id, name from cards order by name, id"
    cur.execute(sql)
    recs = cur.fetchall()
    conn.close()
    out = []
    for rec in recs:
        out.append({'id': rec[0], 'name': rec[1]})
    return out

def delete_card(id):
    global _sl, _file
    _sl.log.info('delete cards')
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "delete from cards where id='"+str(id)+"'"
    cur.execute(sql)
    conn.commit()
    conn.close()
    return {'id': id}

def get_card(id):
    global _sl, _file
    _sl.log.info('delete cards')
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "select name, con_sensor, hum_sensor, con_limit, hum_limit from cards where id='"+str(id)+"'"
    cur.execute(sql)
    row = cur.fetchone()
    conn.close()
    return {'id': id, 'name': row[0], 'con_sensor': row[1], 'hum_sensor': row[2], 'con_limit': row[3], 'hum_limit': row[4]}

def set_card(data):
    global _sl, _file
    _sl.log.info('save card')
    conn = sqlite3.connect(_file)
    cur = conn.cursor()
    sql = "update cards set name='"+data['name']+"', con_sensor='"+data['con_sensor']+"', hum_sensor='"+data['hum_sensor']
    sql += "', con_limit='"+str(data['con_limit'])+"', hum_limit='"+ str(data['hum_limit'])
    sql += "' where id='"+str(data['id'])+"'"
    cur.execute(sql)
    conn.commit()
    conn.close()
    return {'id': data['id']}

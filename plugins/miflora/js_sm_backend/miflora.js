define(['jqxtabs', 'jqxcheckbox', 'jqxbutton', 'jqxdata', 'jqxlistbox', 'jqxsplitter', 'jqxinput', 'jqxfileupload', 'jqxdropdownlist',
        'jqxnumberinput'], function() {
  var miflora = {
    init : function() {
      html = '<div id="smarthome_miflora_tab"><ul><li>Pflanzenkarten</li><li>Kanäle</li><li>Sensoren</li><li>Module</li></ul>'
      html += '<div><div id="flora_card_split"><div><div id="miflora_cards"></div><input id="miflora_add" type="button" value="Hinzufügen" '
      html += 'style="position: absolute; bottom: 0;" /><input id="miflora_delete" type="button" value="Löschen" '
      html += 'style="position: absolute; bottom: 0; right: 0;" /></div>'
      html += '<div><table><tr><td><b>Name: </b></td><td><input id="miflora_cards_name" /></td><td></td></tr>'
      html += '<tr><td><b>Düngersensor: </b></td><td><div id="miflora_cards_con_sensor"></div></td><td></td></tr>'
      html += '<tr><td><b>Düngerlimit: </b></td><td><div id="miflora_cards_con_limit"></div></td><td></td></tr>'
      html += '<tr><td><b>Feuchtigkeitssensor: </b></td><td><div id="miflora_cards_hum_sensor"></div></td><td></td></tr>'
      html += '<tr><td><b>Feuchtigkeitslimit: </b></td><td><div id="miflora_cards_hum_limit"></div></td><td></td></tr>'
      html += '<tr><td></td><td></td><td><input id="miflora_cards_save" type="button" value="Speichern" /></td></tr>'
      html += '<tr><td><b>Bild hochladen (150x150):</b</td><td colspan="2"><div id="miflora_cards_pic"></div></td></tr>'
      html += '<tr><td></td><td colspan="2"><div id="miflora_cards_image"></div></td></tr>'
      html += '</table></div></div></div>'
      html += '<div><div id="smarthome_miflora_channels"></div></div>'
      html += '<div><div id="smarthome_miflora_sensors"></div></div>'
      html += '<div><table><tr><td><b>Frontend</b></td><td><div id="flora_front" style="margin-left: 10px"></div></td><td></td></tr>'
      html += '<tr><td><b>Backend</b></td><td><div id="flora_back" style="margin-left: 10px"></div></td><td></td></tr>'
      html += '<tr><td></td><td></td><td><input id="miflora_send" type="button" value="Senden" /></td></tr></table></div>'
      html += '</div>'
      $('#smarthome_data').html(html)
      $('#smarthome_miflora_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      window.sm_b.miflora.call_tab(0)
      $('#smarthome_miflora_tab').off('selected')
      $('#smarthome_miflora_tab').on('selected', function(ev) {
        var selectedTab = ev.args.item
        window.sm_b.miflora.call_tab(selectedTab)
      })
      $('#flora_front, #flora_back').jqxCheckBox({height: 25})
      $('#miflora_send').jqxButton()
      $('#miflora_send').off('click')
      $('#miflora_send').on('click', function() {
        data = {"frontend": $('#flora_front').val(),
                "backend": $('#flora_back').val()}
        window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'miflora', 'tab': 'module', 'frontend': data.frontend, 'backend': data.backend}}, function() {})
      })
      window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                     'modul':'miflora', 'tab': 'module'}}, function(data) {
        if (!data.data.frontend) {
          $('#smarthome_miflora_tab').jqxTabs('disableAt', 0)
        }
        if (!data.data.backend) {
          $('#smarthome_miflora_tab').jqxTabs('disableAt', 1)
          $('#smarthome_miflora_tab').jqxTabs('disableAt', 2)
        }
      })
      $('#miflora_add, #miflora_delete').jqxButton({width: 139, height: 30})
      $('#miflora_cards').jqxListBox({width: 298, height: 'calc( 100% - 34px )'})
      $('#miflora_add').off('click')
      $('#miflora_add').on('click', function() {
        window.smcall({'client': 'smarthome.backend', 'cmd':'new', 'data': {'server': window.module.smarthome.server,
                       'modul':'miflora', 'tab': 'cards'}}, function(data) {
          $('#miflora_cards').jqxListBox('addItem', {value: data.data.id, label: data.data.name})
        })
      })
      $('#miflora_delete').off('click')
      $('#miflora_delete').on('click', function() {
        var item = $('#miflora_cards').jqxListBox('getSelectedItem')
        window.smcall({'client': 'smarthome.backend', 'cmd':'delete', 'data': {'server': window.module.smarthome.server,
                       'modul':'miflora', 'tab': 'cards', 'id': item.value}}, function(data) {
        })
        $("#miflora_cards").jqxListBox('removeItem', item)
      })
      $('#miflora_cards').off('select')
      $('#miflora_cards').on('select', function(ev) {
        var args = ev.args
        if (args) {
          window.smcall({'client': 'smarthome.backend', 'cmd':'get_card', 'data': {'server': window.module.smarthome.server,
                         'modul':'miflora', 'tab': 'cards', 'id': args.item.value}}, function(data) {
            $('#miflora_cards_name').jqxInput('val', data.data.name)
            var sensor =  data.data.con_sensor
            if (sensor == null) {
              sensor = ""
            }
            var item = $('#miflora_cards_con_sensor').jqxDropDownList('getItemByValue', sensor)
            if (item == undefined) {
              $("#miflora_cards_con_sensor").jqxDropDownList('selectIndex', -1 )
            } else {
              $("#miflora_cards_con_sensor").jqxDropDownList('selectItem', item )
            }
            var sensor =  data.data.hum_sensor
            if (sensor == null) {
              sensor = ""
            }
            var item = $('#miflora_cards_hum_sensor').jqxDropDownList('getItemByValue', sensor)
            if (item == undefined) {
              $("#miflora_cards_hum_sensor").jqxDropDownList('selectIndex', -1 )
            } else {
              $("#miflora_cards_hum_sensor").jqxDropDownList('selectItem', item )
            }
            var limit = data.data.con_limit
            if (limit == null) {
              limit = 0
            }
            $("#miflora_cards_con_limit").jqxNumberInput('val', limit )
            var limit = data.data.hum_limit
            if (limit == null) {
              limit = 0
            }
            $("#miflora_cards_hum_limit").jqxNumberInput('val', limit )
            if (data.data.file == "") {
              $('#miflora_cards_image').html("")
            } else {
              var html = '<img src="/api-core/get_file?_='+encodeURI(data.data.file)
              html += '" style="width:150px; height:150px;border-radius: 10%; border-style:solid;">'
              $('#miflora_cards_image').html(html)
            }
          })
        }
      })
      $('#miflora_cards_name').jqxInput({width: 250, height:30})
      $('#miflora_cards_con_sensor, #miflora_cards_hum_sensor').jqxDropDownList({width: 250, height:30})
      $('#miflora_cards_con_limit').jqxNumberInput({width: 250, height:30, decimalDigits:0, digits:3, spinButtons: true, min: 0, max: 999})
      $('#miflora_cards_hum_limit').jqxNumberInput({width: 250, height:30, decimalDigits:0, digits:2, spinButtons: true, min: 0, max: 99})
      $('#miflora_cards_save').jqxButton()
      $('#miflora_cards_save').off('click')
      $('#miflora_cards_save').on('click', function() {
        var item = $('#miflora_cards').jqxListBox('getSelectedItem'),
            id = item.value,
            name = $('#miflora_cards_name').val(),
            con_sensor = "",
            hum_sensor = ""
        item.label = name
        $('#miflora_cards').jqxListBox('updateItem', item, id)
        item = $("#miflora_cards_con_sensor").jqxDropDownList('getSelectedItem')
        if (item != undefined) {
          con_sensor = item.value
        }
        item = $("#miflora_cards_hum_sensor").jqxDropDownList('getSelectedItem')
        if (item != undefined) {
          hum_sensor = item.value
        }
        var con_limit = $("#miflora_cards_con_limit").jqxNumberInput('val')
        var hum_limit = $("#miflora_cards_hum_limit").jqxNumberInput('val')
        window.smcall({'client': 'smarthome.backend', 'cmd':'set_card', 'data': {'server': window.module.smarthome.server,
                       'modul':'miflora', 'tab': 'cards', 'id': id, 'name': name, 'con_sensor': con_sensor,
                       'hum_sensor': hum_sensor, 'con_limit': con_limit, 'hum_limit': hum_limit}}, function(data) {
        })
      })
      $('#miflora_cards_pic').jqxFileUpload({
        width: 350,
        multipleFilesUpload: false,
        accept: 'image/*', /* */
        fileInputName: 'file',
        localization: {
          browseButton: 'wählen',
          uploadButton: 'Hochladen',
          cancelButton: 'Abrechen',
          uploadFileTooltip: 'Datei hochladen',
          cancelFileTooltip: 'abrechen'
        },
        uploadUrl : '/api-core/upload.php'
      })
      $('#miflora_cards_pic').off('upload')
      $("#miflora_cards_pic").on('uploadStart', function(){
        var item = $('#miflora_cards').jqxListBox('getSelectedItem'),
            id = item.value,
            data = {'client': 'smarthome.backend', 'cmd':'file_upload', 'data': {'server': window.module.smarthome.server,
                    'modul':'miflora', 'tab': 'cards', 'id': id}, 'token': sessionStorage.getItem("token")}
        $('form[action="/api-core/upload.php"]').append('<input type="hidden" name="data" value="'+encodeURI(JSON.stringify(data))+'" />');
      })
    },
    call_tab : function(id) {
      if (id == 0) {
        $('#flora_card_split').jqxSplitter({height: 'calc( 100% - 4px )', width: 'calc( 100% - 4px )', resizable: false, panels: [{ size: 300 }]})
        $('#flora_card_split').parent().css('overflow', 'hidden')
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'miflora', 'tab': 'cards'}}, function(data) {
          $('#miflora_cards').jqxListBox('clear')
          var l = data.data.sensors.length
          $('#miflora_cards_con_sensor').jqxDropDownList('clear')
          $('#miflora_cards_hum_sensor').jqxDropDownList('clear')
          for (var i = 0; i<l; i++) {
             $('#miflora_cards_con_sensor').jqxDropDownList('addItem', {value: data.data.sensors[i].name, label: data.data.sensors[i].friendly})
             $('#miflora_cards_hum_sensor').jqxDropDownList('addItem', {value: data.data.sensors[i].name, label: data.data.sensors[i].friendly})
          }
          var l = data.data.cards.length
          for (var i = 0; i<l; i++) {
             $('#miflora_cards').jqxListBox('addItem', {value: data.data.cards[i].id, label: data.data.cards[i].name})
          }
          if (l > 0) {
            $('#miflora_cards').jqxListBox('selectIndex', 0)
          }
        })
      } else if (id == 1) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'miflora', 'tab': 'channels'}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'name', type: 'string' },
              { name: 'friendly', type: 'string' },
              { name: 'value', type: 'string' },
              { name: 'last', type: 'string' },
              { name: 'send', type: 'bool' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#smarthome_miflora_channels").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            editable: true,
            columns: [
              { text: 'ID', hidden: true, datafield: 'name'},
              { text: 'Name', datafield: 'friendly'},
              { text: 'Wert', editable: false, datafield: 'value', cellsalign: 'right',  width: 150},
              { text: 'Alter', editable: false, datafield: 'last', cellsalign: 'right',  width: 150},
              { text: 'Senden', datafield: 'send', cellsalign: 'right',  width: 100, columntype: 'checkbox'}
            ]
          })
          $('#smarthome_miflora_channels').parent().css('overflow', 'hidden')
          $("#smarthome_miflora_channels").off('cellendedit')
          $("#smarthome_miflora_channels").on('cellendedit', function (event) {
            var args = event.args
            var data = $('#smarthome_miflora_channels').jqxGrid('getrowdatabyid', args.rowindex)
            window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                           'modul':'miflora', 'tab': 'channels',
                           'name':data.name, 'field': args.datafield, 'value': args.value}}, function(data) {
            })
          })
        })
      } else if (id == 2) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'miflora', 'tab': 'sensors'}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'addr', type: 'string' },
              { name: 'name', type: 'string' },
              { name: 'hum', type: 'integer' }
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#smarthome_miflora_sensors").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            editable: true,
            columns: [
              { text: 'MAC-Adresse', editable: false, datafield: 'addr', width: 200 },
              { text: 'Name', datafield: 'name'},
              { text: 'Feuchtigkeit (%)', editable: false, datafield: 'hum', cellsalign: 'right',  width: 150}
            ]
          })
          $('#smarthome_miflora_sensors').parent().css('overflow', 'hidden')
          $("#smarthome_miflora_sensors").off('cellendedit')
          $("#smarthome_miflora_sensors").on('cellendedit', function (event) {
            var args = event.args
            var data = $('#smarthome_miflora_sensors').jqxGrid('getrowdatabyid', args.rowindex)
            window.smcall({'client': 'smarthome.backend', 'cmd':'set_data', 'data': {'server': window.module.smarthome.server,
                           'modul':'miflora', 'tab': 'sensors',
                           'addr':data.addr, 'name': args.value}}, function(data) {
            })
          })
        })
      } else if (id == 3) {
        window.smcall({'client': 'smarthome.backend', 'cmd':'get_data', 'data': {'server': window.module.smarthome.server,
                       'modul':'miflora', 'tab': 'module'}}, function(data) {
          if (data.data.frontend) {
            $('#flora_front').jqxCheckBox('check')
          }
          if (data.data.backend) {
            $('#flora_back').jqxCheckBox('check')
          }
        })
      } else {
        console.error(id)
      }
    }
  }
  window.sm_b.miflora = miflora
  return miflora
})

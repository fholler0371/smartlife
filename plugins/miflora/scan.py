import os
import sys
from threading import Timer, Thread
import time

from plugins.miflora import db

try:
    from btlewrap.bluepy import BluepyBackend
except:
    os.popen(sys.executable + ' -m pip install bluepy').read()
    from btlewrap.bluepy import BluepyBackend

try:
    from miflora import miflora_scanner
    from miflora.miflora_poller import MiFloraPoller
except:
    os.popen(sys.executable + ' -m pip install miflora').read()
    from miflora import miflora_scanner
    from miflora.miflora_poller import MiFloraPoller

_sl = None
_sensors = {}
_channels = {}

class sendout(Thread):
    def __init__(self, sl, data):
       Thread.__init__(self)
       self.sl = sl
       self.data = data

    def run(self):
       try:
           self.sl.plugins.sensors.new_data(self.data)
       except Exception as e:
           self.sl.log.error(str(e))

def found(addr):
    global _sl, _sensors
    if not(addr in _sensors):
        _sensors[addr] = {'name': addr.replace(':', '_'), 'lname': addr.replace(':', '_'), 'addr': addr, 'fw': '', 'temp':0.0, 'hum':0,
                          'light':0, 'con':0, 'bat':0, 'last':0}
        _sl.log.error('New Sensor: '+str(_sensors[addr]))

def init(sl):
    global _sl, _loop
    _sl = sl
    os.popen("sudo setcap 'cap_net_raw,cap_net_admin+eip' `which hcitool`").read()
    t = Timer(10, scan)
    t.start()

def channels_data(name, friendly, lfriendly, value, last):
    global _sl, _channels
    if not(name in _channels):
        _channels[name] = {'name' : name, 'friendly': friendly, 'lfriendly': lfriendly, 'value': value, 'last':last, 'send': False}
    else:
        if lfriendly == _channels[name]['friendly']:
            _channels[name]['lfriendly'] = _channels[name]['friendly']
            _channels[name]['friendly'] = friendly
        _channels[name]['value'] = value
        _channels[name]['last'] = last
    _sl.log.debug(str(_channels[name]))
    if _channels[name]['send']:
        if 'sensors' in _sl.plugins.list:
            th = sendout(_sl, {'name': _sl.const.host+':miflora:'+name,
                               'friendly': _sl.const.host+':miflora:'+_channels[name]['friendly'],
                               'lfriendly': _sl.const.host+':miflora:'+_channels[name]['lfriendly'],
                               'value': _channels[name]['value'],
                               'time': _channels[name]['last']})
            th.start()

def scan(job= None):
    global _sl, _sensors
    _sl.log.info('Start Scan')
    try:
        devices = miflora_scanner.scan(BluepyBackend, 30)
        for device in devices:
            found(device)
        time.sleep(5)
        for sensor in _sensors:
            if _sensors[sensor]['last'] + 19800 < time.time():
                _sl.log.debug("Lese: "+sensor)
                poller = MiFloraPoller(sensor, BluepyBackend)
                poller.ble_timeout = 30
                retry = 5
                while retry > 0:
                    retry -= 1
                    try:
                        _sensors[sensor]['fw'] = poller.firmware_version()
                        _sensors[sensor]['temp'] = poller.parameter_value('temperature')
                        _sensors[sensor]['hum'] = poller.parameter_value('moisture')
                        _sensors[sensor]['light'] = poller.parameter_value('light')
                        _sensors[sensor]['con'] = poller.parameter_value('conductivity')
                        _sensors[sensor]['bat'] = poller.parameter_value('battery')
                        _sensors[sensor]['last'] = int(time.time())
                        retry = -1
                    except Exception as e:
                        x=1
                    if retry == -1:
                        channels_data(sensor.replace(':', '.')+':fw', _sensors[sensor]['name']+':fw', _sensors[sensor]['lname']+':fw',
                                      _sensors[sensor]['fw'], _sensors[sensor]['last'])
                        channels_data(sensor.replace(':', '.')+':temp', _sensors[sensor]['name']+':temp', _sensors[sensor]['lname']+':temp',
                                      _sensors[sensor]['temp'], _sensors[sensor]['last'])
                        channels_data(sensor.replace(':', '.')+':hum', _sensors[sensor]['name']+':hum', _sensors[sensor]['lname']+':hum',
                                      _sensors[sensor]['hum'], _sensors[sensor]['last'])
                        channels_data(sensor.replace(':', '.')+':light', _sensors[sensor]['name']+':light', _sensors[sensor]['lname']+':light',
                                      _sensors[sensor]['light'], _sensors[sensor]['last'])
                        channels_data(sensor.replace(':', '.')+':con', _sensors[sensor]['name']+':con', _sensors[sensor]['lname']+':con',
                                      _sensors[sensor]['con'], _sensors[sensor]['last'])
                        channels_data(sensor.replace(':', '.')+':bat', _sensors[sensor]['name']+':bat', _sensors[sensor]['lname']+':bat',
                                      _sensors[sensor]['bat'], _sensors[sensor]['last'])
                if retry == 0:
                    _sl.log.error('no connection: '+sensor)
        db.save()
    except Exception as e:
        _sl.log.error(str(e))

import time
import os
import shutil
import glob

_sl = None
_miflora = None

from plugins.miflora import db

def init(sl, miflora):
    global _sl, _miflora
    _sl = sl
    _miflora = miflora

def module(cmd, data):
    global _sl, _miflora
    if cmd == 'get_data':
        return _miflora.config
    elif cmd == 'set_data':
        _sl.config.plugins[_miflora.name]['frontend'] = data['frontend']
        _sl.config.plugins[_miflora.name]['backend'] = data['backend']
        _sl.config.save()
    return data

def sensors(cmd, data):
    global _sl
    from plugins.miflora import scan
    if cmd == 'get_data':
        out = []
        for sensor in scan._sensors:
            _data = scan._sensors[sensor]
            _sl.log.debug(str(_data))
            out.append({'name': _data['name'], 'addr': _data['addr'], 'hum': _data['hum']})
        return out
    elif cmd == 'set_data':
        if data['addr'] in scan._sensors:
            scan._sensors[data['addr']]['lname'] = scan._sensors[data['addr']]['name']
            scan._sensors[data['addr']]['name'] = data['name']
            db.save()
    return data

def channels(cmd, data):
    global _sl
    from plugins.miflora import scan
    if cmd == 'get_data':
        out = []
        for channel in scan._channels:
            _data = scan._channels[channel]
            out.append({'name': _data['name'], 'friendly': _data['friendly'], 'value': str(_data['value']), 'send': _data['send'],
                        'last': make_time_string(_data['last'])})
        return out
    elif cmd == 'set_data':
        if data['name'] in scan._channels:
            if data['field'] == 'friendly':
                scan._channels[data['name']]['lfriendly'] = scan._channels[data['name']]['friendly']
                scan._channels[data['name']]['friendly'] = data['value']
            elif data['field'] == 'send':
                scan._channels[data['name']]['send'] = data['value']
            db.save()
    return data

def file_upload(data):
    global _sl
    dir = _sl.const.path_var + '/files'
    try:
        os.mkdir(dir)
    except:
        x = 1
    dir += '/server'
    try:
        os.mkdir(dir)
    except:
        x = 1
    dir += '/miflora'
    try:
        os.mkdir(dir)
    except:
        x = 1
    dest = dir + '/' + str(data['id']) + '.' + data['filename'].split('.')[-1]
    try:
        os.popen("rm "+_sl.const.path_var+"/files/server/miflora/"+str(data['id'])+".*").read()
        shutil.copyfile(data['tmpname'], dest)
    except:
        x = 1
    os.popen("rm "+_sl.const.path_var+"/tmp/miflora_*").read()
    return {}

def get_filekey(id):
    global _sl
    path = _sl.const.path_var+'/files/server/miflora/'+str(id)+'.*'
    file = ""
    for name in glob.glob(path):
        data = {'timeout': int(time.time()+1800), 'file': name[len(_sl.const.path_var+'/files/'):]}
        if 'authencitator' in _sl.plugins.list:
            return _sl.plugins.authencitator.file_encode(data)
    return ""

def cards(cmd, data):
    if cmd == "new":
        data = db.new_card()
    elif cmd == "get_data":
        data = {'cards': db.get_cards(), 'sensors': []}
        if 'sensors' in _sl.plugins.list:
            for sensor in _sl.plugins.sensors.get_names():
                if 'miflora' in sensor['name']:
                    data['sensors'].append(sensor)
    elif cmd == "delete":
        data = db.delete_card(data['id'])
    elif cmd == "get_card":
        data = db.get_card(data['id'])
        data['file'] = get_filekey(data['id'])
    elif cmd == "set_card":
        data = db.set_card(data)
    elif cmd == "file_upload":
        data = file_upload(data)
    return data

def api(data):
    global _sl
    cmd = data['cmd']
    tab = data['data']['tab']
    _data = data['data']
    _sl.log.debug(cmd)
    _sl.log.debug(tab)
    _sl.log.debug(str(_data))
    if tab == 'sensors':
        data['data'] = sensors(cmd, _data)
    elif tab == 'module':
        data['data'] = module(cmd, _data)
    elif tab == 'channels':
        data['data'] = channels(cmd, _data)
    elif tab == 'cards':
        data['data'] = cards(cmd, _data)
    return data

def make_time_string(x):
    t = int(time.time() - x)
    if t < 60:
        s = str(t)+' s'
        return s
    t = int(t/60)
    if t < 60:
        s = str(t)+' min'
        return s
    m = t - 60*int(t/60)
    s = ('0'+str(m))[-2:]
    s = ':'+s
    t = int(t/60)
    if t < 24:
        s = ('0'+str(t))[-2:]+s
        return s
    h = t - 24*int(t/24)
    s = 'd '+('0'+str(m))[-2:]+s
    t = int(t/24)
    s = str(t)+s
    return s

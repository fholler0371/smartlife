import os
import sys

import lib.plugin as plugin
from plugins.miflora import db, api

class miflora(plugin.pluginBase):
    def __init__(self, sl, manifest):
        plugin.pluginBase.__init__(self, sl, manifest)
        sl.log.info('Init Plugin')
        self.running = False

    def main_menu(self, keyword):
        if 'miflora' == keyword:
            return {'label': 'Pflanzen', 'mod': 'miflora', 'p1':''}

    def sm_backend_menu(self):
        return {'name': 'miflora', 'friendly':'Pflanzen'}

    def get_card(self, name):
        id = int(name.split('.')[1])
        card = db.get_card(id)
        card['card'] = name
        del card['id']
        card['con_value'] = 0
        card['hum_value'] = 0
        if 'sensors' in self.sl.plugins.list:
            card['con_value'] = self.sl.plugins.sensors.get_value(card['con_sensor'])
            del card['con_sensor']
            card['hum_value'] = self.sl.plugins.sensors.get_value(card['hum_sensor'])
            del card['hum_sensor']
        card['file'] = api.get_filekey(id)
        return card

    def api(self, data):
        if data['cmd'] == 'get_data':
            out = []
            dout = []
            for card in db.get_cards():
                out.append('miflora.'+str(card['id']))
            for card in out:
                dout.append(self.get_card(card))
            data['data'] = dout
        elif data['cmd'] == 'get_chart':
            id = int(data['data']['card'].split('.')[1])
            card = db.get_card(id)
            if data['data']['sensor'] == 'hum':
                sensor = card['hum_sensor']
                label = 'Feuchtigkeit in %'
            elif data['data']['sensor'] == 'con':
                sensor = card['con_sensor']
                label = 'Düngung µS/cm'
            else:
                sensor = 'none'
                label = ''
            history = 0
            if 'sensors' in self.sl.plugins.list:
                history = self.sl.plugins.sensors.get_history(sensor)
            typ = history
            values = []
            if 'sensors' in self.sl.plugins.list:
                if typ == 5:
                    values = self.sl.plugins.sensors.get_chart_day(sensor)
            data['data'] = {'win_title': card['name'], 'label': label, 'history': history, 'typ': typ, 'values': values}
        return data

    def sm_api(self, data):
        return api.api(data)

    def start(self):
        self.sl.log.info('Start')
        if not self.running:
            self.running = True
            api.init(self.sl, self)
            db.init(self.sl, self)
            if self.config['backend']:
                from plugins.miflora import scan
                scan.init(self.sl)
                if 'sensors' in self.sl.plugins.list:
                    self.sl.plugins.sensors.start()
                if 'timer' in self.sl.plugins.list:
                    self.sl.plugins.timer.start()
                    self.timer = self.sl.plugins.timer.do(3600, scan.scan)

    def stop(self):
        self.sl.log.info('Stop')

define(['module', 'jqxpanel'], function(module) {
  var miflora = {
    init : function() {
      html = '<div style="width:100%; height:100%;"><div id="miflora_panel"></div>'
      $('#client_area').html(html+'</div>')
      $(window).off('resize', window.module.miflora.resize)
      $(window).on('resize', window.module.miflora.resize)
      window.module.miflora.resize()
      $("#miflora_panel").jqxPanel({ width: '100%', height: '100%'})
      window.smcall({'client': 'miflora', 'cmd':'get_data', 'data': {}}, function(data) {
        var len = data.data.length
        for (var i=0; i<len; i++) {
           var card = data.data[i]
           html = '<div style="position: relative; height: 180px;margin: 5px;border: solid;border-radius: 20px;border-width: 1px;" data-card="'
           html += card.card+'"><img src="/api-core/get_file?_='+encodeURI(card.file)
           html += '" style="width:150px; height:150px;border-radius: 10%; border-style:solid; top: 10px; left: 10px; position: relative;">'
           html += '<span style="font-family: '+"'Fredoka One'"+', cursive; font-size: 2.25rem; position: absolute; top: 10px; left: 185px;">'
           html += card.name+'</span><table style="position: absolute; left: 190px; top: 55px;"><tr height=40><td><b>Düngung:</b></td>'
           html += '<td align="right"><span'
           if (card.con_value < card.con_limit) {
             html += ' style="color:red; font-weight: bold"'
           }
           html += '>'+card.con_value+'</span></td><td><input type="button" class="card_miflors_chart_con" /></td></tr><tr height=40><td>'
           html += '<b>Feuchtigkeit:</b></td><td align="right"><span '
           if (card.hum_value < card.hum_limit) {
             html += ' style="color:red; font-weight: bold"'
           }
           html += '>'+card.hum_value+' %</span></td><td><input type="button" class="card_miflors_chart_hum" /></td></tr></table></div>'
           $("#miflora_panel").jqxPanel('append', html)
        }
        $(".card_miflors_chart_con").jqxButton({ imgSrc: "/lib/img/diagramm-white.svg", imgPosition: "center", width: 24, height: 24 })
        $(".card_miflors_chart_con").off('click')
        $(".card_miflors_chart_con").on('click', function(ev) {
          var card = $(ev.currentTarget).parent().closest('div').data('card')
          window.smcall({'client': 'miflora', 'cmd':'get_chart', 'data': {'card': card, 'sensor': 'con'}}, function(data) {
            requirejs(['chart'], function(chart) {
              chart.execute(data.data)
            })
          })
        })
        $(".card_miflors_chart_hum").jqxButton({ imgSrc: "/lib/img/diagramm-white.svg", imgPosition: "center", width: 24, height: 24 })
        $(".card_miflors_chart_hum").off('click')
        $(".card_miflors_chart_hum").on('click', function(ev) {
          var card = $(ev.currentTarget).parent().closest('div').data('card')
          window.smcall({'client': 'miflora', 'cmd':'get_chart', 'data': {'card': card, 'sensor': 'hum'}}, function(data) {
            requirejs(['chart'], function(chart) {
              chart.execute(data.data)
            })
          })
        })
      })
    },
    stop : function() {
    },
    resize: function() {
      $('#client_area').children().height($('#client_area').height())
    }
  }
  miflora['init_data'] = window.module_const[module.id]
  window.module.miflora = miflora
  return miflora
})

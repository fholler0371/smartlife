import json
import os
import boto3

def checkDir(pfad, sub):
    if len(sub) > 0:
        pfad = pfad + '/' + sub[0]
        if not os.path.isdir(pfad):
            os.mkdir(pfad)
        sub = sub[1:]
        checkDir(pfad, sub)

def lambda_handler(event, context):
    eventS3 = False
    if 'Records' in event:
        record = event['Records'][0]
        if record['eventSource'] == 'aws:s3':
            eventS3 = True
            region = record['awsRegion']
            s3data = record['s3']
            bucket = s3data['bucket']['name']
            key = s3data['object']['key']
    if eventS3:
        pathArray = key.split('/')[:-1]
        if bucket.startswith('config'):
            pathArray = ('config/' + key).split('/')[:-1]
        elif bucket.startswith('db'):
            pathArray = ('db/' + key).split('/')[:-1]
        checkDir('/mnt/smartlife', pathArray)
        file = '/mnt/smartlife/'
        if bucket.startswith('config'):
            file += 'config/'
        elif bucket.startswith('db'):
            file += 'db/'
        file += key
        session = boto3.Session(region_name=region)
        s3 = boto3.resource('s3')
        my_bucket = s3.Bucket(bucket)
        my_bucket.download_file(key, file)
    # TODO implement
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }

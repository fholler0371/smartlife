# smartlife

Project Ziel ist ein WebSystem verteilt lokal auf Raspberry Pi´s und AWS-Cloud möglichst viele Prozesse sind zu automatisieren.

## Realisierte Projekte

- Einkaufsliste mit Übertragung auf Telegram und Hizufügen via Barcode und RFID

## Installation

wget -q -O - https://install.smartlife.holler.pro/ | bash 

/opt/smartlife/smartlife -i

## Ideen

- Zentrale Musiksteuerung
- DNLA Verwaltung über Webseite
- Fernsehprogramm dowlaoden, Auswahl und Fernseher steuern
- Batteriemeldungen nach Telegram und in einer Alarmanzeige auf der Page


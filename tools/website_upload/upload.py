import os
import sys
import json
import subprocess
import mimetypes

try:
    import boto3
except:
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'boto3'])
    import boto3

INSTALL_PATH='/'.join(os.path.realpath(__file__).split('/')[:-3])
CONFIG={}

def upload_file(fname, remote):
    global CONFIG, INSTALL_PATH
    client = boto3.client('s3', region_name=CONFIG['region'], aws_access_key_id=CONFIG['key'], aws_secret_access_key=CONFIG['secret'])
    mimetype, _ = mimetypes.guess_type(fname)
    if mimetype != None:
        client.upload_file(fname,CONFIG['bucket'],remote, ExtraArgs={"ContentType": mimetype})

def upload(folder):
    global INSTALL_PATH
    for file in os.listdir(folder):
        if os.path.isdir(folder+'/'+file):
            upload(folder+'/'+file)
        elif os.path.isfile(folder+'/'+file):
            upload_file(folder+'/'+file, (folder+'/'+file)[len(INSTALL_PATH)+9:])

def config():
    global CONFIG
    while True:
        cols, rows = os.popen('stty size', 'r').read().split()
        cmd = 'whiptail --title "Upload Webinstaller" --menu "Bitte eine Option ausuchen" '+str(int(int(cols)/2))+' '+str(int(int(rows)/2))+' 9 '
        cmd += '   "1)" "Region setzen" '
        cmd += '   "2)" "Bucket setzen" '
        cmd += '   "3)" "Key setzen" '
        cmd += '   "4)" "Secret setzen" '
        cmd += '   "5)" "Upload starten" '
        res = os.popen(cmd+' 3>&2 2>&1 1>&3').read()[:1]
        if res == "":
            os._exit(1)
        elif '1' == res:
            region = "eu-central-1"
            if 'region' in CONFIG:
                region = CONFIG['region']
            cmd = 'whiptail --inputbox "Welche Region?" 8 39 "'+region+'" --title "Dialog"'
            res = os.popen(cmd+' 3>&2 2>&1 1>&3').read()
            CONFIG['region'] = res
        elif '2' == res:
            bucket = ""
            if 'bucket' in CONFIG:
                bucket = CONFIG['bucket']
            cmd = 'whiptail --inputbox "Welcher Bucket?" 8 39 "'+bucket+'" --title "Dialog"'
            res = os.popen(cmd+' 3>&2 2>&1 1>&3').read()
            CONFIG['bucket'] = res
        elif '3' == res:
            key = ""
            if 'key' in CONFIG:
                key = CONFIG['key']
            cmd = 'whiptail --inputbox "AWS Key" 8 39 "'+key+'" --title "Dialog"'
            res = os.popen(cmd+' 3>&2 2>&1 1>&3').read()
            CONFIG['key'] = res
        elif '4' == res:
            secret = ""
            if 'secret' in CONFIG:
                secret = CONFIG['secret']
            cmd = 'whiptail --inputbox "AWS Secret" 8 39 "'+secret+'" --title "Dialog"'
            res = os.popen(cmd+' 3>&2 2>&1 1>&3').read()
            CONFIG['secret'] = res
        elif '5' == res:
            return

def load_config():
    global INSTALL_PATH, CONFIG
    fname = INSTALL_PATH + '/config/website_upload.json'
    if os.path.isfile(fname):
        f = open(fname, 'r')
        CONFIG=json.loads(f.read())
        f.close()

def save_config():
    global INSTALL_PATH, CONFIG
    fname = INSTALL_PATH + '/config/website_upload.json'
    f = open(fname, 'w')
    f.write(json.dumps(CONFIG))
    f.close()

def main():
    global CONFIG, INSTALL_PATH
    load_config()
    if not('region' in CONFIG) or not('bucket' in CONFIG)  or not('key' in CONFIG) or not('secret' in CONFIG):
       config()
    if not('region' in CONFIG) or not('bucket' in CONFIG)  or not('key' in CONFIG) or not('secret' in CONFIG):
       os._exit(0)
    upload(INSTALL_PATH+'/webroot')
    save_config()

if __name__ == '__main__':
    main()

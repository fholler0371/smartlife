#!/usr/bin/env bash

# setup inspiered by pihole

# -e option instructs bash to immediately exit if any command [1] has a non-zero exit status
# We do not want users to end up with a partially working install, so we exit the script
# instead of continuing the installation with something broken
set -e

GIT_URL=https://gitlab.com/fholler0371/smartlife.git

DIR_BASE=/opt/smartlife
DIR_REPRO=/opt/smartlife/.repro
DIR_VENV="/opt/smartlife/env"

PYTHON=${DIR_VENV}/bin/python

USER_SM=smartlife
USER_GROUP=www-data,adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,gpio,i2c,spi

COL_NC='\e[0m' # No Color
COL_LIGHT_GREEN='\e[1;32m'
COL_LIGHT_RED='\e[1;31m'
TICK="[${COL_LIGHT_GREEN}\\u2713${COL_NC}]"
CROSS="[${COL_LIGHT_RED}\\u2717${COL_NC}]"
INFO="[i]"
OVER="\\r\\033[K"

SCRIPT_NAME=$(basename $(test -L "$0" && readlink "$0" || echo "$0"));
SCRIPT_DIR=$(cd $(dirname "$0") && pwd);

is_command() {
    local check_command="$1"
    command -v "${check_command}" >/dev/null 2>&1
}

check_root() {
  ## Is Root ##
  local str="Is Root?"

  printf "\\n"
  if [[ "${EUID}" -eq 0 ]]; then
    printf "  %b %s\\n" "${TICK}" "${str}"
  else
    printf "  %b %s\\n" "${CROSS}" "${str}"
    printf "  %b %bScript called with non-root privileges%b\\n" "${INFO}" "${COL_LIGHT_RED}" "${COL_NC}"
    printf "      SmartLive requires root privileges to install\\n\\n"
    printf "  %b sudo utility check" "${INFO}"
    if is_command sudo ; then
      printf "%b  %b sudo utility check\\n" "${OVER}"  "${TICK}"
      printf "      Starting with sudo\\n"
      wget -q -O - https://install.smartlife.holler.pro/ | sudo bash
      exit 0
    else
      printf "%b  %b sudo utility check\\n" "${OVER}" "${CROSS}"
      printf "  %b Sudo is needed\\n\\n" "${INFO}"
      printf "  %b %bPlease re-run this installer as root${COL_NC}\\n" "${INFO}" "${COL_LIGHT_RED}"
      exit 1
    fi
  fi
}

check_user() {
  ## check user smartlife
  set +e
  printf "  %b Check for user %b" "${INFO}" "${USER_SM}"
  cat /etc/passwd | grep ${USER_SM} >/dev/null 2>&1
  if [ $? -eq 0 ] ; then
    printf "%b  %b Check for user %b\\n" "${OVER}"  "${TICK}" "${USER_SM}"
    set -e
  else
    printf "%b  %b Check for user %b\\n" "${OVER}"  "${CROSS}" "${USER_SM}"
    set -e
    printf "\\n"
    printf "  %b %bCreate User %b %b\\n" "${INFO}" "${COL_LIGHT_RED}" "${USER_SM}" "${COL_NC}"
    adduser $USER_SM --gecos "SmartLife,404,0,0" --disabled-password --quiet
    set +e
    printf "  %b Check for user %b" "${INFO}" "${USER_SM}"
    cat /etc/passwd | grep ${USER_SM} >/dev/null 2>&1
    if [ $? -eq 0 ] ; then
      printf "%b  %b Check for user %b\\n" "${OVER}"  "${TICK}" "${USER_SM}"
      set -e
    else
      printf "%b  %b Check for user %b\\n" "${OVER}"  "${CROSS}" "${USER_SM}"
      set -e
      exit 1
    fi
  fi
  printf "  %b Update groups \\n" "${INFO}"
  usermod -a -G $USER_GROUP $USER_SM
}

check_installer () {
  local str="Check apt-get"
  printf "  %b %b" "${INFO}" "${str}"
  if is_command apt-get ; then
    printf "%b  %b %b\\n" "${OVER}"  "${TICK}" "${str}"
  else
    printf "%b  %b %b\\n" "${OVER}"  "${CROSS}" "${str}"
    exit 1
  fi
  local str="run apt-get update"
  printf "  %b %b" "${INFO}" "${str}"
  apt-get update -q=2
  printf "%b  %b %b\\n" "${OVER}"  "${TICK}" "${str}"
  local str="Checking apt-get for upgraded packages"
  printf "\\n  %b %s..." "${INFO}" "${str}"
  PKG_COUNT="apt-get -s -o Debug::NoLocking=true upgrade | grep -c ^Inst || true"
  updatesToInstall=$(eval "${PKG_COUNT}")
  if [[ -d "/lib/modules/$(uname -r)" ]]; then
    if [[ "${updatesToInstall}" -eq 0 ]]; then
      printf "%b  %b %s... up to date!\\n\\n" "${OVER}" "${TICK}" "${str}"
    else
      printf "%b  %b %s... %s updates available\\n" "${OVER}" "${TICK}" "${str}" "${updatesToInstall}"
      printf "  %b %bIt is recommended to update your OS after installing the SmartLife!%b\\n\\n" "${INFO}" "${COL_LIGHT_GREEN}" "${COL_NC}"
    fi
  else
    printf "%b  %b %s\\n" "${OVER}" "${CROSS}" "${str}"
    printf "      Kernel update detected. If the install fails, please reboot and try again\\n"
  fi
  printf "\\n"
}

check_program() {
  local prg="$1"
  local str="Check $1"
  local lib="$2"
  printf "  %b %b" "${INFO}" "${str}"
  if is_command $prg ; then
    printf "%b  %b %b\\n" "${OVER}"  "${TICK}" "${str}"
  else
    printf "%b  %b %b\\n" "${OVER}"  "${CROSS}" "${str}"
    printf "  %b %b\\n" "${INFO}" "install $1"
    apt-get -qq --no-install-recommends install $lib > /dev/null
    printf "  %b %b" "${INFO}" "${str}"
    if is_command whiptail ; then
      printf "%b  %b %b\\n" "${OVER}"  "${TICK}" "${str}"
    else
      printf "%b  %b %b\\n" "${OVER}"  "${CROSS}" "${str}"
      exit 1
    fi
  fi
}

isRepro() {
  local directory="${1}"
  local rc
  if [[ -d "${directory}" ]]; then
    pushd "${directory}" &> /dev/null || return 1
    git status --short &> /dev/null || rc=$?
  else
    rc=1
  fi
  popd &> /dev/null || return 1
  return "${rc:-0}"
}

makeRepro() {
  local directory="${1}"
  local remoteRepo="${2}"
  str="Clone ${remoteRepo} into ${directory}"
  printf "  %b %s..." "${INFO}" "${str}"
  if [[ -d "${directory}" ]]; then
    rm -rf "${directory}"
  fi
  git clone -q --depth 20 "${remoteRepo}" "${directory}" &> /dev/null || return $?
  pushd "${directory}" &> /dev/null || return 1
  curBranch=$(git rev-parse --abbrev-ref HEAD)
  if [[ "${curBranch}" == "master" ]]; then #If we're calling make_repo() then it should always be master, we may not need to check.
    git reset --hard "$(git describe --abbrev=0 --tags)" || return $?
  fi
  printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${str}"
  chmod -R a+rX "${directory}"
  popd &> /dev/null || return 1
  return 0
}

updateRepro() {
    local directory="${1}"
    local curBranch
    local str="Update repo in ${1}"
    pushd "${directory}" &> /dev/null || return 1
    printf "  %b %s..." "${INFO}" "${str}"
    git reset --hard --quiet &> /dev/null || true # hard reset
    git stash --all --quiet &> /dev/null || true # Okay for stash failure
    git clean --quiet --force -d || true # Okay for already clean directory
    git pull --quiet &> /dev/null || return $?
    curBranch=$(git rev-parse --abbrev-ref HEAD)
    if [[ "${curBranch}" == "master" ]]; then
         git reset --hard "$(git describe --abbrev=0 --tags)" || return $?
    fi
    printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${str}"
    chmod -R a+rX "${directory}"
    popd &> /dev/null || return 1
    return 0
}

getGit() {
  local directory="${1}"
  local remoteRepo="${2}"
  local str="Check for existing repository in ${1}"
  printf "  %b %s..." "${INFO}" "${str}"
  if isRepro "${directory}"; then
    printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${str}"
    updateRepro "${directory}" || { printf "\\n  %b: Could not update local repository. Contact support.%b\\n" "${COL_LIGHT_RED}" "${COL_NC}"; exit 1; }
  else
    printf "%b  %b %s\\n" "${OVER}" "${CROSS}" "${str}"
    makeRepro "${directory}" "${remoteRepo}" || { printf "\\n  %bError: Could not update local repository. Contact support.%b\\n" "${COL_LIGHT_RED}" "${COL_NC}"; exit 1; }
  fi
  echo ""
  return 0
}

check_venv() {
  local directory="${1}"
  local str="Check virtualenv in ${1}"
  printf "  %b %s..." "${INFO}" "${str}"
  if [[ -d "${directory}" ]]; then
    printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${str}"
  else
    printf "%b  %b %s\\n" "${OVER}" "${CROSS}" "${str}"
    check_program "pip3" "python3-pip"
    printf "  %b install virtualenv\\n" "${INFO}"
    pip3 install virtualenv &> /dev/null
    printf "  %b create virtualenv in ${directory}\\n" "${INFO}"
    virtualenv ${directory} &> /dev/null
  fi
  echo ""
  return 0
}

set_owner() {
  printf "  %b %s %s\\n" "${INFO}" "set owner for " "${DIR_BASE}"
  chown -R ${USER_SM}:${USER_SM} ${DIR_BASE}
  chown -R ${USER_SM}:${USER_SM} ${DIR_BASE}/*
  chown -R ${USER_SM}:${USER_SM} ${DIR_REPRO}
}

file_copy() {
  local source="${1}"
  local destination="${2}"
  local file="${3}"
  local str="Copy file ${file} to ${destination}"
  printf "  %b %s..." "${INFO}" "${str}"
  cp ${source}/${file} ${destination}/${file}
  if [[ -f ${destination}/${file} ]]; then
    printf "%b  %b %s\\n" "${OVER}" "${TICK}" "${str}"
  else
    printf "%b  %b %s\\n" "${OVER}" "${CROSS}" "${str}"
    exit 1
  fi
}

main() {
  check_root
  check_user
  check_installer

  check_program "whiptail" "whiptail"
  check_program "python3" "python3"
#  check_program "jq" "jq"
  check_program "git" "git"

  mkdir -p $DIR_REPRO
  mkdir -p $DIR_BASE/bin
  getGit $DIR_REPRO $GIT_URL

  check_venv $DIR_VENV

  set_owner

#  file_copy ${DIR_REPRO}/core/bin ${DIR_BASE}/bin setup.py
  file_copy ${DIR_REPRO}/core ${DIR_BASE}/ smartlife
  chmod +x ${DIR_BASE}/smartlife
#  printf "  %b %s\\n" "${INFO}" "switch to smartlife"
#  pushd ${DIR_BASE} &> /dev/null
#  local cmd="${DIR_BASE}/smartlife"
#  su - ${USER_SM} -c "${cmd}"
}

main "$@"
